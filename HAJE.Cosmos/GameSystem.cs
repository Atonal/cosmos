﻿using HAJE.Cosmos.Rendering;
using HAJE.Cosmos.SystemComponent;
using OpenTK;
using OpenTK.Input;
using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace HAJE.Cosmos
{
    public static class GameSystem
    {
        #region Getter/Setter
        public static Profiler Profiler
        {
            get;
            private set;
        }

        public static GameTime UpdateTime
        {
            get;
            private set;
        }

        public static GameTime RenderTime
        {
            get;
            private set;
        }

        public static BuildInfo BuildInfo
        {
            get;
            private set;
        }

        public static Scheduler Scheduler
        {
            get;
            private set;
        }

        public static MainForm MainForm
        {
            get;
            private set;
        }

        public static LogSystem Log
        {
            get;
            private set;
        }

        public static SystemComponent.GameLoopBase CurrentGameLoop
        {
            get;
            private set;
        }

        public static Vector2 Viewport
        {
            get
            {
                return new Vector2(MainForm.Width, MainForm.Height);
            }
        }
        public static DebugOutput DebugOutput
        {
            get;
            private set;
        }
        #endregion

		static void InitializeCurrentDirectory()
        {
            // Resource 폴더를 일일이 복사하지 않더라도 실행 경로를 수정. 테스트 가능하도록
            if (!Directory.Exists(Environment.CurrentDirectory + "\\Resource"))
            {
                if (Debugger.IsAttached || Directory.Exists(Environment.CurrentDirectory + "\\..\\..\\Resource"))
                {
                    Environment.CurrentDirectory = "../../";
                }
            }
            if (!Directory.Exists(Environment.CurrentDirectory + "\\Resource"))
            {
                throw new FileNotFoundException("리소스 파일의 경로를 찾지 못했습니다.\n현재 경로: " + Environment.CurrentDirectory + "\n");
            }
        }

        static void InitializeErrorReporting()
        {
            Action<Exception> errorHandler = (e) =>
            {
                MessageBox.Show("처리되지 않은 예외가 발생했습니다.\n" +
                    "게임을 종료합니다.\n\n" +
                    "오류내용 -\n" + e, "오류");
                Log.Write(LogLevel.Fatal, e.ToString());
                var sb = new StringBuilder();
                var recentLogs = Log.Recent;
                foreach (var log in recentLogs)
                    sb.AppendLine(log.ToString());
                File.WriteAllText("error.log", sb.ToString());
                if (!Debugger.IsAttached)
                    Environment.Exit(-1);

            };
            Application.ThreadException += (o, e) => { errorHandler(e.Exception); };
            Thread.GetDomain().UnhandledException += (o, e) => { errorHandler((Exception)e.ExceptionObject); };
        }

        public static void Initialize()
        {
            Debug.Assert(initialized == false);

            BuildInfo = new BuildInfo();
            InitializeErrorReporting();
            InitializeCurrentDirectory();
            MainForm = new MainForm();
            Profiler = new Profiler();
            Scheduler = new Scheduler("Root");
            UpdateTime = new GameTime();
            RenderTime = new GameTime();
            DebugOutput = new DebugOutput();

            if (Debugger.IsAttached)
            {
                UpdateTime.SetBreakPointSafeValue((Second)1.0f, (Second)0.05f);
                RenderTime.SetBreakPointSafeValue((Second)1.0f, (Second)0.05f);
            }
            Log = new LogSystem();
            Log.OnMessage += (log) => { Debug.WriteLine(log); };
            initialized = true;

            MainForm.Visible = true;
        }

        public static void Run()
        {
            UpdateTime.Refresh();
            RenderTime.Refresh();
            MainForm.Run();
        }

        public static void UpdateTick()
        {
            UpdateTime.Refresh();
            Scheduler.Update(UpdateTime);
            if (nextGameLoop != null)
            {
                StopGameLoop();
                CurrentGameLoop = nextGameLoop;
                CurrentGameLoop.Run();
                nextGameLoop = null;
                Log.Write(LogLevel.Debug, "GameSystem", CurrentGameLoop.Name + " 실행 시작");
            }
            if (CurrentGameLoop != null)
                CurrentGameLoop.Update(UpdateTime.DeltaTime);

            if (Keyboard.GetState()[Key.F12] == true)
            {
                if (GameSystem.F12Pressed == false)
                {
                    if (DebugOutput.visible == SystemComponent.DebugOutput.Visiblity.Full)
                        DebugOutput.visible = SystemComponent.DebugOutput.Visiblity.WatchOnly;
                    else if (DebugOutput.visible == SystemComponent.DebugOutput.Visiblity.WatchOnly)
                        DebugOutput.visible = SystemComponent.DebugOutput.Visiblity.None;
                    else if (DebugOutput.visible == SystemComponent.DebugOutput.Visiblity.None)
                        DebugOutput.visible = SystemComponent.DebugOutput.Visiblity.Full;

                    GameSystem.F12Pressed = true;
                }
            }
            else
                GameSystem.F12Pressed = false;
        }

        public static void RenderTick()
        {
            RenderTime.Refresh();
            Profiler.UpdateFps(RenderTime);
            Profiler.UpdateProbeProfiles(RenderTime);
            if (CurrentGameLoop != null)
                CurrentGameLoop.RenderFrame(RenderTime.DeltaTime);
            DebugOutput.Draw();
        }

        public static void RunGameLoop(SystemComponent.GameLoopBase loop)
        {
            Log.Write(LogLevel.Debug, "GameSystem", loop.Name + " 실행 예약됨");
            nextGameLoop = loop;
        }

        public static void StopGameLoop()
        {
            if (CurrentGameLoop == null) return;
            if (CurrentGameLoop.Status == GameLoopStatus.Running)
                CurrentGameLoop.Stop();
            CurrentGameLoop.Dispose(true);
        }

        public static void FinalizeSystem()
        {
            StopGameLoop();
            MainForm.Dispose();
            MainForm = null;
            ShaderProvider.UnloadShader();
            GLHelper.Unload();
        }

        #region privates

        private static bool F12Pressed = false;

        static bool initialized = false;
        static SystemComponent.GameLoopBase nextGameLoop = null;
        

        #endregion
    }
}
