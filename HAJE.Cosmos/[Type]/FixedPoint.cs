﻿using System.Diagnostics;
using HAJE.Cosmos.SystemComponent;
using System;
using System.Collections.Generic;

namespace HAJE.Cosmos
{
    [UnitTestAttribute]
    public struct FixedPoint
    {
        #region type conversions

        public static explicit operator float(FixedPoint value)
        {
            return value.FloatingValue;
        }

        public static explicit operator FixedPoint(float value)
        {
            return new FixedPoint(value);
        }

        public static explicit operator Int64(FixedPoint value)
        {
            return (value.value >> SBit);
        }

        public static implicit operator FixedPoint(int value)
        {
            return new FixedPoint(value << SBit);
        }

        #endregion

        #region unary arithmetic operators

        public static FixedPoint operator -(FixedPoint op)
        {
            return new FixedPoint(-op.InternalValue);
        }

        #endregion

        #region binary arithmetic operators

        public static FixedPoint operator +(FixedPoint l, FixedPoint r)
        {
            return new FixedPoint(l.value + r.value);
        }

        public static FixedPoint operator -(FixedPoint l, FixedPoint r)
        {
            return new FixedPoint(l.value - r.value);
        }

        public static FixedPoint operator /(FixedPoint l, FixedPoint r)
        {
            return new FixedPoint((l.value << SBit) / r.value);
        }

        public static FixedPoint operator *(FixedPoint l, FixedPoint r)
        {
            return new FixedPoint((l.value * r.value) >> SBit);
        }

        #endregion

        #region comparison operators

        public static bool operator <(FixedPoint l, FixedPoint r)
        {
            return l.value < r.value;
        }

        public static bool operator <=(FixedPoint l, FixedPoint r)
        {
            return l.value <= r.value;
        }

        public static bool operator ==(FixedPoint l, FixedPoint r)
        {
            return l.value == r.value;
        }

        public static bool operator !=(FixedPoint l, FixedPoint r)
        {
            return l.value != r.value;
        }

        public static bool operator >=(FixedPoint l, FixedPoint r)
        {
            return l.value >= r.value;
        }

        public static bool operator >(FixedPoint l, FixedPoint r)
        {
            return l.value > r.value;
        }

        #endregion

        #region c# base overrides

        public override bool Equals(object obj)
        {
            if (obj is FixedPoint)
            {
                return ((FixedPoint)obj).value == value;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return value.GetHashCode();
        }

        public override string ToString()
        {
            return ((float)this).ToString();
        }

        #endregion

        /// <summary>
        /// Sqrt 함수. Fixed-Point Iteration를 사용한다.
        /// </summary>
        public static FixedPoint Sqrt(FixedPoint value)
        {
            if (value == 0)
                return value;
            else if (value < 0)
                throw new Exception(string.Format("FixedPoint.Sqrt 함수에 음수인 파라미터가 들어왔습니다.\nvalue = {0}", value));

            FixedPoint res = value;
            FixedPoint delta;

            do
            {
                // res^2 - value = 0이라는 함수에 대해 Fixed-Point Iteration을 사용함.
                FixedPoint newres = (res + value / res) / 2;

                delta = res - newres;
                res = newres;
            } while (delta > (FixedPoint)0.00001);
            
            return res;
        }

        // 삼각 함수 계열은 전부 Cordic Method를 사용함
        #region trigonometric functions 

        // precalculated atan(1/2^index), index in [0, 12]
        private static FixedPoint[] CordicAngleTable =
        {
            (FixedPoint)0.7853f,
            (FixedPoint)0.4636f, (FixedPoint)0.2450f, (FixedPoint)0.1244f, (FixedPoint)0.0624f,
            (FixedPoint)0.0312f, (FixedPoint)0.0156f, (FixedPoint)0.0078f, (FixedPoint)0.0039f, 
            (FixedPoint)0.0020f, (FixedPoint)0.0010f, (FixedPoint)0.0005f, (FixedPoint)0.0003f
        };
        private static FixedPoint CordicConstant = (FixedPoint)1.646f;

        public static FixedPoint Pi = new FixedPoint(3.141592f);
        public static FixedPoint Sin(FixedPoint radian)
        {
            // 범위 처리
            if (radian > Pi / 2)
                return -Sin(radian - Pi);
            if (radian < -Pi / 2)
                return -Sin(radian + Pi);

            // v = [cos radian, sin radian]
            FixedVector2 v = new FixedVector2(0.607f, 0);
            FixedPoint factor = 1;
            for (int i = 0; i < CordicAngleTable.Length; i++)
            {
                if (radian > 0)
                {
                    v = new FixedVector2(v.X - new FixedPoint(v.Y.InternalValue >> i), v.Y + new FixedPoint(v.X.InternalValue >> i));
                    radian -= CordicAngleTable[i];
                }
                else
                {
                    v = new FixedVector2(v.X + new FixedPoint(v.Y.InternalValue >> i), v.Y - new FixedPoint(v.X.InternalValue >> i));
                    radian += CordicAngleTable[i];
                }

                factor.InternalValue >>= 1;
            }

            return v.Y;
        }

        public static FixedPoint Cos(FixedPoint radian)
        {
            // 범위 처리
            if (radian > Pi / 2)
                return -Cos(radian - Pi);
            if (radian < -Pi / 2)

            // v = [cos radian, sin radian]
                return -Cos(radian + Pi);
            FixedVector2 v = new FixedVector2(0.607f, 0);
            FixedPoint factor = 1;
            for (int i = 0; i < CordicAngleTable.Length; i++)
            {
                if (radian > 0)
                {
                    v = new FixedVector2(v.X - new FixedPoint(v.Y.InternalValue >> i), v.Y + new FixedPoint(v.X.InternalValue >> i));
                    radian -= CordicAngleTable[i];
                }
                else
                {
                    v = new FixedVector2(v.X + new FixedPoint(v.Y.InternalValue >> i), v.Y - new FixedPoint(v.X.InternalValue >> i));
                    radian += CordicAngleTable[i];
                }

                factor.InternalValue >>= 1;
            }

            return v.X;
        }

        public static FixedPoint Tan(FixedPoint radian)
        {
            // 범위 처리
            if (radian > Pi / 2)
                return Tan(radian - Pi);
            if (radian < -Pi / 2)
                return Tan(radian + Pi);

            // v = [cos radian, sin radian]
            FixedVector2 v = new FixedVector2(1000000, 0);
            FixedPoint factor = 1;
            for (int i = 0; i < CordicAngleTable.Length; i++)
            {
                if (radian > 0)
                {
                    v = new FixedVector2(v.X - new FixedPoint(v.Y.InternalValue >> i), v.Y + new FixedPoint(v.X.InternalValue >> i));
                    radian -= CordicAngleTable[i];
                }
                else
                {
                    v = new FixedVector2(v.X + new FixedPoint(v.Y.InternalValue >> i), v.Y - new FixedPoint(v.X.InternalValue >> i));
                    radian += CordicAngleTable[i];
                }

                factor.InternalValue >>= 1;
            }

            return v.Y / v.X;
        }

        public static FixedPoint Atan(FixedPoint value)
        {
            FixedPoint trace = 0;
            FixedPoint tan = 1;
            FixedPoint radian = 0;

            for (int i = 0; i < CordicAngleTable.Length; i++ )
            {
                if (value > trace)
                {
                    trace = (trace + tan) / (1 - new FixedPoint(trace.InternalValue >> i));
                    radian += CordicAngleTable[i];
                }
                else
                {
                    trace = (trace - tan) / (1 + new FixedPoint(trace.InternalValue >> i));
                    radian -= CordicAngleTable[i];
                }

                tan.InternalValue >>= 1;
            }
            
            return radian;
        }

        /// <summary>
        /// atan(y/x)를 리턴.
        /// </summary>
        public static FixedPoint Atan2(FixedPoint y, FixedPoint x)
        {
            if (y == 0)
            {
                if (x >= 0)
                    return Pi / 2;
                else
                    return -Pi / 2;
            }
            else if (x == 0)
            {
                if (y > 0)
                    return 0;
                else
                    return Pi;
            }

            if (x > 0 && y > 0)
                return Atan(y / x);
            else if (x < 0 && y > 0)
                return Atan(y / x) + Pi;
            else if (x < 0 && y < 0)
                return Atan(y / x) - Pi;
            else
                return Atan(y / x);
        }

        #endregion trigonometric functions

        public static FixedPoint Abs(FixedPoint value)
        {
            if (value < 0)
            {
                return -value;
            }
            else
            {
                return value;
            }
        }

        private Int64 value;
        public float FloatingValue
        {
            get
            {
                return value / One;
            }
            set
            {
                this.value = (Int64)(value * One);
            }
        }
        public Int64 InternalValue
        {
            get
            {
                return value;
            }
            set
            {
                this.value = value;
            }
        }

        #region privates

        /// <summary>
        /// 내부 구현용 편의성을 위한 생성자
        /// </summary>
        private FixedPoint(Int64 fixedPointInnerValue)
        {
            value = fixedPointInnerValue;
        }

        /// <summary>
        /// 외부에서는 생성자 대신 명시적 타입 변환 사용
        /// </summary>
        private FixedPoint(float value)
        {
            // Value 속성만을 사용하여 일관성있는 타입 변환을 하기 위해 임시 초기화
            this.value = 0;
            // 진짜 초기화
            FloatingValue = value;
        }

        /// <summary>
        /// 유효 숫자 비트 수
        /// 반드시 2의 배수로.
        /// </summary>
        const int SBit = 12;

        /// <summary>
        /// 유효 숫자 절반
        /// </summary>
        const int HSBit = SBit / 2;

        /// <summary>
        /// 숫자 1.0f가 내부 값에서 어떻게 표현되는가.
        /// </summary>
        const float One = (float)(1 << SBit);

        static FixedPoint()
        {
            Debug.Assert(SBit % 2 == 0);
            Debug.Assert(HSBit == SBit / 2);
        }

        #endregion


        private delegate FixedPoint GetRandom(Random r);
        private delegate FixedPoint FIxedPointDelegate(FixedPoint lhs, FixedPoint rhs);
        private delegate float ExpectDelegate(float lhs, float rhs);
        private delegate Boolean ExceptionCatcher(FixedPoint lhs, FixedPoint rhs);

        [Conditional("UNIT_TEST")]
        public static void RunUnitTest()
        {
            UnitTest.Begin("FixedPoint");

            #region Test List
            List<Tuple<string, GetRandom, GetRandom, FIxedPointDelegate, ExpectDelegate, ExceptionCatcher>> testlist =
                new List<Tuple<string, GetRandom, GetRandom, FIxedPointDelegate, ExpectDelegate, ExceptionCatcher>>()
            {
                new Tuple<string,GetRandom,GetRandom,FIxedPointDelegate,ExpectDelegate,ExceptionCatcher>
                (
                    "Comparison <",
                    (r) => r.NextFixedPoint(-100, 100),
                    (r) => r.NextFixedPoint(-100, 100),
                    (lhs, rhs) => lhs < rhs ? 0 : 1,
                    (lhs, rhs) => lhs < rhs ? 0 : 1,
                    null
                ),
                new Tuple<string,GetRandom,GetRandom,FIxedPointDelegate,ExpectDelegate,ExceptionCatcher>
                (
                    "Comparison >",
                    (r) => r.NextFixedPoint(-100, 100),
                    (r) => r.NextFixedPoint(-100, 100),
                    (lhs, rhs) => lhs > rhs ? 0 : 1,
                    (lhs, rhs) => lhs > rhs ? 0 : 1,
                    null
                ),
                new Tuple<string,GetRandom,GetRandom,FIxedPointDelegate,ExpectDelegate,ExceptionCatcher>
                (
                    "Comparison >=",
                    (r) => r.NextFixedPoint((FixedPoint)(-0.01f), (FixedPoint)0.01f),
                    (r) => r.NextFixedPoint((FixedPoint)(-0.01f), (FixedPoint)0.01f),
                    (lhs, rhs) => lhs >= rhs ? 0 : 1,
                    (lhs, rhs) => lhs >= rhs ? 0 : 1,
                    null
                ),
                new Tuple<string,GetRandom,GetRandom,FIxedPointDelegate,ExpectDelegate,ExceptionCatcher>
                (
                    "Comparison <=",
                    (r) => r.NextFixedPoint((FixedPoint)(-0.01f), (FixedPoint)0.01f),
                    (r) => r.NextFixedPoint((FixedPoint)(-0.01f), (FixedPoint)0.01f),
                    (lhs, rhs) => lhs <= rhs ? 0 : 1,
                    (lhs, rhs) => lhs <= rhs ? 0 : 1,
                    null
                ),
                new Tuple<string,GetRandom,GetRandom,FIxedPointDelegate,ExpectDelegate,ExceptionCatcher>
                (
                    "Comparison ==",
                    (r) => r.NextFixedPoint((FixedPoint)(-0.01f), (FixedPoint)0.01f),
                    (r) => r.NextFixedPoint((FixedPoint)(-0.01f), (FixedPoint)0.01f),
                    (lhs, rhs) => lhs == rhs ? 0 : 1,
                    (lhs, rhs) => lhs == rhs ? 0 : 1,
                    null
                ),
                new Tuple<string,GetRandom,GetRandom,FIxedPointDelegate,ExpectDelegate,ExceptionCatcher>
                (
                    "Comparison !=",
                    (r) => r.NextFixedPoint((FixedPoint)(-0.01f), (FixedPoint)0.01f),
                    (r) => r.NextFixedPoint((FixedPoint)(-0.01f), (FixedPoint)0.01f),
                    (lhs, rhs) => lhs != rhs ? 0 : 1,
                    (lhs, rhs) => lhs != rhs ? 0 : 1,
                    null
                ),
                new Tuple<string,GetRandom,GetRandom,FIxedPointDelegate,ExpectDelegate,ExceptionCatcher>
                (
                    "Add",
                    (r) => r.NextFixedPoint(-100, 100),
                    (r) => r.NextFixedPoint(-100, 100),
                    (lhs, rhs) => lhs + rhs,
                    (lhs, rhs) => lhs + rhs,
                    null
                ),
                new Tuple<string,GetRandom,GetRandom,FIxedPointDelegate,ExpectDelegate,ExceptionCatcher>
                (
                    "Sub",
                    (r) => r.NextFixedPoint(-100, 100),
                    (r) => r.NextFixedPoint(-100, 100),
                    (lhs, rhs) => lhs - rhs,
                    (lhs, rhs) => lhs - rhs,
                    null
                ),
                new Tuple<string,GetRandom,GetRandom,FIxedPointDelegate,ExpectDelegate,ExceptionCatcher>
                (
                    "Mul",
                    (r) => r.NextFixedPoint(-100, 100),
                    (r) => r.NextFixedPoint(-100, 100),
                    (lhs, rhs) => lhs * rhs,
                    (lhs, rhs) => lhs * rhs,
                    null
                ),
                new Tuple<string,GetRandom,GetRandom,FIxedPointDelegate,ExpectDelegate,ExceptionCatcher>
                (
                    "Div",
                    (r) => r.NextFixedPoint(-100, 100),
                    (r) => r.NextFixedPoint(-100, 100),
                    (lhs, rhs) => lhs / rhs,
                    (lhs, rhs) => lhs / rhs,
                    (lhs, rhs) => rhs == 0
                ),
                new Tuple<string,GetRandom,GetRandom,FIxedPointDelegate,ExpectDelegate,ExceptionCatcher>
                (
                    "Minus",
                    (r) => r.NextFixedPoint(-100, 100),
                    null,
                    (lhs, rhs) => -lhs,
                    (lhs, rhs) => -lhs,
                    null
                ),
                new Tuple<string,GetRandom,GetRandom,FIxedPointDelegate,ExpectDelegate,ExceptionCatcher>
                (
                    "Sqrt",
                    (r) => r.NextFixedPoint(0, 100),
                    null,
                    (lhs, rhs) => FixedPoint.Sqrt(lhs),
                    (lhs, rhs) => (float)Math.Sqrt(lhs),
                    null
                ),
                new Tuple<string,GetRandom,GetRandom,FIxedPointDelegate,ExpectDelegate,ExceptionCatcher>
                (
                    "Sin",
                    (r) => r.NextFixedPoint(-10, 10),
                    null,
                    (lhs, rhs) => FixedPoint.Sin(lhs),
                    (lhs, rhs) => (float)Math.Sin(lhs),
                    null
                ),
                new Tuple<string,GetRandom,GetRandom,FIxedPointDelegate,ExpectDelegate,ExceptionCatcher>
                (
                    "Cos",
                    (r) => r.NextFixedPoint(-10, 10),
                    null,
                    (lhs, rhs) => FixedPoint.Cos(lhs),
                    (lhs, rhs) => (float)Math.Cos(lhs),
                    null
                ),
                new Tuple<string,GetRandom,GetRandom,FIxedPointDelegate,ExpectDelegate,ExceptionCatcher>
                (
                    "Tan",
                    (r) => r.NextFixedPoint(-10, 10),
                    null,
                    (lhs, rhs) => FixedPoint.Tan(lhs),
                    (lhs, rhs) => (float)Math.Tan(lhs),
                    null
                ),
                new Tuple<string,GetRandom,GetRandom,FIxedPointDelegate,ExpectDelegate,ExceptionCatcher>
                (
                    "Atan",
                    (r) => r.NextFixedPoint(-Pi / 2, Pi / 2),
                    null,
                    (lhs, rhs) => FixedPoint.Atan(FixedPoint.Tan(lhs)),
                    (lhs, rhs) => lhs,
                    (lhs, rhs) => FixedPoint.Cos(lhs) == 0
                ),
                new Tuple<string,GetRandom,GetRandom,FIxedPointDelegate,ExpectDelegate,ExceptionCatcher>
                (
                    "Atan2",
                    (r) => r.NextFixedPoint(-10, 10),
                    (r) => r.NextFixedPoint(-10, 10),
                    (lhs, rhs) => FixedPoint.Atan2(lhs, rhs),
                    (lhs, rhs) => (float)Math.Atan2(lhs, rhs),
                    null
                )
            };
            #endregion

            Random rd = new Random(DateTime.Now.Millisecond);
            int errorcount = 0;
            int maxtestcount = 100;
            FixedPoint maxerror = (FixedPoint)0.1f;
            foreach (var tuple in testlist)
            {
                errorcount = 0;
                for (int i = 0; i < maxtestcount; i++)
                {
                    FixedPoint arg0 = tuple.Item2 == null ? 0 : tuple.Item2.Invoke(rd);
                    FixedPoint arg1 = tuple.Item3 == null ? 0 : tuple.Item2.Invoke(rd);
                    
                    FixedPoint eval = 0;
                    FixedPoint expect = 0;

                    try
                    {
                        eval = tuple.Item4.Invoke(arg0, arg1);
                        expect = (FixedPoint)tuple.Item5.Invoke(arg0.FloatingValue, arg1.FloatingValue);

                        if (Abs(expect - eval) > maxerror)
                        {
                            //GameSystem.Log.Write(String.Format("{0} -- Error {1}", arg0, expect - eval));
                            errorcount++;
                        }
                    }
                    catch (Exception e)
                    {
                        if (tuple.Item4 == null)
                            throw e;
                        else if (tuple.Item6.Invoke(arg0, arg1) == false)
                            throw e;
                    }
                }
                GameSystem.Log.Write(String.Format(
                    "{0} Exception Rate : {1}/{2} ({3}%)",
                    tuple.Item1,
                    errorcount, maxtestcount, (float)errorcount / maxtestcount * 100));
            
            }

            UnitTest.End();
        }
    }
}
