﻿using OpenTK;
using System;

namespace HAJE.Cosmos
{
    public struct FixedVector2
    {
        public FixedPoint X;
        public FixedPoint Y;

        public FixedVector2(int x, int y)
        {
            X = x;
            Y = y;
        }

        public FixedVector2(float x, float y)
        {
            X = (FixedPoint)x;
            Y = (FixedPoint)y;
        }

        public FixedVector2(Vector2 vector)
        {
            X = (FixedPoint)vector.X;
            Y = (FixedPoint)vector.Y;
        }

        public FixedVector2(FixedPoint x, FixedPoint y)
        {
            X = x;
            Y = y;
        }

        public static readonly FixedVector2 Zero = new FixedVector2(0, 0);

        #region type conversion

        public static explicit operator FixedVector2(Vector2 value)
        {
            return new FixedVector2(value);
        }

        public static explicit operator Vector2(FixedVector2 value)
        {
            return new Vector2((float)value.X, (float)value.Y);
        }

        #endregion

        #region unary arithmetic operators

        public static FixedVector2 operator -(FixedVector2 vec)
        {
            return new FixedVector2(-vec.X, -vec.Y);
        }

        #endregion

        #region binary arithmetic operators

        public static FixedVector2 operator -(FixedVector2 left, FixedVector2 right)
        {
            return new FixedVector2(left.X - right.X, left.Y - right.Y);
        }

        public static FixedVector2 operator *(FixedPoint scale, FixedVector2 vec)
        {
            return new FixedVector2(scale * vec.X, scale * vec.Y);
        }

        public static FixedVector2 operator *(FixedVector2 vec, FixedPoint scale)
        {
            return new FixedVector2(scale * vec.X, scale * vec.Y);
        }

        public static FixedVector2 operator *(FixedVector2 left, FixedVector2 scale)
        {
            return new FixedVector2(left.X * scale.X, left.Y * scale.Y);
        }

        public static FixedVector2 operator /(FixedVector2 vec, FixedPoint scale)
        {
            return new FixedVector2(vec.X / scale, vec.Y / scale);
        }

        public static FixedVector2 operator +(FixedVector2 left, FixedVector2 right)
        {
            return new FixedVector2(left.X + right.X, left.Y + right.Y);
        }

        #endregion

        #region comparison operators

        public static bool operator !=(FixedVector2 left, FixedVector2 right)
        {
            return (left.X != right.X) || (left.Y != right.Y);
        }

        public static bool operator ==(FixedVector2 left, FixedVector2 right)
        {
            return (left.X == right.X) && (left.Y == right.Y);
        }

        #endregion

        #region vector operations

        public FixedPoint LengthSqared()
        {
            return (X * X) + (Y * Y);
        }

        public FixedPoint Length()
        {
            return FixedPoint.Sqrt(LengthSqared());
        }

        public static FixedVector2 Normalize(FixedVector2 v)
        {
            return v / v.Length();
        }

        public void Normalize()
        {
            var len = Length();
            X = X / len;
            Y = Y / len;
        }

        public static FixedPoint Dot(FixedVector2 v, FixedVector2 w)
        {
            return v.X * w.X + v.Y * w.Y;
        }
        public static FixedVector2 Wedge2(FixedVector2 v)
        {
            return new FixedVector2(-v.Y, v.X);
        }
        public static FixedPoint Wedge3(FixedVector2 v, FixedVector2 w)
        {
            return v.X * w.Y - v.Y * w.X;
        }

        public Vector2 ToVector2()
        {
            return new Vector2(this.X.FloatingValue, this.Y.FloatingValue);
        }

        // 만약 원한다면 초기 벡터를 this로 하고
        // Cordic Method를 따라가는 방법으로 최적화를 할 수 있음
        //
        // 연산량은 절반 이상으로 줄어듬
        public FixedVector2 Rotate(FixedPoint radian)
        {
            FixedPoint cos = FixedPoint.Cos(radian);
            FixedPoint sin = FixedPoint.Sin(radian);

            return new FixedVector2(
                this.X * cos - this.Y * sin,
                this.X * sin + this.Y * cos);
        }

        #endregion

        #region c# base overrides

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override string ToString()
        {
            return string.Format("X={0}, Y={1}", X, Y);
        }

        #endregion
    }
}
