﻿
namespace HAJE.Cosmos
{
    /// <summary>
    /// 시간의 초 단위를 나타내는 타입.
    /// 시간 단위를 타입으로 명시해서 바로 알아볼 수 있게 한다.
    /// 
    /// float->Second는 명시적 변환
    /// Second->float은 암시적 변환이 된다.
    /// 
    /// 기타 연산은 float과 동일하게 사용 가능해야한다.
    /// </summary>
    public struct Second
    {
        public static explicit operator Second(float time)
        {
            return new Second(time);
        }

        public static implicit operator float(Second time)
        {
            return time.value;
        }

        #region operator

        public static Second operator -(Second lhs, Second rhs)
        {
            return (Second)(lhs.value - rhs.value);
        }

        public static Second operator *(Second lhs, float rhs)
        {
            return (Second)(lhs.value * rhs);
        }

        #endregion

        public override string ToString()
        {
            return value + "seconds";
        }

        #region privates

        private float value;

        private Second(float value)
        {
            this.value = value;
        }

        #endregion
    }
}
