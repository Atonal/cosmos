﻿using OpenTK;
using System;

namespace HAJE.Cosmos
{
    public struct Radian
    {
        public Radian(float radian)
        {
            value = radian;
        }
        public Radian(Degree degree)
        {
            value = (float)degree / 180.0f * Pi;
        }

        public static float Cos(Radian r)
        {
            return (float)Math.Cos(r.value);
        }
        public static float Sin(Radian r)
        {
            return (float)Math.Sin(r.value);
        }
        public static float Tan(Radian r)
        {
            return (float)Math.Tan(r.value);
        }

        public Vector2 Rotate(Vector2 v)
        {
            return new Vector2(
                (float)(v.X * Math.Cos(value) - v.Y * Math.Sin(value)),
                (float)(v.X * Math.Sin(value) + v.Y * Math.Cos(value))
            );
        }

        public static Vector2 ToDirection(Radian r)
        {
            return new Vector2(Cos(r), Sin(r));
        }

        public static explicit operator Radian(float radian)
        {
            return new Radian(radian);
        }

        public static implicit operator Radian(Degree degree)
        {
            return new Radian(degree);
        }

        public static implicit operator float(Radian radian)
        {
            return radian.value;
        }

        public static Radian operator +(Radian a, Radian b)
        {
            return new Radian(a.value + b.value);
        }

        public static Radian operator -(Radian a, Radian b)
        {
            return new Radian(a.value - b.value);
        }

        public static Radian operator /(Radian a, float f)
        {
            return new Radian(a.value / f);
        }

        public static Radian operator *(Radian a, float f)
        {
            return new Radian(a.value * f);
        }

        public static Radian operator *(float f, Radian a)
        {
            return a * f;
        }

        public static readonly Radian Pi = new Radian((float)Math.PI);

        private float value;
    }
}
