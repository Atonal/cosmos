﻿using OpenTK;

namespace HAJE.Cosmos
{
    public static class MatrixHelper
    {
        public static Matrix4 RotationYawPitchRoll(Vector3 rotation)
        {
            return RotationYawPitchRoll(
                (Radian)rotation.X,
                (Radian)rotation.Y,
                (Radian)rotation.Z);
        }

        public static Matrix4 RotationYawPitchRoll(Radian xRot, Radian yRot, Radian zRot)
        {
            Matrix4 x = Matrix4.CreateRotationX(xRot);
            Matrix4 y = Matrix4.CreateRotationY(yRot);
            Matrix4 z = Matrix4.CreateRotationZ(zRot);
            return z * y * x;
        }
    }
}
