﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.Cosmos
{
    public static class MatrixExtension
    {
        public static Vector2 Transform(this Matrix2 matrix, Vector2 v)
        {
            return new Vector2(
                matrix.M11 * v.X + matrix.M12 * v.Y,
                matrix.M21 * v.X + matrix.M22 * v.Y);
        }
    }
}
