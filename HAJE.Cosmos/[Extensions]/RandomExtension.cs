﻿using System;

namespace HAJE.Cosmos
{
    public static class RandomExtension
    {
        public static float NextFloat(this Random rand, float max)
        {
            return (float)(rand.NextDouble() * max);
        }

        public static float NextFloat(this Random rand, float min, float max)
        {
            return (float)(rand.NextDouble() * (max - min) + min);
        }

        public static FixedPoint NextFixedPoint(this Random rand, FixedPoint max)
        {
            return (FixedPoint)rand.NextDouble() * max;
        }

        public static FixedPoint NextFixedPoint(this Random rand, FixedPoint min, FixedPoint max)
        {
            return (FixedPoint)rand.NextDouble() * (max - min) + min;
        }
    }
}
