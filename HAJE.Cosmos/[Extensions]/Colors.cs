﻿using OpenTK;
using OpenTK.Graphics;

namespace HAJE.Cosmos
{
    public static class Colors
    {
        public static readonly Color4 White = new Color4(255, 255, 255, 255);
    }
}
