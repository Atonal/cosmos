﻿using OpenTK;
using System.Drawing;

namespace HAJE.Cosmos
{
    public static class SizeExtension
    {
        public static Vector2 ToVector2(this Size size)
        {
            return new Vector2(size.Width, size.Height);
        }
    }
}
