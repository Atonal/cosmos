﻿using OpenTK;
using System.Drawing;

namespace HAJE.Cosmos
{
    public static class ColorExtension
    {
        public static uint ToRgba(this Color color)
        {
            return (uint)color.A << 24 | (uint)color.B << 16 | (uint)color.G << 8 | (uint)color.R;
        }

        public static Vector3 ToVector3(this Color color)
        {
            return new Vector3(
                color.R / 255.0f,
                color.G / 255.0f,
                color.B / 255.0f
            );
        }

        public static Vector4 ToVector4(this Color color)
        {
            return new Vector4(
                color.R / 255.0f,
                color.G / 255.0f,
                color.B / 255.0f,
                color.A / 255.0f
            );
        }
    }
}
