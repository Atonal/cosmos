﻿using OpenTK;
using System.Drawing;

namespace HAJE.Cosmos
{
    public static class SizeFExtension
    {
        public static Vector2 ToVector2(this SizeF size)
        {
            return new Vector2(size.Width, size.Height);
        }
    }
}
