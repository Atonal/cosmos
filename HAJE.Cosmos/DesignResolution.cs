﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.Cosmos
{
    public static class DesignResolution
    {
        public const float Width = 1280;
        public const float Height = 720;
    }
}
