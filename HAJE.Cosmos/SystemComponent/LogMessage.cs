﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.Cosmos.SystemComponent
{
    public class LogMessage
    {
        public DateTime TimeStamp;
        public LogLevel Level;
        public string Module;
        public string Message;

        public override string ToString()
        {
            return string.Format("{0} {1} {2}: {3}", 
                TimeStamp.ToString("HH:mm:ss"),
                Level.ToShortString(),
                Module,
                Message);
        }
    }
}
