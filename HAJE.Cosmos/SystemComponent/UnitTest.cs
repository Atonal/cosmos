﻿using System;
using System.Linq;
using System.Diagnostics;

namespace HAJE.Cosmos.SystemComponent
{
    public class UnitTestAttribute : Attribute { }

    public static class UnitTest
    {
        [Conditional("UNIT_TEST")]
        public static void Run()
        {
            GameSystem.Log.Write(LogLevel.Info, "UnitTest", "시작");
            RunAllTest();
            GameSystem.Log.Write(LogLevel.Info, "UnitTest", "끝");
        }

        public static void RunAllTest()
        {
            GameSystem.Log.Write(LogLevel.Info, "UnitTest", "전체 자동 테스트 시작");

#if !UNIT_TEST
            throw new InvalidOperationException("UNIT_TEST 매크로가 정의되어있지 않습니다.");
#endif
            var typesWithMyAttribute =
                from a in AppDomain.CurrentDomain.GetAssemblies().AsParallel()
                from t in a.GetTypes()
                let attributes = t.GetCustomAttributes(typeof(UnitTestAttribute), true)
                where attributes != null && attributes.Length > 0
                select new { Type = t, Attributes = attributes.Cast<UnitTestAttribute>() };

            foreach (var t in typesWithMyAttribute)
            {
                var method = t.Type.GetMethod("RunUnitTest");
                if (method == null)
                    throw new Exception("UnitTestAttribute가 정의되어 있지만 RunUnitTest 메서드를 정의하지 않는 타입이 있습니다. 타입 이름:" + t.Type.Name);
                method.Invoke(null, null);
            }

            GameSystem.Log.Write(LogLevel.Info, "UnitTest", "전체 자동 테스트 종료");
        }

        public static void Begin(string name)
        {
            if (!string.IsNullOrWhiteSpace(testModuleName))
                throw new Exception("이전 테스트 코드의 UnitTest.End() 호출 없이 다음 테스트가 시작되었습니다. 이전 모듈: " + testModuleName);
            GameSystem.Log.Write(LogLevel.Info, "UnitTest", name + " 테스트 시작");
            
            testModuleName = name;
            testCount = 0;
            testFailed = false;
        }

        public static void Test(bool testValue)
        {
            Test(testValue, (++testCount).ToString());
        }

        public static void Test(bool testValue, string description)
        {
            GameSystem.Log.Write(testValue ? LogLevel.Debug : LogLevel.Warning,
                testModuleName,
                string.Format("Test Case {0} {1}",
                    description, testValue ? "성공" : "실패")
            );
            if (testValue == false)
            {
                testFailed = true;
                throw new Exception(testModuleName + " 유닛 테스트 실패");
            }
        }

        public static void End()
        {
            GameSystem.Log.Write(LogLevel.Info,
                "UnitTest",
                string.Format("{0} 테스트 {1}", testModuleName, !testFailed ? "성공" : "실패"));
            
            testModuleName = null;
        }

        static string testModuleName;
        static int testCount;
        static bool testFailed;
    }
}
