﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.Cosmos.SystemComponent
{
    public class Scheduler : IUpdatable
    {
        public readonly string DebugName;

        public Scheduler(string debugName)
        {
            DebugName = debugName;
        }

        public ScheduledElement Schedule(IUpdatable loop)
        {
            return ScheduleRepeat(loop, (Second)0);
        }

        public ScheduledElement Schedule(Action<GameTime> func)
        {
            return ScheduleRepeat(func, (Second)0);
        }

        public ScheduledElement Schedule(Action func)
        {
            return ScheduleRepeat(func, (Second)0);
        }

        public ScheduledElement ScheduleOnce(Action func, Second delay)
        {
            var ret = new InnerScheduledElement(new VoidUpdateHandler(func), this);
            ret.ScheduleOnce(delay);
            scheduledList.Add(ret);
            return ret;
        }

        public ScheduledElement ScheduleOnce(Action<GameTime> func, Second delay)
        {
            var ret = new InnerScheduledElement(new UpdateHandler(func), this);
            ret.ScheduleOnce(delay);
            scheduledList.Add(ret);
            return ret;
        }

        public ScheduledElement ScheduleRepeat(Action func, Second interval)
        {
            Debug.Assert(interval >= 0);
            var ret = new InnerScheduledElement(new VoidUpdateHandler(func), this);
            ret.ScheduleRepeat(interval);
            scheduledList.Add(ret);
            return ret;
        }

        public ScheduledElement ScheduleRepeat(Action<GameTime> func, Second interval)
        {
            Debug.Assert(interval >= 0);
            var ret = new InnerScheduledElement(new UpdateHandler(func), this);
            ret.ScheduleRepeat(interval);
            scheduledList.Add(ret);
            return ret;
        }

        public ScheduledElement ScheduleRepeat(IUpdatable updatable, Second interval)
        {
            Debug.Assert(interval >= 0);
            var ret = new InnerScheduledElement(updatable, this);
            ret.ScheduleRepeat(interval);
            scheduledList.Add(ret);
            return ret;
        }

        public void Unschedule(IUpdatable loop)
        {
            foreach (var s in scheduledList)
            {
                if (s.Updatable == loop)
                {
                    Unschedule(s as ScheduledElement);
                    return;
                }
            }
        }

        public void Unschedule(ScheduledElement func)
        {
            var function = func as InnerScheduledElement;
            if (function == null) return;
            function.Unschedule();
            scheduledList.Remove(function);
        }

        public void Update(GameTime gameTime)
        {
            updateList.Clear();
            foreach (var s in scheduledList)
                updateList.Add(s);
            foreach (var u in updateList)
                u.Update(gameTime);
        }

        #region privates

        #region inner class

        class UpdateHandler : IUpdatable
        {
            public Action<GameTime> func;

            public UpdateHandler(Action<GameTime> func)
            {
                this.func = func;
            }

            public void Update(GameTime time)
            {
                func(time);
            }
        }

        class VoidUpdateHandler : IUpdatable
        {
            public Action func;

            public VoidUpdateHandler(Action func)
            {
                this.func = func;
            }

            public void Update(GameTime time)
            {
                func();
            }
        }

        class InnerScheduledElement : IUpdatable, ScheduledElement
        {
            public InnerScheduledElement(IUpdatable updatable, Scheduler owner)
            {
                Debug.Assert(owner != null);
                Debug.Assert(updatable != null);
                this.Owner = owner;
                this.Updatable = updatable;
                this.Loop = false;
                this.Alive = false;
            }

            public void ScheduleRepeat(Second interval)
            {
                Debug.Assert(!Alive);
                Debug.Assert(interval >= 0);
                Loop = true;
                Alive = true;
                delay = this.interval = interval;
            }

            public void ScheduleOnce(Second delay)
            {
                Debug.Assert(!Alive);
                Loop = false;
                Alive = true;
                this.delay = interval = delay;
            }

            public void Unschedule()
            {
                Alive = false;
            }

            public void Update(GameTime gameTime)
            {
                if (Alive)
                {
                    if (interval == 0)
                    {
                        Updatable.Update(gameTime);
                    }
                    else
                    {
                        delay -= gameTime.DeltaTime;
                        if (delay < 0)
                        {
                            if (Loop)
                            {
                                while (delay < 0)
                                {
                                    Updatable.Update(gameTime);
                                    delay += interval;
                                }
                            }
                            else
                            {
                                Updatable.Update(gameTime);
                                Owner.Unschedule(this as ScheduledElement);
                            }
                        }
                    }
                }
            }

            public readonly IUpdatable Updatable;
            public readonly Scheduler Owner;
            public bool Loop { get; private set; }
            public bool Alive { get; private set; }

            private Second delay = (Second)0;
            private Second interval = (Second)0;
        }

        #endregion

        List<InnerScheduledElement> updateList = new List<InnerScheduledElement>();
        List<InnerScheduledElement> scheduledList = new List<InnerScheduledElement>();

        #endregion
    }
}
