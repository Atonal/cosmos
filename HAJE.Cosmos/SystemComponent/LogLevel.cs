﻿
using System.Diagnostics;
namespace HAJE.Cosmos.SystemComponent
{
    public enum LogLevel
    {
        Verbose,
        Trace,
        Debug,
        Info,
        Warning,
        Error,
        Fatal
    }

    public static class LogLevelExtenstion
    {
        public static string ToShortString(this LogLevel level)
        {
            switch (level)
            {
                case LogLevel.Verbose:
                    return "v";
                case LogLevel.Trace:
                    return "t";
                case LogLevel.Debug:
                    return "d";
                case LogLevel.Info:
                    return "i";
                case LogLevel.Warning:
                    return "W";
                case LogLevel.Error:
                    return "E";
                case LogLevel.Fatal:
                    return "F";
                default:
                    Debug.Assert(false);
                    return "?";
            }
        }
    }
}
