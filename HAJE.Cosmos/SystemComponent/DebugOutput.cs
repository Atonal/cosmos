﻿using System;
using System.Drawing;
using System.Collections.Generic;
using OpenTK;

namespace HAJE.Cosmos.SystemComponent
{
    public class DebugOutput
    {
        public enum Visiblity
        {
            Full,
            WatchOnly,
            None
        }

        public DebugOutput()
        {
            InitDefault();

            if (GameSystem.BuildInfo.DebugBuild)
                this.visible = Visiblity.WatchOnly;
        }

        public void SetWatch(string label, object text)
        {
            SetWatch(label, text, Color.Gray);
        }

        public void SetWatch(string label, object text, Color color)
        {
            if (watchLabelList.ContainsKey(label))
            {
                watchLabelList[label].SetText(label + ": " + text);
                watchLabelList[label].SetColor(color);
            }
            else
            {                
                top -= lineHeight*2;
                UI.Label watchLabel = new UI.Label(label+": "+text, new Vector2(left, top), lineHeight);
                fpsLabel.SetColor(color);
                watchLabelList.Add(label, watchLabel);
            }
        }

        void DrawFPS()
        {
            var fps = GameSystem.Profiler.Fps.Average;

            var fpsMsg = string.Format("fps: {0,-6}  ({1,-6}-{2,6})",
                fps.ToString("F1"),
                GameSystem.Profiler.Fps.Minimum.ToString("F1"),
                GameSystem.Profiler.Fps.Maximum.ToString("F1"));
            fpsLabel.SetText(fpsMsg);
            fpsLabel.DrawTextTest();
        }

        void DrawFrames()
        {
            float frameInterval = 1000.0f / GameSystem.Profiler.Fps.Average;
            var frameMsg = "frame interval: " + frameInterval.ToString("F1") + " ms";
            frameLabel.SetText(frameMsg);
            frameLabel.DrawTextTest();
        }
        
        void DrawWatches()
        {
            foreach (var w in watchLabelList.Values)
            {
                w.DrawTextTest();
            }
        }

        void DrawLogs()
        {
            int logIndex = 0;
            float viewHeight = GameSystem.Viewport.Y - 20;
            int maxLogs = (int)(viewHeight / (2*lineHeight));
            var recent = GameSystem.Log.Recent;
            int logFrom = Math.Max(0, recent.Count- maxLogs);
            foreach (LogMessage log in recent)
            {
                if (logIndex >= logFrom)
                {
                    logLabelList[logIndex - logFrom].SetText(log.ToString());
                    logLabelList[logIndex - logFrom].DrawTextTest();
                }
                logIndex++;
                
            }
        }

        public void Draw()
        {
            if (visible == Visiblity.Full)
            {
                DrawFPS();
                DrawFrames();
                DrawWatches();
                DrawLogs();
            }
            if (visible == Visiblity.WatchOnly)
            {
                DrawFPS();
                DrawFrames();
                DrawWatches();
            }
        }      
        
        void InitDefault()
        {
            fpsLabel = new UI.Label("Default FPS", new Vector2(left, top), lineHeight);
            fpsLabel.SetColor(Color.Red);
            top -= lineHeight * 3;
            frameLabel = new UI.Label("Default Frame", new Vector2(left, top), lineHeight);
            frameLabel.SetColor(Color.DarkRed);
            top -= lineHeight * 3;

            //Watch 가 얼마나 생성될지 모른다.
            //top += lineHeight * 3;
            //watchLabel = new UI.Label("Default Watch", new Vector2(left, top), 10);
            //watchLabel.SetFgColor(Color.Gray);

            //Log는 MaxLogs 만큼 만들어두자
            float viewHeight = GameSystem.Viewport.Y-20;
            int maxLogs = (int)(viewHeight / (2 * lineHeight));
            
            for (int i = 0; i < maxLogs; i++)
            {
                UI.Label logLabel = new UI.Label("Default Log", new Vector2(left + 350, viewHeight - i * lineHeight*2), lineHeight);
                logLabelList.Add(i,logLabel);
            }
            
        }

        Dictionary<string, UI.Label> watchLabelList = new Dictionary<string, UI.Label>();
        Dictionary<int, UI.Label> logLabelList = new Dictionary<int, UI.Label>();

        UI.Label fpsLabel;
        UI.Label frameLabel;
        
        public Visiblity visible;
        private const int lineHeight = 10;
        private float top = GameSystem.Viewport.Y-20;
        private float left = 5;
    }
}
