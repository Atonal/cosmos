﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.Cosmos.SystemComponent
{
    public delegate void LogHandler(LogMessage log);

    public class LogSystem
    {
        public event LogHandler OnMessage = delegate { };

        public void Write(string message)
        {
            Write(LogLevel.Verbose, message);
        }

        public void Write(LogLevel level, string message)
        {
            Write(level, "cosmos", message);
        }

        public void Write(LogLevel level, string module, string message)
        {
            Debug.Assert(!isSendingOnMessage);
            lock (this)
            {
                LogMessage log;
                if (Recent.Count >= MaxRecent)
                    log = Recent.Dequeue();
                else
                    log = new LogMessage();
                log.TimeStamp = DateTime.Now;
                log.Level = level;
                log.Module = module;
                log.Message = message;
                Recent.Enqueue(log);
                isSendingOnMessage = true;
                OnMessage(log);
                isSendingOnMessage = false;
            }
        }

        public Queue<LogMessage> Recent = new Queue<LogMessage>();

        const int MaxRecent = 1000;
        bool isSendingOnMessage = false;
    }
}
