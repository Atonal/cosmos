﻿
namespace HAJE.Cosmos.SystemComponent
{
    public class BuildInfo
    {
#if DEBUG
        public readonly bool DebugBuild = true;
#else
        public readonly bool DebugBuild = false;
#endif
    }
}
