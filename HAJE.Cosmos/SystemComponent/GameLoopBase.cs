﻿using System.Diagnostics;

namespace HAJE.Cosmos.SystemComponent
{
    public abstract class GameLoopBase
    {
        public GameLoopBase(string debugName)
        {
            Status = GameLoopStatus.Created;
            Name = debugName;
        }

        public void Initialize()
        {
            OnInitialize();
            Debug.Assert(Status == GameLoopStatus.Created);
            Status = GameLoopStatus.Initialized;
        }

        protected virtual void OnInitialize()
        {
        }

        public void Run()
        {
            if (Status == GameLoopStatus.Created)
                Initialize();
            OnRun();
            Debug.Assert(Status == GameLoopStatus.Initialized);
            Status = GameLoopStatus.Running;
        }

        protected virtual void OnRun()
        {
        }

        public abstract void Update(Second deltaTime);

        public abstract void RenderFrame(Second deltaTime);

        public void Stop()
        {
            Debug.Assert(Status == GameLoopStatus.Running);
            OnStop();
            Status = GameLoopStatus.Exited;
        }

        protected virtual void OnStop()
        {

        }

        public virtual void Dispose(bool manual)
        {
            Status = GameLoopStatus.Disposed;
        }

        public GameLoopStatus Status { get; private set; }
        public readonly string Name;
    }
}
