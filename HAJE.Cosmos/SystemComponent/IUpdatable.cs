﻿
namespace HAJE.Cosmos.SystemComponent
{
    public interface IUpdatable
    {
        void Update(GameTime gameTime);
    }
}
