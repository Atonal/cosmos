﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.Cosmos.SystemComponent
{
    public enum GameLoopStatus
    {
        Created,
        Initialized,
        Running,
        Exited,
        Disposed
    }
}
