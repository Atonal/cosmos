﻿using OpenTK;
using OpenTK.Graphics;
using System;
using System.Drawing;

namespace HAJE.Cosmos.Rendering
{
    public class BillboardRenderer : IDisposable
    {
        public BillboardRenderer(int maxCount)
        {
            builder = new BillboardBuilder(maxCount);
            vbo = VertexBuffer.CreateDynamic(builder.Vertices, ShaderProvider.SimpleTexture);
            ebo = ElementBuffer.Create(builder.Elements);
        }

        public void Begin()
        {
            builder.Seek(0);
        }

        public void Draw(Vector3 position,
            Color4? colorOrWhite, RectangleF? textureUVOrUnitRect,
            Vector2? anchorOrZero, Vector2? sizeOrOne, Radian? rotationOrZero)
        {
            builder.Append(position,
                colorOrWhite, textureUVOrUnitRect,
                anchorOrZero, sizeOrOne, rotationOrZero);
        }

        public void Draw(Vector3 position,
            Color4? colorOrWhite, RectangleF? textureUVOrUnitRect,
            Vector2? anchorOrZero, Vector2? sizeOrOne, Vector2 rotationDirection)
        {
            builder.Append(position,
                colorOrWhite, textureUVOrUnitRect,
                anchorOrZero, sizeOrOne, rotationDirection);
        }

        public void End(ref Matrix4 transform, Texture texture)
        {
            vbo.Bind(builder.Vertices);
            ebo.Bind();
            ShaderProvider.SimpleTexture.Bind(ref transform, texture);
            GLHelper.DrawTriangles(builder.ElementPosition);
        }

        public void Dispose()
        {
            if (vbo != null) vbo.Dispose(); vbo = null;
            if (ebo != null) ebo.Dispose(); ebo = null;
        }

        BillboardBuilder builder;
        VertexBuffer vbo;
        ElementBuffer ebo;
    }
}
