﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Collections.Generic;

namespace HAJE.Cosmos.Rendering
{
    public static class TextureManager /*: IDisposable*/
    {
        /*public TextureManager()
        {
            loadedTextures = new Dictionary<string, Texture>();
        }

        public void Dispose()
        {
            // release all loaded textures
            UnloadAllTextures();
        }*/

        public static Texture GetTexture(string path)
        {
            // check if we got texture already
            Texture texture;
            if (loadedTextures.TryGetValue(path, out texture))
            {
                return texture;
            }

            // if not, create new texture, keep and return it
            texture = Texture.CreateFromFile(path);
            loadedTextures.Add(path, texture);
            return texture;
        }

        public static void UnloadAllTextures()
        {
            foreach (Texture texture in loadedTextures.Values)
            {
                texture.Dispose();
            }
            loadedTextures.Clear();
        }

        private static Dictionary<string, Texture> loadedTextures  = new Dictionary<string, Texture>();
    }    
}
