﻿using OpenTK;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.Cosmos.Rendering.ParticleSystem
{
    public class ExplosionParticle : ParticleBase
    {
        public ExplosionParticle()
        {
        }

        public override void Update(FixedPoint deltaTimeInSec)
        {
            this.Color.A -= deltaTimeInSec.FloatingValue / TimeToLive;

            Position += Velocity * deltaTimeInSec.FloatingValue;
            TimeToLive -= deltaTimeInSec.FloatingValue;
        }
    }
}
