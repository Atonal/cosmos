﻿using HAJE.Cosmos.Combat;
using HAJE.Cosmos.Combat.Physics;
using OpenTK;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.Cosmos.Rendering.ParticleSystem
{
    public class DefaultParticleEmitter
    {
        private CombatContext context;
        private ParticleEmitterDescriptor[] Descriptors;
        private Collider collider;
        private Random rd;

        public DefaultParticleEmitter(CombatContext Context, Collider Collider, ParticleEmitterDescriptor[] Descriptors)
        {
            this.context = Context;
            this.collider = Collider;
            this.Descriptors = Descriptors;

            this.rd = new Random();
        }

        public void Update(FixedPoint deltaTimeInSec)
        {
            foreach (var desc in Descriptors)
                for (int i = 0; i < desc.Count; i++)
                {
                    ParticleBase particle = context.ParticleManager.GetParticle(desc.ParticleType);
                    float rad = rd.NextFloat(-desc.Angle, desc.Angle);
                    Vector2 velocity = this.collider.Velocity.Rotate((FixedPoint)rad).ToVector2();
                
                    if (velocity.Length != 0)
                        velocity.Normalize();

                    particle.Emit(
                        velocity * rd.NextFloat(desc.MinSpeed, desc.MaxSpeed),
                        (this.collider.Position.Simulation - this.collider.Velocity * deltaTimeInSec * rd.NextFixedPoint(1)).ToVector2(),
                        desc);
                }
        }
    }
}
