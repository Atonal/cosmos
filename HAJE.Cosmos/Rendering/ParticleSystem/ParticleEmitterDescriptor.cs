﻿using OpenTK.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAJE.Cosmos.Rendering.ParticleSystem
{
    public class ParticleEmitterDescriptor
    {
        public Int32 Count;
        public float TimeToLive;
        public float Radius;
        public float Angle;
        public float MinSpeed;
        public float MaxSpeed;
        public Color4 Color;
        public float RelativeDepth;
        public Texture Texture;

        public Type ParticleType;

        public static ParticleEmitterDescriptor[] MissileEmitter = 
            new ParticleEmitterDescriptor[]
            {
                new ParticleEmitterDescriptor()
                {
                    Count = 5,
                    TimeToLive = 0.5f,
                    Radius = 1,
                    Angle = (float)Math.PI / 2,
                    MinSpeed = 5,
                    MaxSpeed = 25,
                    Color = new Color4(1f, 0f, 0f, 1f),
                    RelativeDepth = 10,
                    Texture = TextureManager.GetTexture("Resource/CircleParticle.png"),
                    ParticleType = typeof(DefaultParticle)
                }
            };
        public static ParticleEmitterDescriptor[] GetExplosionEmitter(FixedPoint[] Range)
        {
            float timeToLive = 0.5f;

            return new ParticleEmitterDescriptor[]
            {
                new ParticleEmitterDescriptor()
                {
                    Count = 16,
                    TimeToLive = timeToLive,
                    Radius = 14,
                    Angle = (float)Math.PI * 2,
                    MinSpeed = Range[1].FloatingValue / timeToLive,
                    MaxSpeed = Range[2].FloatingValue / timeToLive,
                    Color = Color4.OrangeRed,
                    RelativeDepth = -20,
                    Texture = TextureManager.GetTexture("Resource/CircleParticle.png"),
                    ParticleType = typeof(ExplosionParticle)
                },
                new ParticleEmitterDescriptor()
                {
                    Count = 16,
                    TimeToLive = timeToLive,
                    Radius = 8,
                    Angle = (float)Math.PI * 2,
                    MinSpeed = Range[0].FloatingValue / timeToLive,
                    MaxSpeed = Range[1].FloatingValue / timeToLive,
                    Color = Color4.Yellow,
                    RelativeDepth = -15,
                    Texture = TextureManager.GetTexture("Resource/CircleParticle.png"),
                    ParticleType = typeof(ExplosionParticle)
                },
                new ParticleEmitterDescriptor()
                {
                    Count = 4,
                    TimeToLive = timeToLive,
                    Radius = 6,
                    Angle = (float)Math.PI * 2,
                    MinSpeed = 0,
                    MaxSpeed =  Range[0].FloatingValue / timeToLive,
                    Color = Color4.LightYellow,
                    RelativeDepth = -10,
                    Texture = TextureManager.GetTexture("Resource/CircleParticle.png"),
                    ParticleType = typeof(ExplosionParticle)
                },
                new ParticleEmitterDescriptor()
                {
                    Count = 4,
                    TimeToLive = timeToLive,
                    Radius = 12,
                    Angle = (float)Math.PI * 2,
                    MinSpeed = Range[2].FloatingValue / timeToLive,
                    MaxSpeed = Range[2].FloatingValue / timeToLive * 1.5f,
                    Color = Color4.Yellow,
                    RelativeDepth = -17,
                    Texture = TextureManager.GetTexture("Resource/EclipseParticle.png"),
                    ParticleType = typeof(ExplosionParticle)
                }
            };
        }
    }
}
