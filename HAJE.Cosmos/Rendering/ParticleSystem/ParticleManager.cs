﻿using HAJE.Cosmos.Combat;
using HAJE.Cosmos.Rendering;
using OpenTK;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace HAJE.Cosmos.Rendering.ParticleSystem
{
    public class ParticleManager : IDisposable
    {
        private CombatContext context;
        private BillboardRenderer billboardRenderer;
        private Int32 particleCount;
        public Int32 maxParticleCount;

        private Dictionary<Type, List<ParticleBase>> aliveList = new Dictionary<Type, List<ParticleBase>>();
        private Dictionary<Type, Queue<ParticleBase>> garbage = new Dictionary<Type, Queue<ParticleBase>>();
        private Dictionary<Type, ConstructorInfo> ctorList = new Dictionary<Type, ConstructorInfo>();

        public ParticleManager(CombatContext context)
        {
            this.context = context;
            this.particleCount = 0;
            this.maxParticleCount = 10000;

            this.billboardRenderer = new BillboardRenderer(maxParticleCount);
        }

        public ParticleBase GetParticle(Type t)
        {
            if (garbage.ContainsKey(t) == false)
            {
                aliveList.Add(t, new List<ParticleBase>());
                garbage.Add(t, new Queue<ParticleBase>());
                ctorList.Add(t, t.GetConstructor(new Type[]{}));
            }

            ParticleBase p;
            if (garbage[t].Count == 0)
                p = ctorList[t].Invoke(new object[]{}) as ParticleBase;
            else
                p = garbage[t].Dequeue();

            aliveList[t].Add(p);
            return p;
        }
        private void Recycle()
        {
            foreach (var type in aliveList.Keys)
                for (int i = 0; i < aliveList[type].Count; i++)
                    if (aliveList[type][i].TimeToLive < 0)
                    {
                        garbage[type].Enqueue(aliveList[type][i]);
                        aliveList[type].RemoveAt(i);

                        i--;
                    }
        }

        public void Join()
        {
            Recycle();

            GameSystem.DebugOutput.SetWatch("Count of Particle", aliveList.Count);
            GameSystem.DebugOutput.SetWatch("Count of Garbage", garbage.Count);
        }
        public void Update(FixedPoint deltaTimeInSec)
        {
            foreach (var kvp in aliveList)
                foreach (var elem in kvp.Value)
                    elem.Update(deltaTimeInSec);
        }

        public void Render()
        {
            Vector2 anchor = new Vector2(0.5f, 0.5f);
            Dictionary<Texture, List<ParticleBase>> dic = new Dictionary<Texture, List<ParticleBase>>();
            foreach (var kvp in aliveList)
                foreach (var elem in kvp.Value)
                {
                    if (dic.ContainsKey(elem.Texture) == false)
                        dic.Add(elem.Texture, new List<ParticleBase>());
                    dic[elem.Texture].Add(elem);
                }

            foreach (var list in dic)
            {
                billboardRenderer.Begin();
                foreach (var elem in list.Value)
                {
                    Vector2 radius = new Vector2(2 * elem.Radius, 2 * elem.Radius);
                    if (particleCount < maxParticleCount)
                    {
                        billboardRenderer.Draw(
                            new Vector3(elem.Position.X, elem.Position.Y, ZOrder.GameDepth + elem.RelativeDepth),
                            elem.Color,
                            null,
                            anchor,
                            radius,
                            elem.Angle);

                        particleCount++;
                    }
                    else
                    {
                        billboardRenderer.End(ref context.Camera.Matrix, elem.Texture);
                        billboardRenderer.Dispose();
                        billboardRenderer = new BillboardRenderer(maxParticleCount);
                        billboardRenderer.Begin();
                        particleCount = 0;
                    }
                }
                billboardRenderer.End(ref context.Camera.Matrix, list.Key);
            }
        }

        public void Dispose()
        {
            this.billboardRenderer.Dispose();
        }
    }
}
