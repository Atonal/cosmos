﻿using HAJE.Cosmos.Combat;
using HAJE.Cosmos.Combat.Physics;
using OpenTK;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.Cosmos.Rendering.ParticleSystem
{
    public class DefaultParticle : ParticleBase
    {
        public DefaultParticle()
        {
        }

        public override void Update(FixedPoint deltaTimeInSec)
        {
            this.Color.A *= 0.8f;
            Position += Velocity * deltaTimeInSec.FloatingValue;
            TimeToLive -= deltaTimeInSec.FloatingValue;
        }
    }
}
