﻿using OpenTK;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.Cosmos.Rendering.ParticleSystem
{
    public class ParticleBase
    {
        public Vector2 Position;
        public Vector2 Velocity;
        public float Radius;
        public Color4 Color;
        public float TimeToLive;
        public float RelativeDepth;
        public Texture Texture;
        public Radian Angle;

        public void Emit(Vector2 Velocity, Vector2 Position, ParticleEmitterDescriptor Descriptor)
        {
            this.Velocity = Velocity;
            this.Position = Position;
            this.Angle = new Radian((float)Math.Atan2(this.Velocity.Y, this.Velocity.X));

            this.Radius = Descriptor.Radius;
            this.Color = Descriptor.Color;
            this.TimeToLive = Descriptor.TimeToLive;
            this.RelativeDepth = Descriptor.RelativeDepth;
            this.Texture = Descriptor.Texture;
        }
        public virtual void Update(FixedPoint deltaTimeInSec) { }
    }
}
