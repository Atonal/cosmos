﻿using HAJE.Cosmos.Rendering;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.Drawing;

namespace HAJE.Cosmos
{
    public class MainForm : GameWindow
    {
        public MainForm()
            : base(1280, 720,
                GraphicsMode.Default, "Cosmos Online", GameWindowFlags.FixedWindow,
                DisplayDevice.Default, 3, 2,
                GraphicsContextFlags.ForwardCompatible | GraphicsContextFlags.Debug)
        {
            VSync = VSyncMode.Off;
            X = 0;
            Y = 0;
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            GameSystem.UpdateTick();  
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            GL.ClearColor(Color4.Black);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.Enable(EnableCap.Blend);
            GameSystem.RenderTick();
            SwapBuffers();
            GLHelper.CheckGLError();
        }

        public Vector2 GetMousePosition()
        {
            var cursor = OpenTK.Input.Mouse.GetCursorState();
            var scrPnt = new Point(cursor.X, cursor.Y);
            var cliPnt = PointToClient(scrPnt);
            cliPnt.Y = Height - cliPnt.Y;
            return new Vector2(cliPnt.X, cliPnt.Y);
        }
    }
}
