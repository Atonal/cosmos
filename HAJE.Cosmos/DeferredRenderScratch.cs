﻿using HAJE.Cosmos.Combat;
using HAJE.Cosmos.Rendering;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;

namespace HAJE.Cosmos
{
    public class DeferredRenderScratch : IDisposable
    {
        public DeferredRenderScratch()
        {
            color = Texture.CreateFromFile("Resource/Fighter/FighterColor.png");
            depth = Texture.CreateFromFile("Resource/Fighter/FighterDepth.png");
            material = Texture.CreateFromFile("Resource/Fighter/FighterMaterial.png");
            shader = new Shader();

            builder = new BillboardBuilder(1);
            ebo = ElementBuffer.Create(builder.Elements);
            vbo = VertexBuffer.CreateDynamic(builder.Vertices, shader);
        }

        public void Draw(Camera camera, Second deltaTime)
        {
            rotation -= (Radian)(float)deltaTime;

            Vector3 pos = new Vector3(camera.Position);
            pos.Z = ZOrder.GameDepth;
            builder.Seek(0);
            builder.Append(pos, null, null, new Vector2(0.5f, 0.5f), new Vector2(32, 32), rotation);

            shader.Bind(ref camera.Matrix, color, depth, material);
            vbo.Bind(builder.Vertices);
            ebo.Bind();
            GLHelper.DrawTriangles(ebo.Count);
            shader.Unbind();
        }

        public void Dispose()
        {
            if (ebo != null) ebo.Dispose(); ebo = null;
            if (vbo != null) vbo.Dispose(); vbo = null;

            if (color != null) color.Dispose(); color = null;
            if (depth != null) depth.Dispose(); depth = null;
            if (material != null) material.Dispose(); material = null;
        }

        #region privates

        class Shader : ShaderBase
        {
            const string vertexSource = @"
                #version 150 core
                
                uniform mat4 trans;

                in vec3 positionIn;
                in vec4 colorIn;
                in vec2 textureIn;
                
                out vec4 color;
                out vec2 texCoord;
                
                void main()
                {
                    color = colorIn;
                    texCoord = textureIn;
                    gl_Position = trans * vec4(positionIn, 1);
                }
            ";

            const string fragmentSource = @"
                #version 150 core

                uniform sampler2D colorTex;
                uniform sampler2D depthTex;
                uniform sampler2D materialTex;
                
                uniform vec2 deltaTex;
                uniform vec3 lightDir = normalize(vec3(-1, 1, 1));
                uniform vec3 cameraPos = normalize(vec3(0, 0, 1));

                uniform float BumpUnit = 15.0f;
                uniform float MaxDiffuse = 2.0f;
                uniform float MaxAmbient = 2.0f;
                uniform float MaxIntensity = 2.0f;
                uniform float MaxShine = 8.0f;

                in vec4 color;
                in vec2 texCoord;

                out vec4 colorOut;

                void main()
                {
                    float unitU = deltaTex.x;
                    float unitV = deltaTex.y;
                    vec4 center = texture(depthTex, texCoord + vec2(0, 0));
                    vec4 left = texture(depthTex, texCoord + vec2(-unitU, 0));
                    vec4 right = texture(depthTex, texCoord + vec2(unitU, 0));
                    vec4 top = texture(depthTex, texCoord + vec2(0, unitV));
                    vec4 bottom = texture(depthTex, texCoord + vec2(0, -unitV));
                    vec3 dx = vec3( 2, 0, (right.x - left.x) * 256 / BumpUnit );
                    vec3 dy = vec3( 0, 2, (top.y - bottom.y) * 256 / BumpUnit );
                    vec3 normal = normalize(cross(dx, dy));

                    vec4 material = texture(materialTex, texCoord);

                    // ambient                    
                    float ambient = (1 - material.a);

                    // diffuse
                    float lightAmount = max(dot(normal, lightDir), 0);
                    float diffuse = lightAmount * material.x * MaxDiffuse;

                    // specular
                    vec3 reflect = normalize(2 * dot(normal, lightDir) * normal - lightDir);
                    vec3 view = normalize(cameraPos - vec3((texCoord.x - 0.5) / unitU, (texCoord.y - 0.5) / unitV, 0));
                    lightAmount = max(dot(view, reflect), 0);
                    float intensity = material.g * MaxIntensity;
                    float shine = material.b * MaxShine;
                    float specular = pow(lightAmount, shine) * intensity;

                    vec4 textureColor = texture(colorTex, texCoord);
                    colorOut = vec4(textureColor.xyz * (ambient + diffuse) + specular * color.xyz,
                                    textureColor.w);
                }
            ";

            public Shader()
                : base(vertexSource, fragmentSource)
            {
            }

            protected override void GetUniformLocation()
            {
                transPtr = GL.GetUniformLocation(ShaderProgram, "trans");
                colorTexPtr = GL.GetUniformLocation(ShaderProgram, "colorTex");
                depthTexPtr = GL.GetUniformLocation(ShaderProgram, "depthTex");
                materalTexPtr = GL.GetUniformLocation(ShaderProgram, "materialTex");
                deltaTexPtr = GL.GetUniformLocation(ShaderProgram, "deltaTex");
            }

            public void Bind(ref Matrix4 transform, Texture color, Texture depth, Texture material)
            {
                base.Bind();
                GL.UniformMatrix4(transPtr, false, ref transform);

                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, color.TextureHandle);
                GL.ActiveTexture(TextureUnit.Texture1);
                GL.BindTexture(TextureTarget.Texture2D, depth.TextureHandle);
                GL.ActiveTexture(TextureUnit.Texture2);
                GL.BindTexture(TextureTarget.Texture2D, material.TextureHandle);
                GL.Uniform1(colorTexPtr, 0);
                GL.Uniform1(depthTexPtr, 1);
                GL.Uniform1(materalTexPtr, 2);
                GL.Uniform2(deltaTexPtr, new Vector2(1.0f / color.Size.X, 1.0f / color.Size.Y));
            }

            public void Unbind()
            {
                GL.ActiveTexture(TextureUnit.Texture0);
            }

            int transPtr;
            int colorTexPtr;
            int depthTexPtr;
            int materalTexPtr;
            int deltaTexPtr;
        }

        Texture color;
        Texture depth;
        Texture material;
        Shader shader;
        VertexBuffer vbo;
        ElementBuffer ebo;
        BillboardBuilder builder;
        Radian rotation;
        
        #endregion
    }
}
