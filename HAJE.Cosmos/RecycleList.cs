﻿using System.Collections.Generic;

namespace HAJE.Cosmos
{
    public class RecycleList<T>
        where T : new()
    {
        public T NewItem()
        {
            T e;
            if (garbage.Count == 0)
                e = new T();
            else
                e = garbage.Dequeue();
            list.Add(e);
            return e;
        }

        public void Clear()
        {
            foreach (var e in list)
                garbage.Enqueue(e);
            list.Clear();
        }

        public void Recycle(T e)
        {
            list.Remove(e);
            garbage.Enqueue(e);
        }

        /// <summary>
        /// GC Pressure 없이 foreach를 수행하기 위한 인터페이스
        /// 이외의 용도로 사용 금지
        /// </summary>
        public List<T> List
        {
            get
            {
                return list;
            }
        }

        public int Count
        {
            get
            {
                return list.Count;
            }
        }

        List<T> list = new List<T>();
        Queue<T> garbage = new Queue<T>();
    }
}
