﻿
namespace HAJE.Cosmos.Network
{
    /// <summary>
    /// Host -> Client로 가는 패킷 목록
    /// </summary>
    public enum PacketHC
    {
        SessionInfo,
        InputFrame
    }
}
