﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Net;

namespace HAJE.Cosmos.Network
{
    /// <summary>
    /// 플레이어 인덱스를 발급/삭제하고 세션에 필요한 플레이어 정보를 담는 객체
    /// </summary>
    public class PlayerList
    {
        public PlayerList(int maxPlayer)
        {
            infoArray = new PlayerInfo[maxPlayer];
        }

        public int AddPlayer(IPEndPoint endPoint, NetConnection connection)
        {
            for (int i = 0; i < infoArray.Length; i++)
            {
                if (infoArray[i] == null)
                {
                    infoArray[i] = new PlayerInfo(endPoint, connection, i);
                    infoByEndPoint.Add(endPoint, infoArray[i]);
                    return i;
                }
            }

            throw new IndexOutOfRangeException("더 이상 플레이어 인덱스를 할당 할 수 없습니다.");
        }

        public void AddPlayer(PlayerInfo playerInfo)
        {
            var index = playerInfo.PlayerIndex;
            if (infoArray[index] != null)
                throw new IndexOutOfRangeException("이미 할당된 플레이어 인덱스입니다.");
            infoArray[index] = playerInfo;
        }

        public int ResolvePlayerIndex(NetIncomingMessage message)
        {
            return infoByEndPoint[message.SenderEndPoint].PlayerIndex;
        }

        public PlayerInfo this[int index]
        {
            get
            {
                return infoArray[index];
            }
        }

        //TODO: Remove Player 구현

        PlayerInfo[] infoArray;
        Dictionary<IPEndPoint, PlayerInfo> infoByEndPoint = new Dictionary<IPEndPoint, PlayerInfo>();
    }
}
