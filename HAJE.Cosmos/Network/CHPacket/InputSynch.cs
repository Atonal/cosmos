﻿using HAJE.Cosmos.Combat.Synchronization;
using Lidgren.Network;
using System.Diagnostics;

namespace HAJE.Cosmos.Network.CHPacket
{
    public class InputSynch
    {
        public void Begin(NetOutgoingMessage msg, ushort sequence)
        {
            Debug.Assert(this.outMsg == null);
            this.outMsg = msg;
            msg.Write(PacketCH.InputSynch);
            msg.Write(sequence);
        }

        public ushort Begin(NetIncomingMessage msg)
        {
            Debug.Assert(this.inMsg == null);
            this.inMsg = msg;
            var code = msg.ReadPackCH();
            Debug.Assert(code == PacketCH.InputSynch);
            return msg.ReadUInt16();
        }

        public void AppendPressed(SynchKey key)
        {
            outMsg.Write(InputEvent.Pressed);
            outMsg.Write(key);
        }

        public void AppendReleased(SynchKey key)
        {
            outMsg.Write(InputEvent.Released);
            outMsg.Write(key);
        }

        public void AppendDouble(SynchKey key)
        {
            outMsg.Write(InputEvent.DoublePressed);
            outMsg.Write(key);
        }

        public bool ReadEvent(out InputEvent eventType, ref SynchKey key)
        {
            eventType = inMsg.PeekInputEvent();
            if (!(eventType == InputEvent.Pressed
                || eventType == InputEvent.DoublePressed
                || eventType == InputEvent.Released))
            {
                return false;
            }
            eventType = inMsg.ReadInputEvent();
            key = inMsg.ReadSynchKey();
            return true;
        }

        public void End(FixedVector2 cursor, InputSet finalState)
        {
            outMsg.Write(InputEvent.State);
            outMsg.Write(cursor);
            outMsg.Write(finalState.Vector);
            this.outMsg = null;
        }

        public void End(out FixedVector2 cursor, ref InputSet finalState)
        {
            inMsg.ReadInputEvent();
            cursor = inMsg.ReadFixedVector2();
            finalState.Vector = inMsg.ReadUInt16();
            this.inMsg = null;
        }

        NetOutgoingMessage outMsg;
        NetIncomingMessage inMsg;
    }
}
