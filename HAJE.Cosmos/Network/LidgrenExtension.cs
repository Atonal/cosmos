﻿using HAJE.Cosmos.Combat.Synchronization;
using Lidgren.Network;

namespace HAJE.Cosmos.Network
{
    public static class LidgrenExtension
    {
        #region PacketCH

        public static void Write(this NetOutgoingMessage msg, PacketCH value)
        {
            msg.Write((short)value);
        }

        public static PacketCH PeekPacketCH(this NetIncomingMessage msg)
        {
            return (PacketCH)msg.PeekInt16();
        }

        public static PacketCH ReadPackCH(this NetIncomingMessage msg)
        {
            return (PacketCH)msg.ReadInt16();
        }

        #endregion

        #region PacketHC

        public static void Write(this NetOutgoingMessage msg, PacketHC value)
        {
            msg.Write((short)value);
        }

        public static PacketHC PeekPacketHC(this NetIncomingMessage msg)
        {
            return (PacketHC)msg.PeekInt16();
        }

        public static PacketHC ReadPackHC(this NetIncomingMessage msg)
        {
            return (PacketHC)msg.ReadInt16();
        }

        #endregion

        #region InputEvent

        public static void Write(this NetOutgoingMessage msg, InputEvent value)
        {
            msg.Write((byte)value);
        }

        public static InputEvent ReadInputEvent(this NetIncomingMessage msg)
        {
            return (InputEvent)msg.ReadByte();
        }

        public static InputEvent PeekInputEvent(this NetIncomingMessage msg)
        {
            return (InputEvent)msg.PeekByte();
        }

        #endregion

        #region SynchKey

        public static void Write(this NetOutgoingMessage msg, SynchKey value)
        {
            msg.Write((byte)value);
        }

        public static SynchKey ReadSynchKey(this NetIncomingMessage msg)
        {
            return (SynchKey)msg.ReadByte();
        }

        #endregion

        #region FixedPoint

        public static void Write(this NetOutgoingMessage msg, FixedPoint value)
        {
            msg.Write(value.InternalValue);
        }

        public static FixedPoint ReadFixedPoint(this NetIncomingMessage msg)
        {
            FixedPoint ret = new FixedPoint();
            ret.InternalValue = msg.ReadInt64();
            return ret;
        }

        #endregion

        #region FixedVector2

        public static void Write(this NetOutgoingMessage msg, FixedVector2 value)
        {
            msg.Write(value.X);
            msg.Write(value.Y);
        }

        public static FixedVector2 ReadFixedVector2(this NetIncomingMessage msg)
        {
            FixedVector2 ret;
            ret.X = msg.ReadFixedPoint();
            ret.Y = msg.ReadFixedPoint();
            return ret;
        }

        #endregion
    }
}
