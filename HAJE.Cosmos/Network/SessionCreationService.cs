﻿using HAJE.Cosmos.SystemComponent;
using Lidgren.Network;
using System;
using System.Diagnostics;

namespace HAJE.Cosmos.Network
{
    public delegate void SessionCreationHandler(Session session);

    public class SessionCreationService : IUpdatable
    {
        public void Start(SessionCreationInfo info)
        {
            log = GameSystem.Log;
            WriteLog(LogLevel.Info, "시작");

            var config = new NetPeerConfiguration(SessionCreationInfo.AppName);
            config.MaximumConnections = info.MaxPlayer;
            if (GameSystem.BuildInfo.DebugBuild || Debugger.IsAttached)
                config.ConnectionTimeout = 600.0f;
            else
                config.ConnectionTimeout = 5.0f;
            config.AutoFlushSendQueue = true;
            config.EnableMessageType(NetIncomingMessageType.DebugMessage);
            config.EnableMessageType(NetIncomingMessageType.VerboseDebugMessage);
            config.EnableMessageType(NetIncomingMessageType.DiscoveryRequest);
            config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);
            config.Port = SessionCreationInfo.Port;
            server = new NetServer(config);
            server.Start();

            config = new NetPeerConfiguration(SessionCreationInfo.AppName);
            if (GameSystem.BuildInfo.DebugBuild || Debugger.IsAttached)
                config.ConnectionTimeout = 600.0f;
            else
                config.ConnectionTimeout = 5.0f;
            config.AutoFlushSendQueue = true;
            config.EnableMessageType(NetIncomingMessageType.DebugMessage);
            config.EnableMessageType(NetIncomingMessageType.VerboseDebugMessage);
            client = new NetClient(config);
            client.Start();
            client.Connect("localhost", SessionCreationInfo.Port);

            session = new Session(server, client);
            session.MaxPlayer = info.MaxPlayer;
            session.PlayerList = new PlayerList(info.MaxPlayer);

            scheduler = new Scheduler("SessionCreationService");
            GameSystem.Scheduler.Schedule(scheduler);
            scheduler.Schedule(this);
        }

        public event SessionCreationHandler OnFinished = delegate { };

        public void Update(GameTime gameTime)
        {
            NetIncomingMessage msg;
            while ((msg = server.ReadMessage()) != null)
            {
                switch (msg.MessageType)
                {
                    case NetIncomingMessageType.DebugMessage:
                    case NetIncomingMessageType.ErrorMessage:
                    case NetIncomingMessageType.WarningMessage:
                    case NetIncomingMessageType.VerboseDebugMessage:
                        WriteLog(LogLevel.Verbose, "처리되지 않은 서버 메세지: " + msg.ReadString());
                        break;
                    case NetIncomingMessageType.StatusChanged:
                        NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte();
                        string reason = msg.ReadString();
                        WriteLog(LogLevel.Verbose, "서버 상태 변경: " + reason);
                        if (server.ConnectionsCount == 1 && status == NetConnectionStatus.Connected)
                        {
                            var senderEndPoint = msg.SenderEndPoint;
                            var senderConnection = server.GetConnection(senderEndPoint);
                            session.PlayerIndex = session.PlayerList.AddPlayer(senderEndPoint, senderConnection);
                            OnFinished(session);
                            End();
                        }
                        break;
                    case NetIncomingMessageType.DiscoveryRequest:
                        WriteLog(LogLevel.Warning, "호스트 탐색 요청 무시 됨: " + msg.SenderEndPoint);
                        break;
                    case NetIncomingMessageType.ConnectionApproval:
                        WriteLog(LogLevel.Verbose, "접속 요청: " + msg.SenderEndPoint);
                        if (msg.SenderEndPoint.Address.ToString() == "127.0.0.1"
                            && server.ConnectionsCount == 0)
                        {
                            msg.SenderConnection.Approve();
                            WriteLog(LogLevel.Info, "접속 요청 수락 됨");
                        }
                        else
                        {
                            msg.SenderConnection.Deny();
                            WriteLog(LogLevel.Warning, "접속 요청 거절 됨");
                        }
                        break;
                    case NetIncomingMessageType.Data:
                        WriteLog(LogLevel.Warning, "서버 데이터 수신 됨");
                        break;
                }
                server.Recycle(msg);
            }
        }

        public void Abort()
        {
            client.Shutdown("SessonCreationService Aborted\n");
            server.Shutdown("SessonCreationService Aborted\n");
            server = null;
            client = null;
            End();
        }

        void End()
        {
            GameSystem.Scheduler.Unschedule(scheduler);
            WriteLog(LogLevel.Info, "종료");
        }

        void WriteLog(LogLevel level, string message)
        {
            log.Write(level, "SessionCreationService", message);
        }

        LogSystem log;
        Scheduler scheduler;
        Session session;
        NetServer server;
        NetClient client;
    }
}
