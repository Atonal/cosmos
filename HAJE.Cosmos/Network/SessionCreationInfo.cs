﻿
namespace HAJE.Cosmos.Network
{
    public class SessionCreationInfo
    {
        public const string AppName = "Cosmos Online";
        public const int Port = 8253;

        public int MaxPlayer = 1;
        public int PlayerIndex;
    }
}
