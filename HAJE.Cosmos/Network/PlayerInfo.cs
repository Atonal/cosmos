﻿using Lidgren.Network;
using System.Net;

namespace HAJE.Cosmos.Network
{
    public class PlayerInfo
    {
        /// <summary>
        /// 호스트에서 사용하는 생성자
        /// </summary>
        public PlayerInfo(IPEndPoint endPoint, NetConnection connection, int playerIndex)
        {
            this.EndPoint = endPoint;
            this.Connection = connection;
            this.PlayerIndex = playerIndex;
        }

        /// <summary>
        /// 클라이언트에서 사용하는 생성자
        /// </summary>
        public PlayerInfo(int playerIndex)
        {
            this.PlayerIndex = playerIndex;
        }

        public readonly IPEndPoint EndPoint;
        public readonly NetConnection Connection;
        public readonly int PlayerIndex;
    }
}
