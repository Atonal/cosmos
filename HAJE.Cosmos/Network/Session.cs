﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.Cosmos.Network
{
    /// <summary>
    /// 네트워크로 연결이 유지되는 하나의 게임 세션동안 유지되는 정보,
    /// 세션을 유지하고 관리하기 위한 정보,
    /// 세션을 구성하고 있는 플레이어들의 정보 등을 관장하는 객체
    /// </summary>
    public class Session
    {
        public Session(NetServer server, NetClient client)
        {
            this.Server = server;
            this.Client = client;
            this.IsHost = (server != null);
            if (IsHost)
                this.ServerDispatcher = new NetMessageDispatcher(Server);
            this.ClientDispatcher = new NetMessageDispatcher(Client);
        }

        public readonly bool IsHost;
        public readonly NetServer Server;
        public readonly NetClient Client;
        public readonly NetMessageDispatcher ServerDispatcher;
        public readonly NetMessageDispatcher ClientDispatcher;

        public int MaxPlayer;
        public int PlayerIndex;

        public PlayerList PlayerList;
    }
}
