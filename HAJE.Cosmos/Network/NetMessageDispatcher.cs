﻿using HAJE.Cosmos.SystemComponent;
using Lidgren.Network;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

namespace HAJE.Cosmos.Network
{
    public delegate void NetMessageHandler(NetIncomingMessage msg);

    /// <summary>
    /// NetServer나 NetClinet에 수신된 메시지를 종류별로 분류,
    /// 원하는 객체가 받아갈 수 있도록 메세시를 분배해준다.
    /// </summary>
    public class NetMessageDispatcher : IUpdatable
    {
        public NetMessageDispatcher(NetPeer peer)
        {
            Debug.Assert(peer != null);
            this.peer = peer;
        }

        public void Start(Scheduler parent)
        {
            Debug.Assert(this.parent == null);
            this.parent = parent;
            scheduler = new Scheduler("HostMessageDispatcher");
            parent.Schedule(scheduler);
            scheduler.Schedule(this);
        }

        public event NetMessageHandler OnDebugMessage = delegate { };
        public event NetMessageHandler OnStatusChanged = delegate { };
        public event NetMessageHandler OnDiscoveryRequest = delegate { };
        public event NetMessageHandler OnConnectionApproval = delegate { };

        public void AddDataHandler(NetMessageHandler handler, PacketCH code, object owner)
        {
            AddDataHandler(handler, (short)code, owner);
        }

        public void AddDataHandler(NetMessageHandler handler, PacketHC code, object owner)
        {
            AddDataHandler(handler, (short)code, owner);
        }

        void AddDataHandler(NetMessageHandler handler, short code, object owner)
        {
            Debug.Assert(owner != null);
            Debug.Assert(handler != null);
            Debug.Assert(!dataHandlers.ContainsKey(code));
            dataHandlers.Add(code, new HandlerElement(handler, code, owner));
        }

        public void RemoveDataHandler(short code)
        {
            dataHandlers.Remove(code);
        }

        public void RemoveDataHandler(object owner)
        {
            List<short> tobeRemoved = new List<short>();
            foreach (var e in dataHandlers.Values)
            {
                if (e.Owner == owner)
                {
                    tobeRemoved.Add(e.Code);
                }
            }
            foreach (var c in tobeRemoved)
            {
                dataHandlers.Remove(c);
            }
        }

        class HandlerElement
        {
            public HandlerElement(NetMessageHandler handler, short code, object owner)
            {
                this.Handler = handler;
                this.Code = code;
                this.Owner = owner;
            }
            public readonly NetMessageHandler Handler;
            public readonly short Code;
            public readonly object Owner;
        }
        Dictionary<short, HandlerElement> dataHandlers = new Dictionary<short, HandlerElement>();


        public void Update(GameTime gameTime)
        {
            NetIncomingMessage msg;
            while ((msg = peer.ReadMessage()) != null)
            {
                switch (msg.MessageType)
                {
                    case NetIncomingMessageType.DebugMessage:
                    case NetIncomingMessageType.ErrorMessage:
                    case NetIncomingMessageType.WarningMessage:
                    case NetIncomingMessageType.VerboseDebugMessage:
                        OnDebugMessage(msg);
                        break;
                    case NetIncomingMessageType.StatusChanged:
                        OnStatusChanged(msg);
                        break;
                    case NetIncomingMessageType.DiscoveryRequest:
                        OnDiscoveryRequest(msg);
                        break;
                    case NetIncomingMessageType.ConnectionApproval:
                        OnConnectionApproval(msg);
                        break;
                    case NetIncomingMessageType.Data:
                        var code = msg.PeekInt16();
                        if (dataHandlers.ContainsKey(code))
                        {
                            dataHandlers[code].Handler(msg);
                        }
                        else
                        {
                            GameSystem.Log.Write(LogLevel.Warning, "NetMessageDispatcher", "처리되지 않은 데이터 코드");
                        }
                        break;
                }
                peer.Recycle(msg);
            }
        }

        public void End()
        {
            scheduler.Unschedule(this);
            parent.Unschedule(scheduler);
            parent = null;
        }

        NetPeer peer;
        Scheduler parent;
        Scheduler scheduler;
    }
}
