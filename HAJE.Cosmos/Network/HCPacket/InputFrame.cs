﻿using HAJE.Cosmos.Combat.Synchronization;
using Lidgren.Network;
using System.Diagnostics;

namespace HAJE.Cosmos.Network.HCPacket
{
    public class InputFrame
    {
        public void Begin(NetOutgoingMessage msg, int frameIndex)
        {
            Debug.Assert(this.outMsg == null);
            this.outMsg = msg;
            outMsg.Write(PacketHC.InputFrame);
            outMsg.Write(frameIndex);
        }

        public int Begin(NetIncomingMessage msg)
        {
            Debug.Assert(this.inMsg == null);
            this.inMsg = msg;
            var code = msg.ReadPackHC();
            Debug.Assert(code == PacketHC.InputFrame);
            return msg.ReadInt32();
        }

        public void Append(PlayerInput input)
        {
            outMsg.Write(input.State.Vector);
            outMsg.Write(input.Cursor);
            for (int i = 0; i < SynchKeyExtension.Values.Length; i++)
            {
                var skey = SynchKeyExtension.Values[i];
                if (input.Pressed[skey])
                    outMsg.Write(Pack(InputEvent.Pressed, skey));
                if (input.Released[skey])
                    outMsg.Write(Pack(InputEvent.Released, skey));
                if (input.DoublePressed[skey])
                    outMsg.Write(Pack(InputEvent.DoublePressed, skey));
            }
            outMsg.Write((byte)0);
        }

        public void Read(PlayerInput input)
        {
            input.State.Vector = inMsg.ReadUInt16();
            input.Cursor = inMsg.ReadFixedVector2();
            input.Pressed.Clear();
            input.Released.Clear();
            input.DoublePressed.Clear();
            byte pack;
            while ((pack = inMsg.ReadByte()) != 0)
            {
                InputEvent eventType;
                SynchKey skey;
                Unpack(pack, out eventType, out skey);
                if (eventType == InputEvent.Pressed)
                    input.Pressed[skey] = true;
                else if (eventType == InputEvent.Released)
                    input.Released[skey] = true;
                else if (eventType == InputEvent.DoublePressed)
                    input.DoublePressed[skey] = true;
                else
                    Debug.Assert(false, "처리되지 않은 InputEvent");
            }
        }

        byte Pack(InputEvent eventType, SynchKey key)
        {
            int e = (int)eventType;
            int k = (int)key;
            Debug.Assert(0 <= e && e < 16);
            Debug.Assert(0 <= k && k < 16);
            return (byte)(((e & 0xF) << 4) | (k & 0xF));
        }

        void Unpack(byte data, out InputEvent eventType, out SynchKey key)
        {
            int e, k;
            e = (data >> 4) & 0xF;
            k = (data & 0xF);
            eventType = (InputEvent)e;
            key = (SynchKey)k;
        }

        public void End()
        {
            Debug.Assert(outMsg != null || inMsg != null);
            outMsg = null;
            inMsg = null;
        }

        NetOutgoingMessage outMsg;
        NetIncomingMessage inMsg;
    }
}
