﻿using Lidgren.Network;

namespace HAJE.Cosmos.Network.HCPacket
{
    public class SessionInfo
    {
        public void Write(NetOutgoingMessage msg, Session session, int playerIndex)
        {
            msg.Write(PacketHC.SessionInfo);
            msg.Write((byte)playerIndex);
        }

        public void Read(NetIncomingMessage msg, Session session)
        {
            msg.ReadPackHC();
            session.PlayerIndex = msg.ReadByte();
        }
    }
}
