﻿using HAJE.Cosmos.UI;
using OpenTK;
using OpenTK.Graphics;
using System.Drawing;

namespace HAJE.Cosmos.Scene
{
    public class MyButtonListener : ButtonListener
    {
        public override void OnPressed(Second deltaTime)
        {
            // button pressed 
            GameSystem.Log.Write("button pressed");
        }

        public override void OnReleased(Second deltaTime)
        {
            // button released
            GameSystem.Log.Write("button released");
        }
    }

    public class UIDevTestScene : UI.Scene
    {
        public UIDevTestScene()
        {
            Vector2 posScene = new Vector2(0.0f, 0.0f);
            Vector2 posLabel = new Vector2(50.0f, 50.0f);
            Vector2 posLabelChild = new Vector2(50.0f, 50.0f);
            Vector2 posSprite = new Vector2(50.0f, 50.0f);
            Vector2 posNew = new Vector2(400.0f, 0.0f);
            Label label = new Label("This is Scene Test", posLabel, 10);
            Label labelChild = new Label("This is child node", posLabelChild, 80);
            Sprite sprite = new Sprite(posSprite, "Resource/Background/Mars.png");
            label.SetText("한글이 더해지면 또 더해지면  aaabbbcccdddeeefffggghhhiiijjjkkklllmmmnnnooopppqqqrrrssstttuuuvvvwwwxxxyyy 마지막에 더해지면 이것도 되는지 확인중...");            
            //label.SetBgColor(System.Drawing.Color.Brown);
            labelChild.AddChild(sprite);
            label.AddChild(labelChild);
            AddChild(label);
            //SetPosition(posNew);
            Color4 color = Color4.White;

            // button test
            Rectangle rect = new Rectangle(0, 0, 100, 50);
            MyButtonListener btnListener = new MyButtonListener();
            Button button = new Button(rect, "Resource/UI/btnReleased.png", "Resource/UI/btnPressed.png", btnListener);
            button.Dock = new Vector2(1.0f, 1.0f);
            button.Anchor = new Vector2(1.0f, 1.0f);
            AddChild(button);
        }
    }
}
