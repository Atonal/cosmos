﻿using HAJE.Cosmos.SystemComponent;
using System.Diagnostics;

namespace HAJE.Cosmos
{
    static class Program
    {
        static void Main(string[] args)
        {
            GameSystem.Initialize();
            UnitTest.Run();
            if (args.Length == 1 && Debugger.IsAttached)
            {
                string option = args[0];
                if (option == "uiDev")
                    GameSystem.RunGameLoop(new GameLoop.UISceneLoop(new Scene.UIDevTestScene()));
                else if (option == "bgPreview")
                    GameSystem.RunGameLoop(new GameLoop.BackgroundPreviewLoop());
            }
            else
            {
                var sessionCreationService = new Network.SessionCreationService();
                sessionCreationService.Start(new Network.SessionCreationInfo());
                sessionCreationService.OnFinished += (session) =>
                    GameSystem.RunGameLoop(new Combat.CombatGameLoop(session));
            }
            GameSystem.Run();
            GameSystem.FinalizeSystem();
        }

    }
}
