﻿using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System;

namespace HAJE.Cosmos.GameLoop
{
    public class UISceneLoop : SystemComponent.GameLoopBase
    {
        public UISceneLoop(UI.Scene scene)
            : base("UISceneLoop")
        {
            this.scene = scene;
        }

        public override void Update(Second deltaTime)
        {
            GameLoopCommon.TryExitGame();
            scene.Update(deltaTime);
        }

        public override void RenderFrame(Second deltaTime)
        {
            scene.Render(deltaTime);
        }

        public override void Dispose(bool manual)
        {
            scene.Dispose();
        }

        UI.Scene scene;
    }
}
