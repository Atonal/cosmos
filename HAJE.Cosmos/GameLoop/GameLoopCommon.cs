﻿using OpenTK.Input;

namespace HAJE.Cosmos.GameLoop
{
    /// <summary>
    /// GameLoop에서 공통적으로 자주 쓰이는 구문을 묶어놓는 곳
    /// </summary>
    public static class GameLoopCommon
    {
        public static void TryExitGame()
        {
            if (!GameSystem.MainForm.Focused) return;

            var keyboard = OpenTK.Input.Keyboard.GetState();
            if ((keyboard[Key.F4] && keyboard[Key.AltLeft]))
            {
                GameSystem.MainForm.Exit();
            }
        }
    }
}
