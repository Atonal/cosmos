﻿using HAJE.Cosmos.Combat.Background;
using HAJE.Cosmos.Combat.Background.Factory;
using HAJE.Cosmos.Combat.GameArea;
using HAJE.Cosmos.Combat.GameArea.Factory;
using HAJE.Cosmos.Rendering;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System.Collections.Generic;

namespace HAJE.Cosmos.GameLoop
{
    public class BackgroundPreviewLoop : SystemComponent.GameLoopBase
    {
        public BackgroundPreviewLoop()
            : base("BackgroundPreviewLoop")
        {
            backgroundList = new Dictionary<Key, IBackgroundFactory>();
            backgroundList.Add(Key.Number1, new SampleBackground());
            backgroundList.Add(Key.Number2, new UrsaMajorBackground());
            backgroundList.Add(Key.Number3, new ShipBackground());
        }

        protected override void OnInitialize()
        {
            camera = new Camera(GameSystem.Viewport);
            backgroundRenderer = new BackgroundRenderer(new SampleBackground().Create());
            deferredRenderer = new DeferredRenderScratch();
            gameAreaRenderer = new GameAreaRenderer(new TestGameArea().Create());
            base.OnInitialize();
        }

        public override void Update(Second deltaTime)
        {
            GameLoopCommon.TryExitGame();
            var keyboard = OpenTK.Input.Keyboard.GetState();
            var dPos = 500 * deltaTime;
            if (keyboard[Key.W] || keyboard[Key.Up])
                camera.MoveTo(new Vector2(camera.Position.X, camera.Position.Y + dPos));
            
            if (keyboard[Key.A] || keyboard[Key.Left])
                camera.MoveTo(new Vector2(camera.Position.X - dPos, camera.Position.Y));
            if (keyboard[Key.S] || keyboard[Key.Down])
                camera.MoveTo(new Vector2(camera.Position.X, camera.Position.Y - dPos));
            if (keyboard[Key.D] || keyboard[Key.Right])
                camera.MoveTo(new Vector2(camera.Position.X + dPos, camera.Position.Y));

            var prevKeyPressing = keyPressing;
            keyPressing = false;
            foreach (var k in backgroundList.Keys)
            {
                if (keyboard[k])
                {
                    if (!prevKeyPressing)
                    {
                        backgroundRenderer.Dispose();
                        backgroundRenderer = new BackgroundRenderer(backgroundList[k].Create());
                    }
                    keyPressing = true;
                }
            }

            GameSystem.DebugOutput.SetWatch("Camera Position", string.Format("({0}, {1})",
                   (int)camera.Position.X, (int)camera.Position.Y));

            Vector2 mousePos = GameSystem.MainForm.GetMousePosition();

            var mouse = Mouse.GetState();

            Vector3 mouse3DPos = camera.ScreenToWorld(mousePos, mouse.Wheel * 100 + 500);
            
            GameSystem.DebugOutput.SetWatch("Mouse Position", string.Format("({0}, {1}, {2})",
                (int)mouse3DPos.X, (int)mouse3DPos.Y, (int)mouse3DPos.Z));
                
        }

        public override void RenderFrame(Second deltaTime)
        {
            backgroundRenderer.Draw(camera, deltaTime);
            deferredRenderer.Draw(camera, deltaTime);
            gameAreaRenderer.Draw(camera);
        }

        public override void Dispose(bool manual)
        {
            base.Dispose(manual);

            if (backgroundRenderer != null) backgroundRenderer.Dispose(); backgroundRenderer = null;
            if (deferredRenderer != null) deferredRenderer.Dispose(); deferredRenderer = null;
            if (gameAreaRenderer != null) gameAreaRenderer.Dispose(); gameAreaRenderer = null;
        }

        GameAreaRenderer gameAreaRenderer;
        DeferredRenderScratch deferredRenderer;
        BackgroundRenderer backgroundRenderer;
        Dictionary<Key, IBackgroundFactory> backgroundList;
        Camera camera;
        bool keyPressing = false;
    }
}
