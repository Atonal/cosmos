﻿using OpenTK;
using System;
using System.Drawing;
using HAJE.Cosmos.Rendering;
using System.Windows.Forms;


namespace HAJE.Cosmos.UI
{
    public abstract class ButtonListener
    {
        public abstract void OnPressed(Second deltaTime);
        public abstract void OnReleased(Second deltaTime);
    }    
}