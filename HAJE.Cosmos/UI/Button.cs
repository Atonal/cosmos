﻿using OpenTK;
using OpenTK.Graphics;
using System;
using System.Drawing;
using HAJE.Cosmos.Rendering;
using System.Windows.Forms;
using OpenTK.Graphics.OpenGL;


namespace HAJE.Cosmos.UI
{
    public class Button : Node, IDisposable
    {
        public override void Dispose()
        {
            textureAtlas.Dispose();
            base.Dispose();
        }

        public Button(Rectangle rect, String pathReleased, String pathPressed, ButtonListener listener)
            : base(new Vector2(rect.X, rect.Y))
        {
            // get ready for texture atlas
            textureAtlas = new TextureAtlas();
            Bitmap bitmap;
            bitmap = new Bitmap(pathReleased);
            texUVReleased = textureAtlas.Register(bitmap);
            bitmap = new Bitmap(pathPressed);
            texUVPressed = textureAtlas.Register(bitmap);
            textureAtlas.BakeTexture();

            // set
            base.BaseSize = new Vector2(rect.Width, rect.Height);
            width = rect.Width;
            height = rect.Height;
            pressed = false;
            btnListener = listener;
        }

        public override void Update(Second deltaTime)
        {
            // test mouse input
            OpenTK.Input.MouseState mouseState = OpenTK.Input.Mouse.GetState();
            Vector2 posWorld = GetWorldPosition();
            if (OpenTK.Input.ButtonState.Pressed == mouseState.LeftButton)
            {
                Vector2 pos = GameSystem.MainForm.GetMousePosition();
                if (pos.X >= posWorld.X && pos.X < posWorld.X + width &&
                    pos.Y >= posWorld.Y && pos.Y < posWorld.Y + height)
                {
                    if (!pressed)
                    {
                        btnListener.OnPressed(deltaTime);
                    }
                    pressed = true;
                }
                else
                {
                    if (pressed)
                    {
                        btnListener.OnReleased(deltaTime);
                    }
                    pressed = false;
                }
            }            
            else // button released
            {
                if (pressed)
                {
                    btnListener.OnReleased(deltaTime);
                }
                pressed = false;
            }           

            base.Update(deltaTime);
        }

        public override void Draw(Second deltaTime)
        {
            // getting world position
            // TODO, we need to aplly pos and rotation     
            //Vector2 worldPos = GetWorldPosition();
                
            //Rectangle rect = new Rectangle((int)worldPos.X, (int)worldPos.Y, width, height);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            if (pressed)
            {
                //GLHelper.DrawTexture(textureAtlas.Texture, rect, texUVPressed);
                Color4 color = new Color4(Color.X, Color.Y, Color.Z, Opacity);
                GLHelper.DrawTexture(textureAtlas.Texture, WorldTransform, width, height, color, texUVPressed);
            }
            else
            {
                Color4 color = new Color4(Color.X, Color.Y, Color.Z, Opacity);
                GLHelper.DrawTexture(textureAtlas.Texture, WorldTransform, width, height, color, texUVReleased);
            }
        }

        private TextureAtlas textureAtlas;
        private RectangleF texUVReleased;
        private RectangleF texUVPressed;
        private bool pressed;
              
        private int width;
        private int height;

        private ButtonListener btnListener;
    }    
}