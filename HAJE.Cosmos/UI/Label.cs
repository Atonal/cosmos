﻿using OpenTK;
using OpenTK.Graphics;
using System;
using System.Drawing;
using HAJE.Cosmos.Rendering;
using System.Windows.Forms;
using OpenTK.Graphics.OpenGL;


namespace HAJE.Cosmos.UI
{
    public class Label : Node, IDisposable
    {
        public override void Dispose()
        {
            texture.Dispose();
            base.Dispose();            
        }

        public Label(string str, Vector2 pos, int ftSize)
            : base(pos)
        {            
            Vector2 texSize = _ReplaceTexture(str, ftSize, Opacity, Color);
                       
            // set
            base.BaseSize = texSize;
            Name = str;
            fontSize = ftSize;
        }

        public override void Draw(Second deltaTime)
        {
            // getting world position
            //Vector2 worldPos = GetWorldPosition();

            //GL.PushAttrib();
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            //GLHelper.DrawTexture(texture, worldPos, FgColor);
            //GL.PopAttrib();

            //Matrix3 mat = WorldTransform;
            //mat.Transpose();            
            GLHelper.DrawTexture(texture, WorldTransform, Opacity, Color);
        }

        public override void SetColor(Color color)
        {            
            // TODO, this should be delayed operation
            Color = new Vector3(color.R >> 7, color.G >> 7, color.B >> 7);
            Opacity = color.A >> 7;
            _ReplaceTexture(base.Name, fontSize, Opacity, Color);
        }

        public override void SetColor(float opacity, Vector3 color)
        {
            // TODO, this should be delayed operation
            _ReplaceTexture(base.Name, fontSize, opacity, color);

            // set
            Opacity = opacity;
            Color = color;
        }      

        public void SetText(String str)
        {
            if (Name == str)
            {
                return;
            }

            // otherwise, recrate texture
            // TODO, this should be delayed operation
            Vector2 texSize = _ReplaceTexture(str, fontSize, Opacity, Color);

            // set
            base.BaseSize = texSize;
            Name = str;
        }

        public void SetSize(int size)
        {
            if (fontSize == size)
            {
                return;
            }

            // otherwise, recrate texture
            // TODO, this should be delayed operation
            Vector2 texSize = _ReplaceTexture(Name, size, Opacity, Color);

            // size
            base.BaseSize = texSize;
            fontSize = size;
        }

        public void DrawTextTest()
        {      
            // drawing Texture
            // TODO, donot use GLHelper.DrawTexture
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);            
            GLHelper.DrawTexture(texture, Position, Opacity, Color);
        }

        private Vector2 _ReplaceTexture(String str, int size, float opacity, Vector3 color)
        {
            // need to calc. Bitmap size from string length and height           
            font = new Font(FontFamily.GenericSansSerif, size);

            //TextFormatFlags flags
            //= TextFormatFlags.Left
            //| TextFormatFlags.Top
            //| TextFormatFlags.NoPadding
            //| TextFormatFlags.NoPrefix;
            //Size szProposed = new Size(3024, 3024);
            //Size stringSize = TextRenderer.MeasureText(str, font, szProposed, flags);
           
            //Size stringSize = TextRenderer.MeasureText(str, font);
            //// TODO, temp solution
            //stringSize.Width += stringSize.Width/20;

            Graphics g = Graphics.FromHwnd(GameSystem.MainForm.WindowInfo.Handle);    
            SizeF stringSize = g.MeasureString(str, font);
            
            using (Bitmap bitmap = new Bitmap((int)stringSize.Width, (int)stringSize.Height))
            {
                using (Graphics gfx = Graphics.FromImage(bitmap))
                {                    
                    PointF pt = new PointF(0.0f, 0.0f);
                    gfx.Clear(System.Drawing.Color.Transparent);                                       
                    gfx.DrawString(str, font, Brushes.White, pt);
                }

                if (null != texture)
                {
                    texture.Dispose();
                }
                texture = Texture.CreateFromBitmap(bitmap);
            }

            return (new Vector2(stringSize.Width, stringSize.Height));            
        }       

        private int fontSize;
        private Font font;
        private Texture texture;       
    }    
}