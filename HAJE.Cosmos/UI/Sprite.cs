﻿using OpenTK;
using OpenTK.Graphics;
using System;
using System.Drawing;
using HAJE.Cosmos.Rendering;
using System.Windows.Forms;


namespace HAJE.Cosmos.UI
{
    public class Sprite : Node, IDisposable
    {
        public override void Dispose()
        {
            texture.Dispose();
            base.Dispose();            
        }

        public Sprite(Vector2 pos, string path) : base(pos)
        {
            texture = TextureManager.GetTexture(path);
            base.BaseSize = texture.Size;
        }

        public override void Draw(Second deltaTime)
        {
            // getting world position
            //Vector2 worldPos = GetWorldPosition();

            // TODO, we need to aplly pos and rotation            
            GLHelper.DrawTexture(texture, WorldTransform, Opacity, Color);
        }
        
        private Texture texture;
    }    
}