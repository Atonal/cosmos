﻿using OpenTK;
using System;
using System.Collections.Generic;

namespace HAJE.Cosmos.UI
{    
    public class Scene : Node, IDisposable
    {
        public Scene()
        {
            BaseSize = GameSystem.Viewport;
        }

        public Scene(Vector2 pos) : base(pos)
        {            
            BaseSize = GameSystem.Viewport;
        }

        public override void Dispose()
        {
            // destroying all
            // then call base's dispose
            base.Dispose();
        }

        public override void Draw(Second deltaTime)
        {
            // no nothting yet
        }

        public void Render(Second deltaTime)
        {
            // TODO, if camera status is turned-on, then draw children
            //

            Visit(deltaTime);
        }

        // TODO, Scene should have a camera as property
    }    
}
