﻿using OpenTK;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace HAJE.Cosmos.UI
{
    public abstract class Node : IDisposable
    {
        public Node()
            : this(Vector2.Zero)
        {
        }

        public Node(Vector2 pos)
        {
            Position = pos;
            Anchor = new Vector2(0.0f, 0.0f);
            Dock = new Vector2(0.0f, 0.0f);
            Rotation = new Radian(0.0f);
            Children = new List<Node>();
            Visible = true;

            LocalTransform = Matrix3.Identity;
            WorldTransform = Matrix3.Identity;
            _RebuildLocalTransform();
            _RebuildWorldTransform();

            dirtyTransform = false;

            //FgColor = Color4.White;
            //BgColor = Color4.Transparent;

            Opacity = 1.0f;
            Color = new Vector3(1.0f, 1.0f, 1.0f);
        }

        public virtual void Dispose()
        {
        }

        //
        // ScreenSpace에서 최종 위치에 변화 없이 anchor 값만 변경.
        // 위치 변동 없이 anchor만 바꾸는 과정에서 position도 함께 바뀐다.
        public void MoveAnchor(Vector2 anchor) 
        { }
        public void SetAnchorInPixels(Vector2 anchorInPixel)
        { }
        public void MoveAnchorInPixels(Vector2 anchorInPixel)
        { }

        //
        // 부모 좌표계 기준 실제 크기. Scale x BaseSize 
        public Vector2 GetSize() 
        {
            return Scale * BaseSize;
        }

        public void SetZOrder(float zOrder)
        {
            ZOrder = zOrder;
        }

        //
        // 주어진 크기가 부모 좌표계 기준 실제 크기가 되도록 scale 조절 
        public void SetSize(Vector2 size)
        {
            //Vector2.Divide(size, BaseSize, Scale);
        }

        //
        // 모든 조상 Node들이 다 Visible인가. SceneGraphBase에 연결 되어있는가.
        public bool IsShowing()
        {
            return Visible;
        }

        public void SetPosition(Vector2 pos)
        {
            // TODO, do not recalc. world matrix in here, use dirty flag for removing reduant calc.
            // rebuild local matrix
            Position = pos;           

            // rebuild world matrix
            _SetDirtyTransform(true);
        }

        public void SetRotation(Radian angle)
        {
            //
            Rotation = angle;

            // rebuild matrix
            _SetDirtyTransform(true);
        }

        public void SetParent(Node parent)
        {
            Parent = parent;

            // rebuild world matrix
            _SetDirtyTransform(true);
        }

        public void AddChild(Node child, float zOrder = 0.0f)
        {
            Children.Add(child);
            child.Parent = this;

            child.SetZOrder(zOrder);

            // need to rebuild order
            _SetDirtyTransform(true);
        }

        public void RemoveChild(Node child)
        {
            Children.Remove(child);
        }

        public void RemoveFromParent()
        {
            if (null == Parent)
            {
                return;
            }

            Parent.RemoveChild(this);
        }

        public Vector2 NodeToScreen(Vector2 vectorFromNodeSpace)
        {
            Vector2 vec2 = new Vector2();
            return vec2;
        }
        public Vector2 ScreenToNode(Vector2 vectorFromScreenSpace)
        {
            Vector2 vec2 = new Vector2();
            return vec2;
        }
        public Vector2 NodeToNode(Node otherNode, Vector2 vectorFromOtherNodeSpace)
        {
            Vector2 vec2 = new Vector2();
            return vec2;
        }
        
        //
        // Update는 화면 앞쪽에 그려지는 노드를 먼저 방문 
        // 화면 앞쪽에 있는 노드가 먼저 입력을 처리 할 수 있도록 
        public virtual void Update(Second deltaTime)
        {  
            foreach (Node node in Children)
            {
                node.Update(deltaTime);
            }
        }       

        //
        // Visits this node's children and draw them recursively.
        public virtual void Visit(Second deltaTime)
        {
            // at first, check if we need to upate transform
            _UpdateTransform();
            if (Visible)
            {
                // actuall drawing
                Draw(deltaTime);
            }

            if (0 < Children.Count)
            {
                // sort children to ascending order regarding z value
                // TODO
                //Children.Sort();

                // visit children for drawing
                foreach (Node node in Children)
                {
                    node.Visit(deltaTime);
                }

                //if (Visible)
                //{
                //    // actuall drawing
                //    Draw(deltaTime);
                //}
            }
            //else if (Visible)
            //{
            //    // actuall drawing
            //    Draw(deltaTime);
            //}
        }

        //
        // should be overided in drived node class (e.g., Label, Sprite etc.)
        public abstract void Draw(Second deltaTime);

        public virtual void SetColor(Color color)
        {
            // set
            Color = new Vector3(color.R >> 7, color.G >> 7, color.B >> 7);
            Opacity = color.A >> 7;
        }

        public virtual void SetColor(float opacity, Vector3 color)
        {         
            // set
            Color = color;
            Opacity = opacity;
        } 

        public Vector2 GetWorldPosition()
        {
            //Vector2 worldPos = new Vector2(WorldTransform.M31, WorldTransform.M32);
            //Vector2 screenSize = GameSystem.Viewport;
            //worldPos = worldPos + (Dock * screenSize - Anchor * BaseSize);
            //return worldPos;

            Vector2 worldPos = new Vector2(WorldTransform.M31, WorldTransform.M32);
            return worldPos;
        }
      
        private void _RebuildLocalTransform()
        {
            // rotate frist, then move.
            Vector3 axis = new Vector3(0.0f, 0.0f, -1.0f);
            Matrix3 matAnchor = Matrix3.Identity;
            Matrix3 matRotate = Matrix3.CreateFromAxisAngle(axis, Rotation);
            matAnchor.M31 -= Anchor.X * BaseSize.X;
            matAnchor.M32 -= Anchor.Y * BaseSize.Y;
            Matrix3 matMove = Matrix3.Identity;
            matMove.M31 = Position.X;
            matMove.M32 = Position.Y;

            LocalTransform = matMove * matRotate * matAnchor;          
        }

        private void _RebuildWorldTransform()
        {
            // rebuild world matrix
            if (null != Parent)
            {
                WorldTransform = Matrix3.Identity;
                WorldTransform = Parent.WorldTransform * LocalTransform;
            }
            else
            {
                WorldTransform = LocalTransform;
            }

            // TODO
            Vector2 parentBaseSize;
            if (null != Parent)
            {
                parentBaseSize = Parent.BaseSize;
            }
            else
            {
                parentBaseSize = GameSystem.Viewport;
            }
            WorldTransform.M31 += Dock.X * parentBaseSize.X;
            WorldTransform.M32 += Dock.Y * parentBaseSize.Y;
        }

        //
        // set dirty transform recursively
        private void _SetDirtyTransform(bool dirty)
        {
            dirtyTransform = dirty;
  
            foreach (Node node in Children)
            {
                node._SetDirtyTransform(dirty);
            }
        }

        //
        // update transfrom
        private void _UpdateTransform()
        {
            if (!dirtyTransform)
            {
                return;
            }

            // at first, calc. local transform
            _RebuildLocalTransform();
            _RebuildWorldTransform();

            // and then calc. world transform of children recursively 
            foreach (Node node in Children)
            {
                node._UpdateTransform();
            }

            dirtyTransform = false;
        }
        
        //
        //=================================================================
        // Properties
        //=================================================================
        public Vector2 Position;
        public Vector2 Anchor;
        public Vector2 Dock;

        public Vector2 Scale;
        public Vector2 BaseSize;

        public Vector3 Color;
        public bool InheritColor;   // true인 경우 부모의 색을 사용 
        public float Opacity;
        public bool InheritOpacity; // true인 경우 부모의 불투명도를 사용

        //private Color4 DefaultFgColor = Color4.White;
        //private Color4 DefaultBgColor = Color4.Transparent;       
        //public Color4 FgColor;
        //public Color4 BgColor;

        public Radian Rotation;

        public bool Visible;

        public float ZOrder { private set; get; }
       
        public Node Parent { private set; get; }
        public List<Node> Children; // ZOrder 오름차순으로 정렬된 리스트 

        public Matrix3 LocalTransform;
        public Matrix3 WorldTransform;

        public String Name;

        private bool dirtyTransform;
    }
}
