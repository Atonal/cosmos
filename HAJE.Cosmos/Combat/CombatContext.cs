﻿using HAJE.Cosmos.Combat.GameArea;
using HAJE.Cosmos.Combat.Physics;
using HAJE.Cosmos.Combat.Projectile;
using HAJE.Cosmos.Combat.Synchronization;
using HAJE.Cosmos.Network;
using HAJE.Cosmos.Rendering;
using HAJE.Cosmos.Rendering.ParticleSystem;
using HAJE.Cosmos.SystemComponent;
using System;

namespace HAJE.Cosmos.Combat
{
    /// <summary>
    /// 전투와 관련된 온갖 객체들을 담고 공유하는 객체.
    /// </summary>
    public class CombatContext
    {
        public CombatGameLoop Loop;
        public Session Session;
        public Scheduler Schdeulder;
        public CombatRenderer CombatRenderer;
        public CombatSimulation CombatSimulation;
        public CombatSync CombatSync;
        public Camera Camera;
        public Random Random;
        public GameAreaData GameAreaData;

        public Fighter.Fighter[] PlayerFighters;
        public Fighter.Fighter[] AIFighters;

        public ProjectileManager ProjectileManager;
        public ParticleManager ParticleManager;
        public ColliderManager ColliderManager;
    }
}
