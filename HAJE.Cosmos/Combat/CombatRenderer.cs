﻿
namespace HAJE.Cosmos.Combat
{
    /// <summary>
    /// 렌더링과 관련된 객체들을 관리하고
    /// 렌더링 관련 객체들을 순회하면서 렌더링 작업을 수행한다.
    /// 
    /// 시뮬레이션 업데이트는 불연속적이고 그 주기도 렌더링보다 느리기 때문에,
    /// 시뮬레이션의 최근 두 프레임 결과를 적절히 보간해서 보여준다.
    /// </summary>
    public class CombatRenderer
    {
        public CombatRenderer(CombatContext context)
        {
            this.context = context;
            backgroundRenderer = new Background.BackgroundRenderer(
                new Background.Factory.UrsaMajorBackground().Create()
            );
            gameAreaRenderer = new GameArea.GameAreaRenderer(
                new GameArea.Factory.TestGameArea().Create()
            );
        }

        /// <summary>
        /// 최근에 보여진 상태를 이전 상태로 저장한다.
        /// 이전 상태는 새 시뮬레이션 결과와 보간되어 렌더링하는데 사용 된다.
        /// </summary>
        public void NextFrame()
        {
            foreach (var f in context.PlayerFighters)
                f.NextFrame();
            foreach (var f in context.AIFighters)
                f.NextFrame();

            context.ProjectileManager.NextFrame();
        }

        /// <summary>
        /// 시뮬레이션에 진행에 따른 고정 프레임 업데이트
        /// 시뮬레이션 진행 속도와 완전히 일치하게 업데이트 되어야 하는 변경을 처리
        /// </summary>
        public void Update(FixedPoint deltaTimeInSec)
        {
        }

        /// <summary>
        /// 렌더링 작업을 수행.
        /// 렌더링 값 = 이이전프레임 * (1-frameAlpha) + 이전프레임 * frameAlpha
        /// 네트워크 지연 발생시 frameAlpha가 1보다 클 수 있다. 
        /// </summary>
        public void Render(Second deltaTime, float frameAlpha)
        {
            foreach (var f in context.PlayerFighters)
                f.UpdateRender(frameAlpha);
            foreach (var f in context.AIFighters)
                f.UpdateRender(frameAlpha);
            context.ProjectileManager.UpdateRender(frameAlpha);
            
            //XXX: 임시 코드. 나중에 전투 카메라가 담당해야 할 부분
            //     전투 카메라의 조작은 전투 스크립트가 담당
            context.Camera.MoveTo(context.PlayerFighters[0].RenderPosition);
            
            backgroundRenderer.Draw(context.Camera, deltaTime);
            gameAreaRenderer.Draw(context.Camera);
            context.ProjectileManager.Render();
            context.ParticleManager.Render();
            context.ColliderManager.Render();
            foreach (var f in context.PlayerFighters)
                f.Render();
            foreach (var f in context.AIFighters)
                f.Render();
        }

        CombatContext context;
        Background.BackgroundRenderer backgroundRenderer;
        GameArea.GameAreaRenderer gameAreaRenderer;
    }
}
