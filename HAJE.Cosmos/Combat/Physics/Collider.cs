﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.Cosmos.Combat.Physics
{
    public class Collider
    {
        public Collider(object owner)
        {
            this.owner = owner;
            this.Enabled = true;
        }

        public Boolean Enabled;
        public readonly object owner;
        public Position Position = new Position();
        public FixedVector2 Velocity = FixedVector2.Zero;
        public FixedPoint Radius = 8;
        public FixedPoint Mass = 100;
    }
}
