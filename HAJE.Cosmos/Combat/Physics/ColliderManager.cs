﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.Cosmos.Combat.Physics
{
    public class ColliderManager
    {
        private List<Collider> ColliderList;
        private Grid ColliderGrid;

        public ColliderManager(CombatContext context)
        {
            ColliderList = new List<Collider>();
            ColliderGrid = new Grid();
        }

        public Collider GetCollider(object owner)
        {
            Collider col = new Collider(owner);
            ColliderList.Add(col);
            return col;
        }
        public void Update()
        {
            ColliderGrid.Update(ColliderList);
        }
        public void Render()
        {
        }

        public List<Collider> CheckCollision(Collider col)
        {
            return ColliderGrid.ParseCollider(col, col.Radius);
        }
        public List<Collider> CheckCollision(Collider col, Type type)
        {
            return ColliderGrid.ParseCollider(col, col.Radius, type);
        }
        public List<Collider> CheckCollision(Collider col, Type[] types)
        {
            List<Collider> res = new List<Collider>();
            foreach (Type t in types)
                foreach (Collider c in ColliderGrid.ParseCollider(col, col.Radius, t))
                    res.Add(c);

            return res;
        }
        public List<Collider> ParseCollider(Collider col, FixedPoint radius)
        {
            return ColliderGrid.ParseCollider(col, radius);
        }
        public List<Collider> ParseCollider(Collider col, FixedPoint radius, Type type)
        {
            return ColliderGrid.ParseCollider(col, radius, type);
        }
        public List<Collider> ParseCollider(Collider col, FixedPoint radius, Type[] types)
        {
            List<Collider> res = new List<Collider>();
            foreach (Type t in types)
                foreach (Collider c in ColliderGrid.ParseCollider(col, radius, t))
                    res.Add(c);

            return res;
        }
    }
}
