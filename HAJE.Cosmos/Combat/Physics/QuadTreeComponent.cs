﻿using HAJE.Cosmos.Rendering;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.Cosmos.Combat.Physics
{
    class QuadTreeComponent
    {
        private FixedPoint minx, maxx, miny, maxy;
        private FixedPoint xdist, ydist;
        private FixedPoint mindist;
        private CombatContext context;

        /// <summary>
        /// 1    |    0
        ///      |
        /// -----------
        ///      |
        /// 2    |    3
        /// </summary>
        private QuadTreeComponent Parent;
        private QuadTreeComponent[] Child;
        public List<Collider> ColliderList;

        private bool haveChild
        {
            get
            {
                return this.Child != null;
            }
        }

        public QuadTreeComponent(FixedPoint minx, FixedPoint maxx, FixedPoint miny, FixedPoint maxy, CombatContext context)
        {
            this.context = context;
            
            this.mindist = 30;
            this.minx = minx;
            this.maxx = maxx;
            this.miny = miny;
            this.maxy = maxy;

            this.xdist = maxx - minx;
            this.ydist = maxy - miny;

            ColliderList = new List<Collider>();

            Debug.Assert(xdist > mindist);
            Debug.Assert(ydist > mindist);
        }

        private Boolean CheckRange(Collider collider)
        {
            FixedPoint x = collider.Position.Simulation.X;
            FixedPoint y = collider.Position.Simulation.Y;

            if (minx <= x && x <= maxx && miny <= y && y <= maxy)
                return true;
            return false;
        }
        private Int32 GetChildIndex(FixedPoint x, FixedPoint y)
        {
            FixedPoint midx = (minx + maxx) / 2;
            FixedPoint midy = (miny + maxy) / 2;

            if (midx <= x && midy <= y)
                return 0;
            else if (midx >= x && midy <= y)
                return 1;
            else if (midx >= x && midy >= y)
                return 2;
            else if (midx <= x && midy >= y)
                return 3;

            throw new Exception("여기 오면 안됨.");
        }
        private void MakeChild()
        {
            FixedPoint midx = (minx + maxx) / 2;
            FixedPoint midy = (miny + maxy) / 2;
            this.Child = new QuadTreeComponent[]
            {
                new QuadTreeComponent(midx, maxx, midy, maxy, context),
                new QuadTreeComponent(minx, midx, midy, maxy, context),
                new QuadTreeComponent(minx, midx, miny, midy, context),
                new QuadTreeComponent(midx, maxx, miny, midy, context)
            };

            foreach (var child in this.Child)
                child.Parent = this;
        }

        public void AddCollider(Collider collider)
        {
            if (collider.Enabled == false)
                return;

            this.ColliderList.Add(collider);

            // Create Child
            if (this.haveChild == false && 
                this.ColliderList.Count >= 2 && 
                this.xdist > mindist * 2 && 
                this.ydist > mindist * 2)
            {
                MakeChild();
                foreach (var col in this.ColliderList)
                {
                    FixedPoint x = col.Position.Simulation.X;
                    FixedPoint y = col.Position.Simulation.Y;

                    this.Child[GetChildIndex(x, y)].AddCollider(col);
                }
            }
            else if (this.haveChild == true)
            {
                foreach (var child in Child)
                    if (child.CheckRange(collider) == true)
                        child.AddCollider(collider);
            }
        }
        public void RemoveCollider(Collider collider)
        {
            this.ColliderList.Remove(collider);
            if (ColliderList.Count < 2)
                this.Child = null;

            if (haveChild == true)
                foreach (var child in Child)
                    if (child.ColliderList.Contains(collider))
                        child.RemoveCollider(collider);
        }
        public void UpdateCollider(List<Collider> list)
        {
            foreach (var col in list)
            {
                if (this.ColliderList.Contains(col))
                {
                    if (col.Enabled == false || CheckRange(col) == false)
                        RemoveCollider(col);
                }
                else if (col.Enabled == true && CheckRange(col) == true)
                    AddCollider(col);
            }

            if (this.haveChild == true)
                foreach (var child in this.Child)
                    child.UpdateCollider(list);
        }
        public void Render()
        {
            if (haveChild == true)
                foreach (var child in Child)
                    child.Render();
            else
            {
                List<Vector3> posList = new List<Vector3>();
                for (FixedPoint fp = minx; fp < maxx; fp += 10)
                {
                    posList.Add(new Vector3(fp.FloatingValue, miny.FloatingValue, -500));
                    posList.Add(new Vector3(fp.FloatingValue, maxy.FloatingValue, -500));
                }
                for (FixedPoint fp = miny; fp < maxy; fp += 10)
                {
                    posList.Add(new Vector3(minx.FloatingValue, fp.FloatingValue, -500));
                    posList.Add(new Vector3(maxx.FloatingValue, fp.FloatingValue, -500));
                }

                BillboardRenderer billboardRenderer = new BillboardRenderer(posList.Count);
                billboardRenderer.Begin();
                foreach (Vector3 v3 in posList)
                    billboardRenderer.Draw(v3,
                        null, null,
                        new Vector2(0.5f, 0.5f), new Vector2(10, 10), null);
                billboardRenderer.End(ref context.Camera.Matrix, TextureManager.GetTexture("Resource/ParticleSample.png"));
                billboardRenderer.Dispose();
            }
        }
        public List<Collider> ParseCollision(Collider collider, FixedPoint radius)
        {
            FixedPoint x = collider.Position.Simulation.X;
            FixedPoint y = collider.Position.Simulation.Y;
            if (this.haveChild == false)
            {
                return new List<Collider>();
            }
            else if (maxx - x - radius > xdist  && x - minx - radius > xdist &&
                maxy - y - radius > ydist && y - miny - radius > ydist)
            {

                return this.Child[GetChildIndex(x, y)].ParseCollision(collider, radius);
            }
            else
            {
                List<Collider> res = new List<Collider>();
                foreach (var col in this.ColliderList)
                    if (col != collider)
                    {
                        if ((collider.Position.Simulation - col.Position.Simulation).Length() < col.Radius + radius)
                        {
                            res.Add(col);
                        }
                    }

                return res;
            }
        }
    }
}
