﻿using HAJE.Cosmos.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.Cosmos.Combat.Physics
{
    class QuadTree
    {
        private FixedPoint minx, maxx, miny, maxy;
        private QuadTreeComponent Component;
        private CombatContext context;

        [Obsolete("HAJE.Cosmos.Combat.Physics.Grid를 사용하는것을 권장합니다.")]
        public QuadTree(FixedPoint minx, FixedPoint maxx, FixedPoint miny, FixedPoint maxy, CombatContext context)
        {
            this.minx = minx;
            this.maxx = maxx;
            this.miny = miny;
            this.maxy = maxy;
            this.context = context;

            this.Component = new QuadTreeComponent(minx, maxx, miny, maxy, context);
        }

        public void Clear()
        {
            this.Component = new QuadTreeComponent(minx, maxx, miny, maxy, context);
        }
        public List<Collider> ParseCollision(Collider collider, FixedPoint radius)
        {
            var t0 = DateTime.Now.Ticks;
            List<Collider> res = Component.ParseCollision(collider, radius);
            GameSystem.DebugOutput.SetWatch("Parse Time", DateTime.Now.Ticks - t0);
            return res;
        }
        public void Update(List<Collider> ColliderList)
        {
            var t0 = DateTime.Now.Ticks;

            FixedPoint cminx = minx;
            FixedPoint cmaxx = maxx;
            FixedPoint cminy = miny;
            FixedPoint cmaxy = maxy;
            foreach (var c in ColliderList)
            {
                cminx = cminx < c.Position.Simulation.X ? cminx : c.Position.Simulation.X;
                cmaxx = c.Position.Simulation.X < cmaxx ? cmaxx : c.Position.Simulation.X;
                cminy = cminy < c.Position.Simulation.Y ? cminy : c.Position.Simulation.Y;
                cmaxy = c.Position.Simulation.Y < cmaxy ? cmaxy : c.Position.Simulation.Y;
            }

            if (cminx != minx)
                cminx -= 200;
            if (cminy != miny)
                cminy -= 200;
            if (cmaxx != maxx)
                cmaxx += 200;
            if (cmaxy != maxy)
                cmaxy += 200;

            if (cminx < minx || maxx < cmaxx || cminy < miny || maxy < cmaxy)
            {
                minx = cminx;
                maxx = cmaxx;
                miny = cminy;
                maxy = cmaxy;
                GameSystem.Log.Write("Resized");

                Component = new QuadTreeComponent(minx, maxx, miny, maxy, context);
                foreach (var c in ColliderList)
                    Component.AddCollider(c);
            }
            else
                Component.UpdateCollider(ColliderList);
            GameSystem.DebugOutput.SetWatch("Collider@QuadTree", Component.ColliderList.Count);
            GameSystem.DebugOutput.SetWatch("Update Time", DateTime.Now.Ticks - t0);
        }
        public void Render()
        {
            Component.Render();
        }
    }
}
