﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.Cosmos.Combat.Physics
{
    public class Grid
    {
        public Grid()
        {
            ColliderDictionary = new Dictionary<Type, Dictionary<KeyValuePair<int, int>, List<Collider>>>();
        }

        private int GridSize = 100;

        // {x, y} -> List<Grid>
        private Dictionary<Type, Dictionary<KeyValuePair<int, int>, List<Collider>>> ColliderDictionary;
        private List<Collider> this[Type t, int x, int y]
        {
            get
            {
                var kvp = new KeyValuePair<int, int>(x, y);
                if (ColliderDictionary.ContainsKey(t) == false)
                    ColliderDictionary.Add(t, new Dictionary<KeyValuePair<int, int>, List<Collider>>());

                if (ColliderDictionary[t].ContainsKey(kvp) == false)
                    ColliderDictionary[t].Add(kvp, new List<Collider>());
                    
                return ColliderDictionary[t][kvp];
            }
            set
            {
                var kvp = new KeyValuePair<int, int>(x, y);
                if (ColliderDictionary.ContainsKey(t) == false)
                    ColliderDictionary.Add(t, new Dictionary<KeyValuePair<int, int>, List<Collider>>());

                if (ColliderDictionary[t].ContainsKey(kvp) == false)
                    ColliderDictionary[t].Add(kvp, new List<Collider>());

                ColliderDictionary[t][kvp] = value;
            }
        }

        public void Update(List<Collider> colliderList)
        {
            int colliderCount = 0;

            ColliderDictionary.Clear();
            foreach (Collider c in colliderList)
                if (c.Enabled == true)
                {
                    this[c.owner.GetType(), (int)c.Position.Simulation.X / GridSize, (int)c.Position.Simulation.Y / GridSize].Add(c);
                    colliderCount++;
                }

            GameSystem.DebugOutput.SetWatch("Count of Collider", colliderCount);
        }
        public List<Collider> ParseCollider(Collider col, FixedPoint radius, Type targetType) 
        {
            List<Collider> cl = new List<Collider>();

            for (int xidx = (int)(col.Position.Simulation.X - radius) / GridSize; xidx <= (int)(col.Position.Simulation.X + radius) / GridSize; xidx++)
                for (int yidx = (int)(col.Position.Simulation.Y - radius) / GridSize; yidx <= (int)(col.Position.Simulation.Y + radius) / GridSize; yidx++)
                    foreach (Type t in ColliderDictionary.Keys)
                        if (t.IsSubclassOf(targetType) || t == targetType)
                            foreach (Collider c in this[t, xidx, yidx])
                            {
                                FixedPoint xdist = col.Position.Simulation.X - c.Position.Simulation.X;
                                FixedPoint ydist = col.Position.Simulation.Y - c.Position.Simulation.Y;
                                FixedPoint r = radius + c.Radius;
                                if (xdist * xdist + ydist * ydist < r * r)
                                    cl.Add(c);
                            }

            return cl;
        }
        public List<Collider> ParseCollider(Collider col, FixedPoint radius)
        {
            return ParseCollider(col, radius, typeof(object));
        }
    }
}
