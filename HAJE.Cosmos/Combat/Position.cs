﻿using OpenTK;

namespace HAJE.Cosmos.Combat
{
    /// <summary>
    /// 위치 정보 보간을 돕는 객체
    /// </summary>
    public class Position
    {
        public Position()
        {
            Initialize(FixedVector2.Zero);
        }

        public Position(FixedVector2 pos)
        {
            Initialize(pos);
        }

        /// <summary>
        /// 시뮬레이션 업데이트를 반영, 다음 프레임 준비
        /// </summary>
        public void NextFrame()
        {
            next = (Vector2)Simulation;
            prev = rendering;
        }

        /// <summary>
        /// 위치 초기화.
        /// 점멸이나 순간이동 등 보간 없는 이동에도 사용.
        /// </summary>
        public void Initialize(FixedVector2 pos)
        {
            Simulation = pos;
            prev = next = rendering = (Vector2)pos;
        }

        /// <summary>
        /// 렌더링 위치 업데이트.
        /// </summary>
        public void UpdateRender(float frameAlpha)
        {
            Vector2.Lerp(ref prev, ref next, frameAlpha, out rendering);
        }

        /// <summary>
        /// 시뮬레이션에서 사용할 로직 위치
        /// </summary>
        public FixedVector2 Simulation;

        /// <summary>
        /// 렌더링에서 사용할 보간된 위치
        /// </summary>
        public Vector3 Rendering
        {
            get
            {
                return new Vector3(rendering.X, rendering.Y, ZOrder.GameDepth);
            }
        }

        Vector2 rendering;
        Vector2 prev;
        Vector2 next;

        public FixedVector2 Previous
        {
            get
            {
                return (FixedVector2)prev;
            }
        }
    }
}
