﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.Cosmos.Combat
{
    public static class SimulationTime
    {
        public static readonly int SimulationUpdatePerSecond = 32;
        public static readonly FixedPoint SimulationUpdateInterval = 1 / (FixedPoint)SimulationUpdatePerSecond;
        public static readonly float InputSynchronizationTimeOut = 1.0f / 48.0f;
    }
}
