﻿using OpenTK.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.Cosmos.Combat.Synchronization
{
    public enum SynchKey
    {
        Left = 0,
        Up,
        Down,
        Right,
        Break,
        S1,
        S2,
        S3,
        S4,
        MouseLeft,
        MouseRight
    }

    public static class SynchKeyExtension
    {
        public static Key ToKey(this SynchKey synchKey)
        {
            switch (synchKey)
            {
                case SynchKey.Right:
                    return Key.D;
                case SynchKey.Down:
                    return Key.S;
                case SynchKey.Left:
                    return Key.A;
                case SynchKey.Up:
                    return Key.W;
                case SynchKey.Break:
                    return Key.Space;
                case SynchKey.S1:
                    return Key.Number1;
                case SynchKey.S2:
                    return Key.Number2;
                case SynchKey.S3:
                    return Key.Number3;
                case SynchKey.S4:
                    return Key.Number4;
                case SynchKey.MouseLeft:
                case SynchKey.MouseRight:
                    Debug.Assert(false); // 마우스 버튼에 대해서 호출 금지
                    break;
                default:
                    Debug.Assert(false); // 처리되지 않은 키
                    break;
            }
            return Key.Escape;
        }

        public static readonly SynchKey[] Values;
        
        static SynchKeyExtension()
        {
            var arr = Enum.GetValues(typeof(SynchKey));
            Values = new SynchKey[arr.Length];
            for (int i = 0; i < arr.Length; i++)
            {
                Values[i] = (SynchKey)arr.GetValue(i);
            }
        }
    }
}
