﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.Cosmos.Combat.Synchronization
{
    public enum InputEvent
    {
        State = 0,
        Pressed,
        Released,
        DoublePressed
    }
}
