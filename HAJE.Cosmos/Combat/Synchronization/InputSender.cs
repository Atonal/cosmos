﻿using HAJE.Cosmos.Network;
using HAJE.Cosmos.Rendering;
using HAJE.Cosmos.SystemComponent;
using Lidgren.Network;
using OpenTK;
using OpenTK.Input;
using System;
using System.Diagnostics;
using InputSynch = HAJE.Cosmos.Network.CHPacket.InputSynch;

namespace HAJE.Cosmos.Combat.Synchronization
{
    /// <summary>
    /// 입력 동기화를 위해 주기적으로 자신의 입력 상태를 호스트로 전송하는 클라이언트 객체
    /// </summary>
    public class InputSender : IUpdatable
    {
        public void Start(CombatContext ctx)
        {
            this.ctx = ctx;
            scheduler = new Scheduler("InputSender");
            ctx.Schdeulder.Schedule(scheduler);
            scheduler.Schedule(this);
            
            lastKeyPressTime = new float[(int)Key.LastKey];
            for (int i = 0; i < (int)Key.LastKey; i++)
                lastKeyPressTime[i] = (float)(-1);

            isReleased = new bool[(int)Key.LastKey];
        }

        public void Update(GameTime gameTime)
        {            
            if (GameSystem.MainForm.Focused)
            {
                curTime = curTime + gameTime.DeltaTime;
                // 키보드 입력
                var keyboard = Keyboard.GetState();
                for (int i = 0; i < SynchKeyExtension.Values.Length; i++)
                {
                    SynchKey skey = SynchKeyExtension.Values[i];
                    
                    // 마우스 버튼 예외처리
                    if (skey == SynchKey.MouseLeft || skey == SynchKey.MouseRight)
                        continue;

                    Key rkey = skey.ToKey();

                    bool dirKey = (skey == SynchKey.Left)
                        || (skey == SynchKey.Right)
                        || (skey == SynchKey.Up)
                        || (skey == SynchKey.Down);

                    if (!inputState[skey] && keyboard[rkey])
                    {
                        PressKey(skey);

                        if (isReleased[(int)rkey] && curTime - lastKeyPressTime[(int)rkey] < MAX_DASH_WAIT_TIME && lastKeySet == keyboard)
                        {
                            DoubleKey(skey);
                            lastKeyPressTime[(int)rkey] = (float)(-1);
                        }
                        lastKeyPressTime[(int)rkey] = curTime;
                        lastKeySet = keyboard;
                        isReleased[(int)rkey] = false;
                    }
                    else if (!dirKey && inputState[skey] && !keyboard[rkey])
                        ReleaseKey(skey);
                }

                if (inputState[SynchKey.Up] && !keyboard[Key.W] && !keyboard[Key.Q] && !keyboard[Key.E])
                {                    
                    ReleaseKey(SynchKey.Up);
                    isReleased[(int)Key.W] = true;
                }
                if (inputState[SynchKey.Down] && !keyboard[Key.S] && !keyboard[Key.Z] && !keyboard[Key.C])
                {
                    ReleaseKey(SynchKey.Down);
                    isReleased[(int)Key.S] = true;
                }                    
                if (inputState[SynchKey.Right] && !keyboard[Key.D] && !keyboard[Key.E] && !keyboard[Key.C])
                {
                    ReleaseKey(SynchKey.Right);
                    isReleased[(int)Key.D] = true;
                }                    
                if (inputState[SynchKey.Left] && !keyboard[Key.A] && !keyboard[Key.Q] && !keyboard[Key.Z])
                {
                    ReleaseKey(SynchKey.Left);
                    isReleased[(int)Key.A] = true;
                }

                //대각선 이동/대쉬
                DiagonalMovement(Key.Q);
                DiagonalMovement(Key.E);
                DiagonalMovement(Key.C);
                DiagonalMovement(Key.Z);

                // 마우스 왼쪽 버튼
                var mouse = Mouse.GetState();
                if (!inputState[SynchKey.MouseLeft] && mouse.LeftButton == ButtonState.Pressed)
                    PressKey(SynchKey.MouseLeft);
                else if (inputState[SynchKey.MouseLeft] && mouse.LeftButton == ButtonState.Released)
                    ReleaseKey(SynchKey.MouseLeft);

                // 마우스 오른쪽 버튼
                if (!inputState[SynchKey.MouseRight] && mouse.RightButton == ButtonState.Pressed)
                    PressKey(SynchKey.MouseRight);
                else if (inputState[SynchKey.MouseRight] && mouse.RightButton == ButtonState.Released)
                    ReleaseKey(SynchKey.MouseRight);

                // 마우스 위치
                var newPos = GameSystem.MainForm.GetMousePosition();
                newPos = Vector2.Clamp(newPos, Vector2.Zero, GameSystem.Viewport);
                newPos = ctx.Camera.ScreenToWorld(newPos, ZOrder.GameDepth).Xy;
                mousePosition = (FixedVector2)newPos;
            }
            else
            {
                for (int i = 0; i < SynchKeyExtension.Values.Length; i++)
                {
                    SynchKey skey = SynchKeyExtension.Values[i];
                    if (inputState[skey])
                        ReleaseKey(skey);
                }
            }

            // 메세지 전송 처리
            if (message != null)
            {
                SendMessage(mousePosition, true);
            }
            else
            {
                timeAfterLastPacket += gameTime.DeltaTime;
                if (timeAfterLastPacket > SimulationTime.InputSynchronizationTimeOut)
                    SendMessage(mousePosition, false);
            }
        }
        

        void PressKey(SynchKey key)
        {
            inputState[key] = true;
            EnsureMessageCreated();
            inputSynchMessage.AppendPressed(key);
        }

        void ReleaseKey(SynchKey key)
        {
            inputState[key] = false;
            EnsureMessageCreated();
            inputSynchMessage.AppendReleased(key);
        }

        void DoubleKey(SynchKey key)
        {
            EnsureMessageCreated();
            inputSynchMessage.AppendDouble(key);
        }

        void DiagonalMovement(Key key)
        {
            var keyboard = Keyboard.GetState();
            if (keyboard[key])
            {
                if (isReleased[(int)key] && curTime - lastKeyPressTime[(int)key] < MAX_DASH_WAIT_TIME && lastKeySet == keyboard)
                {
                    if(key==Key.Q || key==Key.E)
                        DoubleKey(SynchKey.Up);
                    if (key == Key.E || key == Key.C)
                        DoubleKey(SynchKey.Right);
                    if (key == Key.C || key == Key.Z)
                        DoubleKey(SynchKey.Down);
                    if (key == Key.Z || key == Key.Q)
                        DoubleKey(SynchKey.Left);
                    lastKeyPressTime[(int)key] = (float)(-1);
                }
                if ((key == Key.Q || key == Key.E) && !inputState[SynchKey.Up])
                {
                    PressKey(SynchKey.Up);
                    lastKeyPressTime[(int)key] = curTime;
                    lastKeySet = keyboard;
                }
                if ((key == Key.E || key == Key.C) && !inputState[SynchKey.Right])
                {
                    PressKey(SynchKey.Right);
                    lastKeyPressTime[(int)key] = curTime;
                    lastKeySet = keyboard;
                }
                if ((key == Key.C || key == Key.Z) && !inputState[SynchKey.Down])
                {
                    PressKey(SynchKey.Down);
                    lastKeyPressTime[(int)key] = curTime;
                    lastKeySet = keyboard;
                }
                if ((key==Key.Z || key==Key.Q) && !inputState[SynchKey.Left])
                {
                    PressKey(SynchKey.Left);
                    lastKeyPressTime[(int)key] = curTime;
                    lastKeySet = keyboard;
                }                
                isReleased[(int)key] = false;
            }
            else
            {
                isReleased[(int)key] = true;
            }
        }

        void EnsureMessageCreated()
        {
            if (message == null)
            {
                message = ctx.Session.Client.CreateMessage();
                inputSynchMessage.Begin(message, sequenceIndex);
                sequenceIndex++;
            }
        }

        void SendMessage(FixedVector2 mousePosition, bool reliable)
        {
            EnsureMessageCreated();
            timeAfterLastPacket = (Second)0;
            inputSynchMessage.End(mousePosition, inputState);
            ctx.Session.Client.SendMessage(message, reliable ? NetDeliveryMethod.ReliableUnordered : NetDeliveryMethod.Unreliable);
            message = null;
        }

        public void End()
        {
            ctx.Schdeulder.Unschedule(scheduler);
        }

        CombatContext ctx;
        Scheduler scheduler;
        InputSet inputState = new InputSet();
        ushort sequenceIndex;
        NetOutgoingMessage message;
        InputSynch inputSynchMessage = new InputSynch();
        Second timeAfterLastPacket = (Second)0;
        FixedVector2 mousePosition;
        float curTime = (float)0;
        float[] lastKeyPressTime;
        bool[] isReleased;
        const float MAX_DASH_WAIT_TIME = 0.5f;
        KeyboardState lastKeySet;
    }
}
