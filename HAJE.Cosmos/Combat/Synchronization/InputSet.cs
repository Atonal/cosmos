﻿using HAJE.Cosmos.SystemComponent;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.Cosmos.Combat.Synchronization
{
    [UnitTestAttribute]
    public struct InputSet
    {
        public void Clear()
        {
            bitVector = 0;
        }

        public bool this[SynchKey key]
        {
            get
            {
                return (bitVector & (1u << (ushort)key)) != 0;
            }
            set
            {
                if (value)
                    bitVector = (ushort)(bitVector | (1 << (ushort)key));
                else
                    bitVector = (ushort)(bitVector & ~(1 << (ushort)key));
            }
        }

        public ushort Vector
        {
            get
            {
                return bitVector;
            }
            set
            {
                bitVector = value;
            }
        }

        [Conditional("UNIT_TEST")]
        public static void RunUnitTest()
        {
            Debug.Assert(Enum.GetValues(typeof(SynchKey)).Length < 16);

            UnitTest.Begin("InputSet");
            InputSet set = new InputSet();
            set[SynchKey.Left] = true;
            set[SynchKey.Right] = true;
            set[SynchKey.S1] = true;
            UnitTest.Test(set[SynchKey.Left] == true);
            UnitTest.Test(set[SynchKey.Right] == true);
            UnitTest.Test(set[SynchKey.S1] == true);
            set[SynchKey.Right] = false;
            UnitTest.Test(set[SynchKey.Left] == true);
            UnitTest.Test(set[SynchKey.Right] == false);
            UnitTest.Test(set[SynchKey.S1] == true);
            UnitTest.End();
        }

        ushort bitVector;
    }
}
