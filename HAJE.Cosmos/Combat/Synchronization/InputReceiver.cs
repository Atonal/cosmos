﻿using HAJE.Cosmos.Network;
using HAJE.Cosmos.SystemComponent;
using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.Cosmos.Combat.Synchronization
{
    /// <summary>
    /// 호스트로부터 전달 받은 시뮬레이션 입력을 보관하고 순서대로 꺼내 주는 클라이언트 객체
    /// </summary>
    public class InputReceiver : IUpdatable
    {
        public void Start(CombatContext ctx)
        {
            this.ctx = ctx;
            scheduler = new Scheduler("InputDispatcher");
            ctx.Schdeulder.Schedule(scheduler);
            scheduler.Schedule(this);

            ctx.Session.ClientDispatcher.AddDataHandler(
                OnMessage,
                PacketHC.InputFrame,
                this);
        }

        public void Update(GameTime gameTime)
        {

        }

        public bool IsNextFrameReady
        {
            get
            {
                foreach (var e in received.List)
                {
                    if (e.FrameIndex == nextFrameIndex)
                        return true;
                }
                return false;
            }
        }

        public int ReservedFrameCount
        {
            get
            {
                return received.Count;
            }
        }

        public void PopFrame(PlayerInput[] input)
        {
            Debug.Assert(input.Length == ctx.Session.MaxPlayer);
            FrameInput f = null;
            foreach (var e in received.List)
            {
                if (e.FrameIndex == nextFrameIndex)
                {
                    f = e;
                    break;
                }
            }
            Debug.Assert(f != null);
            // f는 리스트에서 지워져서 언제 무효화 될지 모르니 input에 딥카피
            for (int i = 0; i < ctx.Session.MaxPlayer; i++)
            {
                var dst = input[i];
                var src = f.InputList[i];
                dst.CopyFrom(src);
            }
            received.Recycle(f);
            nextFrameIndex++;
        }

        void OnMessage(NetIncomingMessage msg)
        {
            var e = received.NewItem();
            e.FrameIndex = frameReader.Begin(msg);
            if (e.InputList == null)
            {
                e.InputList = new PlayerInput[ctx.Session.MaxPlayer];
                for (int i = 0; i < ctx.Session.MaxPlayer; i++)
                    e.InputList[i] = new PlayerInput();
            }
            for (int i = 0; i < ctx.Session.MaxPlayer; i++)
            {
                frameReader.Read(e.InputList[i]);
            }
            frameReader.End();
        }

        public void End()
        {
            ctx.Schdeulder.Unschedule(scheduler);
            ctx.Session.ClientDispatcher.RemoveDataHandler(this);
        }

        CombatContext ctx;
        Scheduler scheduler;
        class FrameInput
        {
            public int FrameIndex;
            public PlayerInput[] InputList;
        };
        int nextFrameIndex = 0;
        RecycleList<FrameInput> received = new RecycleList<FrameInput>();
        Network.HCPacket.InputFrame frameReader = new Network.HCPacket.InputFrame();
    }
}
