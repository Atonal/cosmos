﻿
namespace HAJE.Cosmos.Combat.Synchronization
{
    public class PlayerInput
    {
        public InputSet State;
        public FixedVector2 Cursor;
        public InputSet Pressed;
        public InputSet Released;
        public InputSet DoublePressed;

        public void CopyFrom(PlayerInput src)
        {
            State = src.State;
            Cursor = src.Cursor;
            Pressed = src.Pressed;
            Released = src.Released;
            DoublePressed = src.DoublePressed;
        }
    }
}
