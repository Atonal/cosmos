﻿using HAJE.Cosmos.Network;
using HAJE.Cosmos.SystemComponent;
using Lidgren.Network;
using System;
using System.Diagnostics;

namespace HAJE.Cosmos.Combat.Synchronization
{
    /// <summary>
    /// 클라이언트들로부터 받은 입력을 종합해서
    /// 매 시뮬레이션 프레임마다 모든 클라이언트에게 동일한 입력을 제공해주는 호스트 객체.
    /// </summary>
    public class InputDispatcher : IUpdatable
    {
        public void Start(CombatContext ctx)
        {
            Debug.Assert(ctx.Session.IsHost);

            this.ctx = ctx;
            scheduler = new Scheduler("InputDispatcher");
            ctx.Schdeulder.Schedule(scheduler);
            scheduler.Schedule(this);

            ctx.Session.ServerDispatcher.AddDataHandler(
                OnMessage,
                PacketCH.InputSynch,
                this);
            eventList = new ReceivedInput[ctx.Session.MaxPlayer];
            for (int i = 0; i < ctx.Session.MaxPlayer; i++)
                eventList[i] = new ReceivedInput();
        }

        public void Update(GameTime gameTime)
        {
            timeAfterLastDispatch += gameTime.DeltaTime;
            if (timeAfterLastDispatch > (float)SimulationTime.SimulationUpdateInterval)
            {
                timeAfterLastDispatch = (Second)0;
                SendInputFrame();
            }
        }

        public void End()
        {
            ctx.Schdeulder.Unschedule(scheduler);
            ctx.Session.ServerDispatcher.RemoveDataHandler(this);
        }

        void SendInputFrame()
        {
            // 모든 클라이언트들에게 메시지 전파
            var msg = ctx.Session.Server.CreateMessage();
            frameBuilder.Begin(msg, frameIndex);
            for (int i = 0; i < eventList.Length; i++)
            {
                var recvInput = eventList[i];
                frameBuilder.Append(recvInput.Input);

            }
            frameBuilder.End();
            ctx.Session.Server.SendToAll(msg, NetDeliveryMethod.ReliableUnordered);

            // 이벤트 목록 비우기
            // 이벤트가 아닌 최근 상태들은 계속 보존
            for (int i = 0; i < eventList.Length; i++)
            {
                var recvInput = eventList[i];
                recvInput.iSent = recvInput.iRecv;
                recvInput.Input.Pressed.Clear();
                recvInput.Input.Released.Clear();
                recvInput.Input.DoublePressed.Clear();
            }
            // 다음 프레임으로
            frameIndex++;
        }

        /// <summary>
        /// Host에 InputSynch 메시지가 도착하면,
        /// 기존에 받아둔 메시지들과 적당히 병합해둔다.
        /// </summary>
        void OnMessage(NetIncomingMessage msg)
        {
            var seqIndex = packetReader.Begin(msg);
            var playerIndex = ctx.Session.PlayerList.ResolvePlayerIndex(msg);
            var recvInput = eventList[playerIndex];
            var input = recvInput.Input;

            InputEvent eventType;
            SynchKey key = SynchKey.Left;
            InputSet state = new InputSet();
            FixedVector2 cursor = new FixedVector2();

            // 새로 온 메시지가 기존에 있던 정보보다 최신인 경우
            if (CheckSequence(recvInput.iRecv, seqIndex))
            {
                // 새 메시지와 기존 예약된 정보 머지
                recvInput.iRecv = seqIndex;
                while (packetReader.ReadEvent(out eventType, ref key))
                {
                    if (eventType == InputEvent.Pressed)
                        input.Pressed[key] = true;
                    else if (eventType == InputEvent.Released)
                        input.Released[key] = true;
                    else if (eventType == InputEvent.DoublePressed)
                        input.DoublePressed[key] = true;
                    else
                        Debug.Assert(false); // ReadEvent가 true를 반환하면 안 되는 상황
                }
                packetReader.End(out cursor, ref state);
                for (int i = 0; i < SynchKeyExtension.Values.Length; i++)
                {
                    SynchKey skey = SynchKeyExtension.Values[i];
                    // 중간에 반드시 거쳤어야 했을 Pressed / Released 이벤트 추가
                    if (!input.State[skey] && state[skey])
                        input.Pressed[skey] = true;
                    if (input.State[skey] && !state[skey])
                        input.Released[skey] = true;
                }
                input.Cursor = cursor;
                input.State = state;
            }
            // 새로 온 메시지가 최근 받은 메시지보다 낡은 경우
            // 하지만 최근에 전송한 메시지 이후인 경우
            else if (CheckSequence(recvInput.iSent, seqIndex))
            {
                // 상태 정보는 폐기, 이벤트는 머지
                while (packetReader.ReadEvent(out eventType, ref key))
                {
                    if (eventType == InputEvent.Pressed)
                        input.Pressed[key] = true;
                    else if (eventType == InputEvent.Released)
                        input.Released[key] = true;
                    else if (eventType == InputEvent.DoublePressed)
                        input.DoublePressed[key] = true;
                    else
                        Debug.Assert(false); // ReadEvent가 true를 반환하면 안 되는 상황
                }
                packetReader.End(out cursor, ref state);
            }
            // 이미 전송되었어야 할 정보가 뒤늦게 온 경우
            else
            {
                // 더블 입력을 제외하고는 전부 폐기
                while (packetReader.ReadEvent(out eventType, ref key))
                {
                    if (eventType == InputEvent.DoublePressed)
                    {
                        input.DoublePressed[key] = true;
                    }
                }
                packetReader.End(out cursor, ref state);
            }
        }

        /// <summary>
        /// 시퀀스 인덱스간 선후 관계를 체크
        /// </summary>
        bool CheckSequence(ushort seqBefore, ushort seqAfter)
        {
            if (Math.Abs((int)seqAfter - (int)seqBefore) < ushort.MaxValue / 2)
            {
                return seqBefore <= seqAfter;
            }
            else
            {
                return seqBefore >= seqAfter;
            }
        }

        CombatContext ctx;
        Scheduler scheduler;
        class ReceivedInput
        {
            public ushort iRecv = 0;
            public ushort iSent = 0;
            public PlayerInput Input = new PlayerInput();
        }
        int frameIndex = 0;
        ReceivedInput[] eventList;
        Second timeAfterLastDispatch = (Second)0;
        Network.CHPacket.InputSynch packetReader = new Network.CHPacket.InputSynch();
        Network.HCPacket.InputFrame frameBuilder = new Network.HCPacket.InputFrame();
    }
}
