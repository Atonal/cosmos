﻿using HAJE.Cosmos.Combat.Physics;

namespace HAJE.Cosmos.Combat.Projectile
{
    public class ScatterBullet : Projectile
    {
        public ScatterBullet(CombatContext context)
            : base(context)
        {
        }

        public void Shoot(ScatterMissile missile,
            FixedVector2 targetPosition,
            BulletDescriptor descriptor,
            FixedPoint timeRemainderInSec)
        {
            // 전투기 속도 보정, 최종 탄속 계산
            var position = missile.Collider.Position.Simulation;
            var dir = FixedVector2.Normalize(targetPosition - position);
            var fighterSpeed = FixedVector2.Dot(dir, missile.Collider.Velocity);
            var velocity = dir * (descriptor.Speed + fighterSpeed);

            // timeRemainder 보정
            position -= missile.Collider.Velocity * timeRemainderInSec;
            position += velocity * timeRemainderInSec;

            Shoot(position, velocity, descriptor);
        }
        public void Shoot(
            FixedVector2 initPosition,
            FixedVector2 velocity,
            BulletDescriptor descriptor)
        {
            this.TimeToLive = descriptor.TimeToLive;
            this.Alive = true;
            this.Descriptor = descriptor;

            Collider.Radius = descriptor.Radius;
            Collider.Velocity = velocity;
            Collider.Position.Initialize(initPosition);
        }

        public override void Update(FixedPoint deltaTimeInSec)
        {
            var p = Collider.Position.Simulation;
            p = p + Collider.Velocity * deltaTimeInSec;
            Collider.Position.Simulation = p;

            TimeToLive -= deltaTimeInSec;
        }

        public BulletDescriptor Descriptor;
    }
}
