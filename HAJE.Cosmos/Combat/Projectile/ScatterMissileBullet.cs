﻿using HAJE.Cosmos.Combat.Physics;
using System.Collections.Generic;

namespace HAJE.Cosmos.Combat.Projectile
{
    public class ScatterMissileBullet : Projectile
    {
        public ScatterMissileBullet(CombatContext context)
            : base(context)
        {
        }

        public void Shoot(ScatterMissile missile,
            FixedVector2 targetPosition,
            ProjectileDescriptor descriptor,
            FixedPoint timeRemainderInSec)
        {
            this.desc = descriptor;

            // 전투기 속도 보정, 최종 탄속 계산
            var position = missile.Collider.Position.Simulation;
            var dir = FixedVector2.Normalize(targetPosition - position);
            var fighterSpeed = FixedVector2.Dot(dir, missile.Collider.Velocity);
            var velocity = dir * (descriptor.Speed + fighterSpeed);

            // timeRemainder 보정
            position -= missile.Collider.Velocity * timeRemainderInSec;
            position += velocity * timeRemainderInSec;

            Shoot(position, velocity);
        }
        public void Shoot(
            FixedVector2 initPosition,
            FixedVector2 velocity)
        {
            this.TimeToLive = desc.TimeToLive;
            this.Alive = true;

            Collider.Radius = desc.Radius;
            Collider.Velocity = velocity;
            Collider.Position.Initialize(initPosition);
        }
        public override void CheckCollision(FixedPoint deltaTimeInSec)
        {
            if (context.ColliderManager.CheckCollision(this.Collider, typeof(Fighter.Fighter)).Count != 0)
            {
                this.Alive = false;
                Explode(deltaTimeInSec);
            }
        }
        public override void Update(FixedPoint deltaTimeInSec)
        {
            var p = Collider.Position.Simulation;
            p = p + Collider.Velocity * deltaTimeInSec;
            Collider.Position.Simulation = p;

            TimeToLive -= deltaTimeInSec; 
            if (TimeToLive < 0)
            {
                Explode(deltaTimeInSec);
                this.Alive = false;
            }
        }
        public override void Explode(FixedPoint deltaTimeInSec)
        {
            foreach (var c in context.ColliderManager.CheckCollision(this.Collider, typeof(Fighter.Fighter)))
                (c.owner as Fighter.Fighter).GetDamage(desc.Impulse);
        }
    }
}
