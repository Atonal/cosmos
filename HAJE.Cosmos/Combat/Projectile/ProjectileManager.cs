﻿using HAJE.Cosmos.Rendering;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Reflection;
namespace HAJE.Cosmos.Combat.Projectile
{
    public class ProjectileManager
    {
        private CombatContext context;
        private Texture texture;

        private Dictionary<Type, List<Projectile>> aliveList = new Dictionary<Type, List<Projectile>>();
        private Dictionary<Type, Queue<Projectile>> garbage = new Dictionary<Type, Queue<Projectile>>();
        private Dictionary<Type, ConstructorInfo> ctorList = new Dictionary<Type, ConstructorInfo>();

        public ProjectileManager(CombatContext context)
        {
            this.context = context;
            this.texture = TextureManager.GetTexture("Resource/Au.png");
        }

        public Projectile GetProjectile(Type ProjectileType)
        {
            Projectile pt;
            if (garbage.ContainsKey(ProjectileType) == false)
            {
                aliveList.Add(ProjectileType, new List<Projectile>());
                garbage.Add(ProjectileType, new Queue<Projectile>());
            }

            if (garbage[ProjectileType].Count > 0)
            {
                pt = garbage[ProjectileType].Dequeue();
                pt.Collider.Enabled = true;
            }
            else
            {
                if (ctorList.ContainsKey(ProjectileType) == false)
                    ctorList.Add(ProjectileType, ProjectileType.GetConstructor(new Type[] { typeof(CombatContext) }));

                // 만약 느리다 싶으면 Delegate로 바꾸는것을 고려해봐도 좋음
                pt = ctorList[ProjectileType].Invoke(new object[] { context }) as Projectile;
            }

            aliveList[ProjectileType].Add(pt);
            return pt;
        }
        private void Recycle()
        {
            foreach (Type type in aliveList.Keys)
                for (int idx = 0; idx < aliveList[type].Count; idx++ )
                    if (aliveList[type][idx].Alive == false)
                    {
                        garbage[type].Enqueue(aliveList[type][idx]);
                        aliveList[type][idx].Collider.Enabled = false;
                        aliveList[type].RemoveAt(idx);

                        idx--;
                    }
        }

        public void Join()
        {
            Recycle();

            foreach (var kvp in aliveList)
                GameSystem.DebugOutput.SetWatch(string.Format("Count of {0}", kvp.Key.Name), kvp.Value.Count);
        }
        public void Update(FixedPoint deltaTimeInSec)
        {
            var clone = new Dictionary<Type, List<Projectile>>();
            foreach (var kvp in aliveList)
                clone.Add(kvp.Key, kvp.Value);

            foreach (var kvp in clone)
                foreach (Projectile proj in kvp.Value)
                {
                    proj.Update(deltaTimeInSec);
                    proj.CheckCollision(deltaTimeInSec);
                }
        }
        public void NextFrame()
        {
            foreach (var kvp in aliveList)
                foreach (Projectile proj in kvp.Value)
                    proj.Collider.Position.NextFrame();
        }
        public void UpdateRender(float frameAlpha)
        {
            foreach (var kvp in aliveList)
                foreach (Projectile proj in kvp.Value)
                    proj.Collider.Position.UpdateRender(frameAlpha);
        }

        public void Render()
        {
            foreach (var kvp in aliveList)
                foreach (Projectile proj in kvp.Value)
                {
                    var col = proj.Collider;

                    var pos = col.Position.Rendering;
                    var diameter = 2 * (float)col.Radius;
                    var size = new Vector2(diameter, diameter);
                    GLHelper.DrawBillboard(texture, ref context.Camera.Matrix,
                        pos, null, null,
                        new Vector2(0.5f, 0.5f), size, null);
                }
        }
    } 
}
