﻿using System.Diagnostics;

namespace HAJE.Cosmos.Combat.Projectile
{
    public class ProjectileDescriptor
    {
        /// <summary>
        /// 사정거리
        /// </summary>
        public FixedPoint Range
        {
            get
            {
                return range;
            }
            set
            {
                range = value;
                TimeToLive = range / Speed;
            }
        }
        FixedPoint range = 100;

        /// <summary>
        /// 폭발 범위
        /// </summary>
        public FixedPoint[] ExplosionRange;

        /// <summary>
        /// 탄속
        /// </summary>
        public FixedPoint Speed
        {
            get
            {
                return speed;
            }
            set
            {
                speed = value;
                TimeToLive = range / speed;
            }
        }
        private FixedPoint speed = 100;

        /// <summary>
        /// 탄이 발사된 이후 유효한 시간
        /// </summary>
        public FixedPoint TimeToLive { get; private set; }


        /// <summary>
        /// 탄의 크기
        /// </summary>
        public FixedPoint Radius;

        /// <summary>
        /// 탄에 피격된 대상에게 가해지는 충격량
        /// </summary>
        public FixedPoint Impulse;

        [Conditional("DEBUG")]
        public void CheckValid()
        {
            Debug.Assert(Range > 0);
            Debug.Assert(Speed > 0);
            if (ExplosionRange != null)
                for (int idx = 0; idx < ExplosionRange.Length - 1; idx++ )
                    Debug.Assert(ExplosionRange[idx + 1] >= ExplosionRange[idx] && ExplosionRange[idx] >= 0);
            Debug.Assert(Radius > 0);
            Debug.Assert(Impulse >= 0);
        }
    }
}
