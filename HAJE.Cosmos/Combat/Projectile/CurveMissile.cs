﻿using HAJE.Cosmos.Combat.Physics;
using HAJE.Cosmos.Rendering.ParticleSystem;

namespace HAJE.Cosmos.Combat.Projectile
{
    public class CurveMissile : Projectile
    {
        private FixedPoint ReachTime;
        private FixedPoint NowTime;
        private FixedVector2 OrthogonalVec;
        private FixedVector2 Target;

        private DefaultParticleEmitter Emitter;
        private DefaultParticleEmitter Explosion;

        public CurveMissile(CombatContext context)
            : base(context)
        {
            Emitter = new DefaultParticleEmitter(context, this.Collider, ParticleEmitterDescriptor.MissileEmitter);
            Explosion = new DefaultParticleEmitter(context, this.Collider, ParticleEmitterDescriptor.GetExplosionEmitter(desc.ExplosionRange));
        }

        public void Shoot(Fighter.Fighter fighter,
            FixedVector2 targetPosition,
            ProjectileDescriptor descriptor,
            FixedPoint timeRemainderInSec,
            int missileDirection)
        {
            this.desc = descriptor;

            // 전투기 속도 보정, 최종 탄속 계산
            var position = fighter.LogicPosition;
            var dir = FixedVector2.Normalize(targetPosition - position);
            var fighterSpeed = FixedVector2.Dot(dir, fighter.Velocity);
            var velocity = dir * (descriptor.Speed + fighterSpeed);

            // timeRemainder 보정
            position -= fighter.Velocity * timeRemainderInSec;
            position += velocity * timeRemainderInSec;

            this.Target = targetPosition;
            this.NowTime = 0;
            this.ReachTime = (targetPosition - fighter.LogicPosition).Length() / velocity.Length();
            this.OrthogonalVec = velocity.Rotate((FixedPoint)missileDirection * FixedPoint.Pi / 2) * 2;
            Shoot(fighter, position, velocity + OrthogonalVec);
        }
        public void Shoot(
            Fighter.Fighter owner,
            FixedVector2 initPosition,
            FixedVector2 velocity)
        {
            this.Owner = owner;
            this.TimeToLive = desc.TimeToLive;
            this.Alive = true;

            Collider.Radius = desc.Radius;
            Collider.Velocity = velocity;
            Collider.Position.Initialize(initPosition);
        }

        public override void Update(FixedPoint deltaTimeInSec)
        {
            Emitter.Update(deltaTimeInSec);
            NowTime += deltaTimeInSec;

            if (NowTime < ReachTime / 3)
            {
                Collider.Velocity -= OrthogonalVec / ReachTime * 2 * deltaTimeInSec;
            
                if (NowTime + deltaTimeInSec > ReachTime / 3)
                {
                    FixedVector2 v = Target - Collider.Position.Simulation;
                    v.Normalize();

                    Collider.Velocity = (Collider.Velocity.Length() * 2) * v;
                }
            }

            var p = Collider.Position.Simulation;
            p = p + Collider.Velocity * deltaTimeInSec;
            Collider.Position.Simulation = p;

            TimeToLive -= deltaTimeInSec;
            if (TimeToLive < 0)
            {
                Explode(deltaTimeInSec);
                this.Alive = false;
            }
        }
        public override void CheckCollision(FixedPoint deltaTimeInSec)
        {
            if (context.ColliderManager.CheckCollision(this.Collider, typeof(Fighter.Fighter)).Count != 0)
            {
                this.Alive = false;
                Explode(deltaTimeInSec);
            }
        }
        public override void Explode(FixedPoint deltaTimeInSec)
        {
            foreach (var c in context.ColliderManager.ParseCollider(this.Collider, desc.ExplosionRange[0], typeof(Fighter.Fighter)))
                (c.owner as Fighter.Fighter).GetDamage(desc.Impulse / 2);
            foreach (var c in context.ColliderManager.ParseCollider(this.Collider, desc.ExplosionRange[1], typeof(Fighter.Fighter)))
                (c.owner as Fighter.Fighter).GetDamage(desc.Impulse / 4);
            foreach (var c in context.ColliderManager.ParseCollider(this.Collider, desc.ExplosionRange[2], typeof(Fighter.Fighter)))
                (c.owner as Fighter.Fighter).GetDamage(desc.Impulse / 4);

            Explosion = new DefaultParticleEmitter(context, this.Collider, ParticleEmitterDescriptor.GetExplosionEmitter(desc.ExplosionRange));
            Explosion.Update(deltaTimeInSec);
        }
    }
}
