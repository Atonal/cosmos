﻿using HAJE.Cosmos.Combat.Physics;

namespace HAJE.Cosmos.Combat.Projectile
{
    public abstract class Projectile
    {
        public Projectile(CombatContext context)
        {
            this.context = context;
            this.Collider = context.ColliderManager.GetCollider(this);
        }

        public abstract void Update(FixedPoint deltaTimeInSec);
        public abstract void CheckCollision(FixedPoint deltaTimeInSec);
        public abstract void Explode(FixedPoint deltaTimeInSec);

        public ProjectileDescriptor desc { get; protected set; }
        public readonly CombatContext context;
        public readonly Collider Collider;
        public Fighter.Fighter Owner { get; protected set; }
        public FixedPoint TimeToLive { get; protected set; }
        public bool Alive { get; protected set; }
    }
}