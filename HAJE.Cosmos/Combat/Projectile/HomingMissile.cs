﻿using HAJE.Cosmos.Combat.Physics;
using HAJE.Cosmos.Rendering.ParticleSystem;
using System;
using System.Collections.Generic;

namespace HAJE.Cosmos.Combat.Projectile
{
    public class HomingMissile : Projectile
    {
        private DefaultParticleEmitter Emitter;
        private DefaultParticleEmitter Explosion;

        private bool ProtectMode;

        private Collider target;
        private Collider lockon;
        private List<HomingMissile> missileList;
        private List<Collider> lockonList;

        public HomingMissile(CombatContext context)
            : base(context)
        {
            Emitter = new DefaultParticleEmitter(context, this.Collider, ParticleEmitterDescriptor.MissileEmitter);
            Explosion = new DefaultParticleEmitter(context, this.Collider, ParticleEmitterDescriptor.GetExplosionEmitter(desc.ExplosionRange));

            ProtectMode = false;
        }

        public void Shoot(Fighter.Fighter fighter,
            FixedVector2 targetPosition,
            Collider target,
            ProjectileDescriptor descriptor,
            FixedPoint timeRemainderInSec,
            List<HomingMissile> missileList)
        {
            this.desc = descriptor;

            // 전투기 속도 보정, 최종 탄속 계산
            var position = fighter.LogicPosition;
            var dir = new FixedVector2(1, 0);
            if (targetPosition != position) dir = FixedVector2.Normalize(targetPosition - position);
            var fighterSpeed = FixedVector2.Dot(dir, fighter.Velocity);
            var velocity = dir * (descriptor.Speed * 3 + fighterSpeed);

            // timeRemainder 보정
            position -= fighter.Velocity * timeRemainderInSec;
            position += velocity * timeRemainderInSec;

            this.missileList = missileList;
            this.lockonList = new List<Collider>();
            this.target = target;
            Shoot(fighter, position, velocity);
        }
        public void Shoot(
            Fighter.Fighter owner,
            FixedVector2 initPosition,
            FixedVector2 velocity)
        {
            this.ProtectMode = false;

            this.Owner = owner;
            this.TimeToLive = desc.TimeToLive;
            this.Alive = true;

            Collider.Radius = desc.Radius;
            Collider.Velocity = velocity;
            Collider.Position.Initialize(initPosition);
        }

        public override void Update(FixedPoint deltaTimeInSec)
        {
            Emitter.Update(deltaTimeInSec);

            FixedVector2 v = target.Position.Simulation - this.Collider.Position.Simulation;
            if (v.LengthSqared() != 0)
            {
                if (v.Length() > 500)
                    v /= 250000;
                else
                    v /= v.LengthSqared();
            }
            v = Collider.Velocity + v * Collider.Velocity.Length() * 10;
            if (v != FixedVector2.Zero)
                v.Normalize();

            foreach (var col in context.ColliderManager.ParseCollider(this.Collider, 100, 
                new Type[]{typeof(Missile), typeof(CurveMissile), typeof(ScatterMissile), typeof(HomingMissile)}))
            {
                if ((col.owner as Projectile).Owner == this.Owner)
                {
                    var vec = this.Collider.Position.Simulation - col.Position.Simulation;
                    if (vec.LengthSqared() != 0)
                        vec /= vec.LengthSqared() * context.Random.NextFixedPoint(1, 2);

                    v += vec;
                }
                else
                {
                    if (this.ProtectMode == false && this.lockonList.Contains(col) == false)
                    {
                        GameSystem.Log.Write("Lock On Changed");
                        this.ProtectMode = true;
                        this.lockon = col;

                        foreach (var hm in this.missileList)
                            hm.lockonList.Add(col);
                        break;
                    }
                }
            }

            if (v.Length() != 0)
                v.Normalize();
            Collider.Velocity = Collider.Velocity.Length() * v;

            if (this.ProtectMode == true)
            {
                Collider.Velocity = lockon.Position.Simulation - this.Collider.Position.Simulation;
                if (Collider.Velocity.Length() != 0)
                    Collider.Velocity.Normalize();
                Collider.Velocity *= desc.Speed;
            }
            else
            {
                if (Collider.Velocity.Length() < desc.Speed)
                    Collider.Velocity *= (FixedPoint)1.2;
                else
                    Collider.Velocity /= (FixedPoint)1.05;
            }

            if (this.ProtectMode == true)
                if (this.lockon.Enabled == false)
                {
                    foreach (var hm in this.missileList)
                        hm.lockonList.Remove(this.lockon);
                    this.ProtectMode = false;
                }

            var p = Collider.Position.Simulation;
            p = p + Collider.Velocity * deltaTimeInSec;
            Collider.Position.Simulation = p;

            TimeToLive -= deltaTimeInSec;
            if (TimeToLive < 0)
            {
                Explode(deltaTimeInSec);
                this.Alive = false;
            }
        }
        public override void CheckCollision(FixedPoint deltaTimeInSec)
        {
            if (context.ColliderManager.CheckCollision(this.Collider, 
                new Type[]{typeof(Fighter.Fighter), typeof(Missile), typeof(CurveMissile), typeof(HomingMissile)}).Count != 0)
            {
                this.Alive = false;
                Explode(deltaTimeInSec);
            }
        }
        public override void Explode(FixedPoint deltaTimeInSec)
        {
            foreach (var c in context.ColliderManager.ParseCollider(this.Collider, desc.ExplosionRange[0], typeof(Fighter.Fighter)))
                (c.owner as Fighter.Fighter).GetDamage(desc.Impulse / 2);
            foreach (var c in context.ColliderManager.ParseCollider(this.Collider, desc.ExplosionRange[1], typeof(Fighter.Fighter)))
                (c.owner as Fighter.Fighter).GetDamage(desc.Impulse / 4);
            foreach (var c in context.ColliderManager.ParseCollider(this.Collider, desc.ExplosionRange[2], typeof(Fighter.Fighter)))
                (c.owner as Fighter.Fighter).GetDamage(desc.Impulse / 4);

            Explosion = new DefaultParticleEmitter(context, this.Collider, ParticleEmitterDescriptor.GetExplosionEmitter(desc.ExplosionRange));
            Explosion.Update(deltaTimeInSec);
        }
    }
}
