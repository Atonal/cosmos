﻿using HAJE.Cosmos.Combat.Physics;
using HAJE.Cosmos.Rendering.ParticleSystem;

namespace HAJE.Cosmos.Combat.Projectile
{
    public class Missile : Projectile
    {
        private DefaultParticleEmitter Emitter;
        private DefaultParticleEmitter Explosion;

        public Missile(CombatContext context)
            : base(context)
        {
            Emitter = new DefaultParticleEmitter(context, this.Collider, ParticleEmitterDescriptor.MissileEmitter);
        }

        public void Shoot(Fighter.Fighter fighter,
            FixedVector2 targetPosition,
            ProjectileDescriptor descriptor,
            FixedPoint timeRemainderInSec)
        {
            this.desc = descriptor;

            // 전투기 속도 보정, 최종 탄속 계산
            var position = fighter.LogicPosition;
            var dir = FixedVector2.Normalize(targetPosition - position);
            var fighterSpeed = FixedVector2.Dot(dir, fighter.Velocity);
            var velocity = dir * (descriptor.Speed + fighterSpeed);

            // timeRemainder 보정
            position -= fighter.Velocity * timeRemainderInSec;
            position += velocity * timeRemainderInSec;

            Shoot(fighter, position, velocity);
        }
        public void Shoot(
            Fighter.Fighter owner,
            FixedVector2 initPosition,
            FixedVector2 velocity)
        {
            this.Owner = owner;
            this.TimeToLive = desc.TimeToLive;
            this.Alive = true;

            Collider.Radius = desc.Radius;
            Collider.Velocity = velocity;
            Collider.Position.Initialize(initPosition);
        }

        public override void Update(FixedPoint deltaTimeInSec)
        {
            Emitter.Update(deltaTimeInSec);
            
            var p = Collider.Position.Simulation;
            p = p + Collider.Velocity * deltaTimeInSec;
            Collider.Position.Simulation = p;

            TimeToLive -= deltaTimeInSec;
            if (TimeToLive < 0)
            {
                Explode(deltaTimeInSec);
                this.Alive = false;
            }
        }
        public override void CheckCollision(FixedPoint deltaTimeInSec)
        {
            if (context.ColliderManager.CheckCollision(this.Collider, typeof(Fighter.Fighter)).Count != 0)
            {
                this.Alive = false;
                Explode(deltaTimeInSec);
            }
        }
        public override void Explode(FixedPoint deltaTimeInSec)
        {
            foreach (var c in context.ColliderManager.ParseCollider(this.Collider, desc.ExplosionRange[0], typeof(Fighter.Fighter)))
                (c.owner as Fighter.Fighter).GetDamage(desc.Impulse / 2);
            foreach (var c in context.ColliderManager.ParseCollider(this.Collider, desc.ExplosionRange[1], typeof(Fighter.Fighter)))
                (c.owner as Fighter.Fighter).GetDamage(desc.Impulse / 4);
            foreach (var c in context.ColliderManager.ParseCollider(this.Collider, desc.ExplosionRange[2], typeof(Fighter.Fighter)))
                (c.owner as Fighter.Fighter).GetDamage(desc.Impulse / 4);

            Explosion = new DefaultParticleEmitter(context, this.Collider, ParticleEmitterDescriptor.GetExplosionEmitter(desc.ExplosionRange));
            Explosion.Update(deltaTimeInSec);
        }
    }
}
