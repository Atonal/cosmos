﻿using HAJE.Cosmos.Combat.Physics;
using HAJE.Cosmos.Rendering.ParticleSystem;
using System;
using System.Collections.Generic;

namespace HAJE.Cosmos.Combat.Projectile
{
    public class ScatterMissile : Projectile
    {
        private DefaultParticleEmitter MissileEmitter;

        public ScatterMissile(CombatContext context)
            : base(context)
        {
            MissileEmitter = new DefaultParticleEmitter(context, this.Collider, ParticleEmitterDescriptor.MissileEmitter);
        }

        public void Shoot(Fighter.Fighter fighter,
            FixedVector2 targetPosition,
            ProjectileDescriptor descriptor,
            FixedPoint timeRemainderInSec)
        {
            this.desc = descriptor;

            // 전투기 속도 보정, 최종 탄속 계산
            var position = fighter.LogicPosition;
            var dir = FixedVector2.Normalize(targetPosition - position);
            var fighterSpeed = FixedVector2.Dot(dir, fighter.Velocity);
            var velocity = dir * (desc.Speed + fighterSpeed);

            // timeRemainder 보정
            position -= fighter.Velocity * timeRemainderInSec;
            position += velocity * timeRemainderInSec;

            Shoot(fighter, position, velocity);
        }
        public void Shoot(
            Fighter.Fighter owner,
            FixedVector2 initPosition,
            FixedVector2 velocity)
        {
            this.Owner = owner;
            this.TimeToLive = desc.TimeToLive;
            this.Alive = true;

            Collider.Radius = desc.Radius;
            Collider.Velocity = velocity;
            Collider.Position.Initialize(initPosition);
        }

        public override void Update(FixedPoint deltaTimeInSec)
        {
            MissileEmitter.Update(deltaTimeInSec);

            var p = Collider.Position.Simulation;
            p = p + Collider.Velocity * deltaTimeInSec;
            Collider.Position.Simulation = p;

            TimeToLive -= deltaTimeInSec;
            if (TimeToLive < 0)
            {
                Explode(deltaTimeInSec);
                this.Alive = false;
            }
        }
        public override void CheckCollision(FixedPoint deltaTimeInSec)
        {
            foreach (Collider c in context.ColliderManager.CheckCollision(this.Collider, typeof(Fighter.Fighter)))
                if (c.owner as Fighter.Fighter != this.Owner)
                {
                    this.Alive = false;
                    Explode(deltaTimeInSec);
                }
        }
        public override void Explode(FixedPoint deltaTimeInSec)
        {
            Alive = false;

            int maxbullet = 350;
            FixedVector2 v = new FixedVector2(10, 0);
            FixedPoint deltarad = FixedPoint.Pi * 2 / (FixedPoint)maxbullet;

            for (int i = 0; i < maxbullet; i++)
            {
                ScatterMissileBullet smb = context.ProjectileManager.GetProjectile(typeof(ScatterMissileBullet)) as ScatterMissileBullet;
                smb.Shoot(this, this.Collider.Position.Simulation + v.Rotate(
                    deltarad * (FixedPoint)i + context.Random.NextFixedPoint((FixedPoint)(-0.02f), (FixedPoint)(0.02f))),
                    new ProjectileDescriptor()
                    {
                        Impulse = 200,
                        Radius = (FixedPoint)1.25f,
                        Range = 50 + context.Random.NextFixedPoint(-40, 40),
                        Speed = 500 + context.Random.NextFixedPoint(-400, 150)
                    },
                    (FixedPoint)deltaTimeInSec);
            }    
        }
    }
}
