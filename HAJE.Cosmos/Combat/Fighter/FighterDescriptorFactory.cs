﻿
namespace HAJE.Cosmos.Combat.Fighter
{
    public static class FighterDescriptorFactory
    {
        public static FighterDescriptor Get(FighterID.ID id)
        {
            switch (id)
            {
                case FighterID.ID.AIDefault:
                    return Data.Fighter.AIDefault.Create();

                case FighterID.ID.Default:
                default:
                    return Data.Fighter.Default.Create();
            };
        }
    }
}
