﻿
namespace HAJE.Cosmos.Combat.Fighter
{
    public abstract class AttackHandlerBase
    {
        public AttackHandlerBase(CombatContext context, Fighter owner, AttackDescriptor attackDesc)
        {
            this.owner = owner;
            this.attackDesc = attackDesc;
            this.context = context;
        }

        public virtual void CooldownReset()
        {
        }

        public abstract void Attack(
            int attackIndex,
            FixedVector2 target,
            FixedPoint timeRemainderInSec);

        protected readonly CombatContext context;
        protected readonly Fighter owner;
        protected readonly AttackDescriptor attackDesc;
    }
}
