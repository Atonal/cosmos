﻿using HAJE.Cosmos.Combat.Physics;
using HAJE.Cosmos.Combat.Synchronization;
using HAJE.Cosmos.Rendering;
using OpenTK;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.Cosmos.Combat.Fighter
{
    public class Fighter
    {
        public Fighter(CombatContext context, FighterID.ID id, bool useAI = false)
        {
            this.context = context;
            this.collider = context.ColliderManager.GetCollider(this);

            this.desc = FighterDescriptorFactory.Get(id);
            desc.CheckValid();
            this.HP = desc.HP;
            this.collider.Radius = desc.Radius;
            this.collider.Mass = desc.Mass;

            this.texture = TextureManager.GetTexture(desc.Texture);
            this.movementLogic = new MovementLogic(this, this.collider, desc.Movement, this.context);
            this.attackLogic = new AttackLogic(context, this, desc.Attack);

            this.direction = movementLogic.Direction;

            if (useAI == true)
                this.ai = new FighterAI(context, this);
        }

        public void Update(PlayerInput input, FixedPoint deltaTimeInSec)
        {
            // 임시 구현
            if (this.HP < 0)
            {
                this.collider.Enabled = false;
                return;
            }

            //if (ai != null)
                // input = ai.GetInput(deltaTimeInSec);
            if (input != null)
            {
                attackLogic.Update(
                    input.State[SynchKey.MouseLeft],
                    input.Released[SynchKey.MouseLeft],
                    input.State[SynchKey.MouseRight],
                    input.Released[SynchKey.MouseRight],
                    input.Cursor,
                    deltaTimeInSec);
                movementLogic.Update(input, deltaTimeInSec);
            }
        }
        public void GetDamage(FixedPoint Damage)
        {
            this.HP -= Damage;
        }

        public void NextFrame()
        {
            collider.Position.NextFrame();
        }

        public void UpdateRender(float frameAlpha)
        {
            collider.Position.UpdateRender(frameAlpha);
        }

        public void Render()
        {
            var pos = collider.Position.Rendering;
            var diameter = 2 * (float)collider.Radius;
            var size = new Vector2(diameter, diameter);

            // 항상 같은 쪽으로 돌도록 했음. 필요시 수정할 수 있음
            if (direction + movementLogic.Direction == FixedVector2.Zero)
                direction = FixedVector2.Normalize(direction * 5 + new FixedVector2(2, 1));
            else
                direction = FixedVector2.Normalize(direction * 5 + movementLogic.Direction);


            // 임시 구현
            if (this.HP > 0)
            {
                GLHelper.DrawBillboard(texture, ref context.Camera.Matrix,
                    pos, null, null,
                    new Vector2(0.5f, 0.5f), size, (Vector2)direction);
            }
        }

        MovementLogic movementLogic;
        AttackLogic attackLogic;
        CombatContext context;
        Texture texture;
        public Physics.Collider collider;

        public FixedPoint HP;

        FixedVector2 direction = new FixedVector2(1, 0);
        FighterAI ai;
        public FighterDescriptor desc;
        public bool useAI 
        {
            get
            {
                return ai != null;
            }
        }

        public Vector3 RenderPosition
        {
            get
            {
                return collider.Position.Rendering;
            }
        }

        public FixedVector2 LogicPosition
        {
            get
            {
                return collider.Position.Simulation;
            }
        }

        public FixedVector2 Velocity
        {
            get
            {
                return collider.Velocity;
            }
        }
    }
}
