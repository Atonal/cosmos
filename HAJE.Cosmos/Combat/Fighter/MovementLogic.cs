﻿using HAJE.Cosmos.Combat.Synchronization;
using OpenTK;
using OpenTK.Input;
using System.Diagnostics;

namespace HAJE.Cosmos.Combat.Fighter
{
    public class MovementLogic
    {
        public MovementLogic(Fighter owner, Physics.Collider collider, MovementDescriptor desc, CombatContext context)
        {
            this.owner = owner;
            this.collider = collider;
            this.context = context;
            this.movementSpeed = desc.MovementSpeed;
            this.dashSpeed = desc.DashSpeed;
            this.acceleration = desc.Acceleration;
            this.additionalRotationAcceleration = desc.AdditionalRotationAcceleration;
            this.deceleration = desc.DefaultDeceleration;
            this.breakDeceleration = desc.BreakDeceleration;
            this.Direction = new FixedVector2(1, 0);
            this.areaNumber = context.GameAreaData.FindArea(this.collider.Position.Simulation);
        }

        public void Update(PlayerInput input, FixedPoint deltaTimeInSec)
        {
            if (Keyboard.GetState()[Key.F11]) F11Pressed = !F11Pressed;

            if (input.DoublePressed[SynchKey.Left]
                || input.DoublePressed[SynchKey.Right]
                || input.DoublePressed[SynchKey.Up]
                || input.DoublePressed[SynchKey.Down])
            {
                FixedVector2 dir = new FixedVector2();
                if (input.DoublePressed[SynchKey.Left])
                    dir += new FixedVector2(-1, 0);
                if (input.DoublePressed[SynchKey.Right])
                    dir += new FixedVector2(1, 0);
                if (input.DoublePressed[SynchKey.Up])
                    dir += new FixedVector2(0, 1);
                if (input.DoublePressed[SynchKey.Down])
                    dir += new FixedVector2(0, -1);

                if (dir == FixedVector2.Zero)
                    Break(deltaTimeInSec);
                else
                {
                    Dash(FixedVector2.Normalize(dir), deltaTimeInSec);
                    goto updatePosition;
                }
            }
            if (input.State[SynchKey.Break])
            {
                Break(deltaTimeInSec);
            }
            else if (input.State[SynchKey.Left]
                || input.State[SynchKey.Up]
                || input.State[SynchKey.Right]
                || input.State[SynchKey.Down])
            {
                FixedVector2 dir = new FixedVector2();
                if (input.State[SynchKey.Left])
                    dir += new FixedVector2(-1, 0);
                if (input.State[SynchKey.Right])
                    dir += new FixedVector2(1, 0);
                if (input.State[SynchKey.Up])
                    dir += new FixedVector2(0, 1);
                if (input.State[SynchKey.Down])
                    dir += new FixedVector2(0, -1);

                if (dir == FixedVector2.Zero)
                    Break(deltaTimeInSec);
                else
                    Move(FixedVector2.Normalize(dir), deltaTimeInSec);
            }
            else
            {
                Decelerate(deltaTimeInSec);
            }

        updatePosition:
            collider.Position.Simulation =
                collider.Position.Simulation + collider.Velocity * deltaTimeInSec;

            if (F11Pressed == false)
            {
                if (!context.GameAreaData.CheckArea(collider.Position.Simulation, areaNumber))
                {
                    var temp = context.GameAreaData.FindArea(collider.Position.Simulation);
                    if (temp == -1)
                    {
                        FixedVector2 normalVector = context.GameAreaData.GetNormalVector(collider.Position.Simulation, areaNumber);
                        collider.Position.Simulation = collider.Position.Simulation - collider.Velocity * deltaTimeInSec;
                        collider.Velocity = (collider.Velocity - (FixedVector2.Dot(normalVector, collider.Velocity) * 2) * normalVector) / 2;
                        Move(new FixedVector2(0, 0), deltaTimeInSec);
                    }
                    else areaNumber = temp;
                }
            }

            if (collider.Velocity.LengthSqared() > (FixedPoint)(0.25f))
            {
                Direction = FixedVector2.Normalize(collider.Velocity);
            }

            if (owner.useAI == false)
            {
                GameSystem.DebugOutput.SetWatch("velocity", collider.Velocity.ToString());
                GameSystem.DebugOutput.SetWatch("position", collider.Position.Simulation.ToString());
                GameSystem.DebugOutput.SetWatch("area", areaNumber);
            }
        }

        public void Dash(FixedVector2 dashDirection, FixedPoint deltaTimeInSec)
        {
            collider.Velocity = dashSpeed * dashDirection;
        }

        public void Move(FixedVector2 forceDirection, FixedPoint deltaTimeInSec)
        {
            var velocity = collider.Velocity;
            var speed = velocity.Length();
            var speedDamped = false;

            // 속도가 너무 높으면 감속하도록 한다
            if (speed > movementSpeed)
            {
                DampSpeed(deltaTimeInSec);
                speed = velocity.Length();
                speedDamped = true;
                velocity = collider.Velocity;
            }

            // 이미 이동 중인 경우, 추가적으로 회전 속도 보정
            if (speed != 0)
            {
                var addRotSpeed = additionalRotationAcceleration * deltaTimeInSec;
                if (speed < addRotSpeed)
                {
                    velocity = forceDirection * speed;
                }
                else
                {
                    var curDir = velocity / speed;
                    velocity = (curDir * (speed - addRotSpeed)) + (forceDirection * addRotSpeed);
                    speed = velocity.Length();
                }
            }
            
            // 감속 루틴
            if (speedDamped)
            {
                var deltaSpeed = acceleration * deltaTimeInSec;
                if (speed > deltaSpeed)
                {
                    var curDir = velocity / speed;
                    velocity = curDir * (speed - deltaSpeed);
                }
            }

            velocity = velocity + (forceDirection * acceleration) * deltaTimeInSec;
            speed = velocity.Length();

            // 감속 안했는데도 빠를 때
            if (!speedDamped && speed > movementSpeed)
            {
                var curDir = velocity / speed;
                velocity = curDir * movementSpeed;
            }

            collider.Velocity = velocity;
        }

        public void DampSpeed(FixedPoint deltaTimeInSec)
        {
            var velocity = collider.Velocity;
            var speed = velocity.Length();
            if (speed > movementSpeed)
            {
                var dir = velocity / speed;
                speed = speed - (speed - movementSpeed) * (deltaTimeInSec * 2);
                if (speed < movementSpeed)
                    speed = movementSpeed;

                collider.Velocity = dir * speed;
            }
        }

        public void Decelerate(FixedPoint deltaTimeInSec)
        {
            DampSpeed(deltaTimeInSec);
            Decelerate(deceleration, deltaTimeInSec);
        }

        void Decelerate(FixedPoint amount, FixedPoint deltaTimeInSec)
        {
            var speed = collider.Velocity.Length();
            amount = amount * deltaTimeInSec;
            if (speed < amount)
            {
                collider.Velocity = FixedVector2.Zero;
            }
            else
            {
                var dir = FixedVector2.Normalize(collider.Velocity);
                collider.Velocity = dir * (speed - amount);
            }
        }

        public void Break(FixedPoint deltaTimeInSec)
        {
            DampSpeed(deltaTimeInSec);
            Decelerate(breakDeceleration, deltaTimeInSec);
        }

        readonly Fighter owner;
        readonly Physics.Collider collider;
        CombatContext context;
        FixedPoint movementSpeed;
        FixedPoint dashSpeed;
        FixedPoint acceleration;
        FixedPoint additionalRotationAcceleration;
        FixedPoint deceleration;
        FixedPoint breakDeceleration;
        int areaNumber;

        private static bool F11Pressed = false;

        public FixedVector2 Direction
        {
            get;
            private set;
        }
    }
}
