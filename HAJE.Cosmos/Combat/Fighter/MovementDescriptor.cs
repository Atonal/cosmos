﻿using System.Diagnostics;

namespace HAJE.Cosmos.Combat.Fighter
{
    /// <summary>
    /// 전투기의 이동능력과 관련된 값들을 포함
    /// </summary>
    public class MovementDescriptor
    {
        /// <summary>
        /// 통상적인 이동 속도
        /// </summary>
        public FixedPoint MovementSpeed;

        /// <summary>
        /// 대시 할 때의 순간 이동 속도
        /// </summary>
        public FixedPoint DashSpeed;

        /// <summary>
        /// 가속도
        /// </summary>
        public FixedPoint Acceleration;        

        /// <summary>
        /// 정지상태에서 MoveSpeed까지 가속하는데 걸리는 시간
        /// </summary>
        public FixedPoint ZeroToMoveTime
        {
            get
            {
                return MovementSpeed / Acceleration;
            }
            set
            {
                Debug.Assert(value > 0);
                Acceleration = MovementSpeed / value;
            }
        }

        /// <summary>
        /// 방향 전환에 추가적으로 사용되는 속도
        /// </summary>
        public FixedPoint AdditionalRotationAcceleration;

        /// <summary>
        /// 아무 이동 명령이 없을 때의 감속
        /// </summary>
        public FixedPoint DefaultDeceleration;

        /// <summary>
        /// 아무런 키 입력이 없는 상태에서 MoveSpeed에서 정지까지 자연 감속하는데 걸리는 시간
        /// </summary>
        public FixedPoint MoveToZeroTime
        {
            get
            {
                return MovementSpeed / DefaultDeceleration;
            }
            set
            {
                Debug.Assert(value > 0);
                DefaultDeceleration = MovementSpeed / value;
            }
        }

        /// <summary>
        /// 브레이크 명령이 있을 때의 감속
        /// </summary>
        public FixedPoint BreakDeceleration;

        /// <summary>
        /// 브레이크 키 입력이 있는 상태에서 MoveSpeed에서 정지까지 감속하는데 걸리는 시간
        /// </summary>
        public FixedPoint MoveToZeroBreakTime
        {
            get
            {
                return MovementSpeed / BreakDeceleration;
            }
            set
            {
                Debug.Assert(value > 0);
                BreakDeceleration = MovementSpeed / value;
            }
        }

        [Conditional("DEBUG")]
        public void CheckValid()
        {
            Debug.Assert(MovementSpeed > 0);
            Debug.Assert(DashSpeed > 0);
            Debug.Assert(Acceleration > 0);
            Debug.Assert(AdditionalRotationAcceleration >= 0);
            Debug.Assert(DefaultDeceleration > 0);
            Debug.Assert(BreakDeceleration > 0);
        }
    }
}
