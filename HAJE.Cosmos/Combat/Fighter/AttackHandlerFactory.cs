﻿using System;

namespace HAJE.Cosmos.Combat.Fighter
{
    public static class AttackHandlerFactory
    {
        public static AttackHandlerBase Get(
            CombatContext context,
            Fighter owner,
            AttackDescriptor attackDesc,
            AttackDescriptor.HandlerType handlerType)
        {
            switch (handlerType)
            {
                case AttackDescriptor.HandlerType.DefaultBullet:
                    return new AttackHandler.DefaultBulletHandler(context, owner, attackDesc);

                case AttackDescriptor.HandlerType.ScatterBullet:
                    return new AttackHandler.ScatterBulletHandler(context, owner, attackDesc);

                case AttackDescriptor.HandlerType.DefaultMissile:
                    return new AttackHandler.DefaultMissileHandler(context, owner, attackDesc);

                case AttackDescriptor.HandlerType.CurveMissile:
                    return new AttackHandler.CurveMissileHandler(context, owner, attackDesc);

                case AttackDescriptor.HandlerType.ScatterMissile:
                    return new AttackHandler.ScatterMissileHandler(context, owner, attackDesc);

                case AttackDescriptor.HandlerType.HomingMissile:
                    return new AttackHandler.HomingMissileHandler(context, owner, attackDesc);
            };
            throw new ArgumentException(string.Format("연결된 핸들러가 없습니다. {0}", handlerType.ToString()));
        }
    }
}
