﻿using HAJE.Cosmos.Combat.Physics;
using HAJE.Cosmos.Combat.Synchronization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.Cosmos.Combat.Fighter
{
    class FighterAI
    {
        #region enum
        [Flags]
        private enum Direction
        {
            Stop = 0,
            Up = 1 << 1,
            Down = 1 << 2,
            Left = 1 << 3,
            Right = 1 << 4
        }
        #endregion
        #region private
        private CombatContext context;
        private Fighter fighter;
        #endregion

        #region .ctor
        public FighterAI(CombatContext context, Fighter fighter)
        {
            this.context = context;
            this.fighter = fighter;
        }
        #endregion

        #region Move
        private bool MovingLeft = false;
        private bool MovingRight = false;
        private bool MovingUp = false;
        private bool MovingDown = false;
        private void Move(PlayerInput input, Direction move)
        {
            if ((move & Direction.Up) == Direction.Up)
                MoveUp(input);
            if ((move & Direction.Down) == Direction.Down)
                MoveDown(input);
            if ((move & Direction.Left) == Direction.Left)
                MoveLeft(input);
            if ((move & Direction.Right) == Direction.Right)
                MoveRight(input);
        }
        private PlayerInput MoveLeft(PlayerInput input)
        {
            if (MovingLeft == true)
            {
                input.State[SynchKey.Left] = true;
            }
            else
            {
                MovingLeft = true;
                input.Pressed[SynchKey.Left] = true;
                input.State[SynchKey.Left] = true;
            }

            return input;
        }
        private PlayerInput MoveRight(PlayerInput input)
        {
            if (MovingRight == true)
            {
                input.State[SynchKey.Right] = true;
            }
            else
            {
                MovingRight = true;
                input.Pressed[SynchKey.Right] = true;
                input.State[SynchKey.Right] = true;
            }

            return input;
        }
        private PlayerInput MoveUp(PlayerInput input)
        {
            if (MovingUp == true)
            {
                input.State[SynchKey.Up] = true;
            }
            else
            {
                MovingUp = true;
                input.Pressed[SynchKey.Up] = true;
                input.State[SynchKey.Up] = true;
            }

            return input;
        }
        private PlayerInput MoveDown(PlayerInput input)
        {
            if (MovingDown == true)
            {
                input.State[SynchKey.Down] = true;
            }
            else
            {
                MovingDown = true;
                input.Pressed[SynchKey.Down] = true;
                input.State[SynchKey.Down] = true;
            }

            return input;
        }
        #endregion
        #region Dash
        private void Dash(PlayerInput input, Direction dash)
        {
            GameSystem.Log.Write(dash.ToString());
            if ((dash & Direction.Up) == Direction.Up)
                DashUp(input);
            if ((dash & Direction.Down) == Direction.Down)
                DashDown(input);
            if ((dash & Direction.Left) == Direction.Left)
                DashLeft(input);
            if ((dash & Direction.Right) == Direction.Right)
                DashRight(input);
        }
        private PlayerInput DashLeft(PlayerInput input)
        {
            input.DoublePressed[SynchKey.Left] = true;
            return input;
        }
        private PlayerInput DashRight(PlayerInput input)
        {
            input.DoublePressed[SynchKey.Right] = true;
            return input;
        }
        private PlayerInput DashUp(PlayerInput input)
        {
            input.DoublePressed[SynchKey.Up] = true;
            return input;
        }
        private PlayerInput DashDown(PlayerInput input)
        {
            input.DoublePressed[SynchKey.Down] = true;
            return input;
        }
        #endregion
        #region Move Helper
        private Direction MoveToDirection(FixedVector2 direction)
        {
            if (direction.Length() == (FixedPoint)0)
                return Direction.Stop;
            direction.Normalize();

            Direction move = Direction.Stop;
            if (FixedPoint.Abs(direction.X) > FixedPoint.Abs(direction.Y))
            {
                if (direction.X > (FixedPoint)0)
                {
                    move = Direction.Right;
                }
                else if (direction.X < (FixedPoint)0)
                {
                    move = Direction.Left;
                }
            }
            else
            {
                if (direction.Y > (FixedPoint)0)
                {
                    move = Direction.Up;
                }
                else if (direction.Y < (FixedPoint)0)
                {
                    move = Direction.Down;
                }
            }

            FixedVector2 nowdirection = fighter.Velocity;
            if (nowdirection.Length() == 0)
                return move;
            nowdirection.Normalize();

            FixedPoint w3 = FixedVector2.Wedge3(nowdirection, direction);
            if (move == Direction.Up)
            {
                if (w3 > (FixedPoint)0)
                    move |= Direction.Left;
                else
                    move |= Direction.Right;
            }
            else if (move == Direction.Down)
            {
                if (w3 > (FixedPoint)0)
                    move |= Direction.Right;
                else
                    move |= Direction.Left;
            }
            else if (move == Direction.Left)
            {
                if (w3 > (FixedPoint)0)
                    move |= Direction.Down;
                else
                    move |= Direction.Up;
            }
            else if (move == Direction.Right)
            {
                if (w3 > (FixedPoint)0)
                    move |= Direction.Up;
                else
                    move |= Direction.Down;
            }
            return move;
        }
        private Direction DashToDirection(FixedVector2 direction)
        {
            FixedPoint atan2 = FixedPoint.Atan2(direction.Y, direction.X);
            if (atan2 < -FixedPoint.Pi * 7 / 8)
                return Direction.Left;
            else if (atan2 < -FixedPoint.Pi * 5 / 8)
                return Direction.Left | Direction.Down;
            else if (atan2 < -FixedPoint.Pi * 3 / 8)
                return Direction.Down;
            else if (atan2 < -FixedPoint.Pi / 8)
                return Direction.Down | Direction.Right;
            else if (atan2 < FixedPoint.Pi / 8)
                return Direction.Right;
            else if (atan2 < FixedPoint.Pi * 3 / 8)
                return Direction.Right | Direction.Up;
            else if (atan2 < FixedPoint.Pi * 5 / 8)
                return Direction.Up;
            else if (atan2 < FixedPoint.Pi * 7 / 8)
                return Direction.Up | Direction.Left;
            else
                return Direction.Left;
        }
        private Direction MoveToPosition(FixedVector2 position)
        {
            return MoveToDirection(position - fighter.collider.Position.Simulation);
        }
        private Direction DashToPosition(FixedVector2 position)
        {
            return DashToDirection(position - fighter.collider.Position.Simulation);
        }
        #endregion
        #region Weapon
        private FixedPoint ChargeTimer = 0;
        private bool Charged 
        { 
            get
            {
                return WeaponEnabled && ChargeTimer >= fighter.desc.Attack.MissileCooldown;
            }
        }
        private void FireMainWeapon(PlayerInput input, FixedVector2 dst)
        {
            if (Charged == true)
            {
                input.State[SynchKey.MouseRight] = false;
                input.Released[SynchKey.MouseRight] = true;
                input.Cursor = dst;
            }
        }
        private void ChargeWeapon(PlayerInput input)
        {
            input.State[SynchKey.MouseRight] = true;
        }
        private void FireSubWeapon(PlayerInput input, FixedVector2 dst)
        {
            input.State[SynchKey.MouseLeft] = true;
            input.Released[SynchKey.MouseLeft] = true;
            input.Cursor = dst;
        }
        #endregion

        private bool WeaponEnabled = false;
        public PlayerInput GetInput(FixedPoint deltaTimeInSec)
        {
            PlayerInput input = new PlayerInput();

            Collider dst = null;
            foreach (var c in context.ColliderManager.ParseCollider(this.fighter.collider, 1000, typeof(Fighter)))
            {
                dst = c;
                break;
            }

            if (dst != null)
                Move(input, MoveToPosition(dst.Position.Simulation));
            
            // Attack Logic
            if (WeaponEnabled == true)
                if (dst != null)
                    FireSubWeapon(input, dst.Position.Simulation);

            if (ChargeTimer < fighter.desc.Attack.MissileCooldown)
            {
                ChargeWeapon(input);
                ChargeTimer += deltaTimeInSec;
            }
            else
            {
                if (dst != null)
                {
                    if ((this.fighter.collider.Position.Simulation - dst.Position.Simulation).Length() > 250)
                        Dash(input, DashToPosition(dst.Position.Simulation));
                    
                    ChargeTimer = 0;
                    if (WeaponEnabled == true)
                    {
                        FireMainWeapon(input, dst.Position.Simulation);
                        input.Cursor = dst.Position.Simulation;
                    }
                }
                ChargeWeapon(input);
            }

            return input;
        }
    }
}
