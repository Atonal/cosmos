﻿using HAJE.Cosmos.Combat.Physics;
using HAJE.Cosmos.Combat.Projectile;
using System.Collections.Generic;

namespace HAJE.Cosmos.Combat.Fighter.AttackHandler
{
    public class HomingMissileHandler : AttackHandlerBase
    {
        public HomingMissileHandler(CombatContext context, Fighter owner, AttackDescriptor attackDesc)
            : base(context, owner, attackDesc)
        {

        }

        public override void Attack(
            int attackIndex,
            FixedVector2 target,
            FixedPoint timeRemainderInSec)
        {
            List<Collider> colliders = context.ColliderManager.ParseCollider(owner.collider, attackDesc.Missiles[0].Range);
            Collider targetfighter = null;

            foreach (var col in colliders)
            {
                GameSystem.Log.Write(col.GetType().ToString());
                if (col.owner.GetType() == typeof(Fighter))
                    if (col.owner != this.owner)
                        if (targetfighter == null)
                            targetfighter = col;
                        else
                            if ((this.owner.LogicPosition - targetfighter.Position.Simulation).Length() > (this.owner.LogicPosition - col.Position.Simulation).Length())
                                targetfighter = col;
            }

            if (targetfighter != null)
            {
                FixedPoint rad = FixedPoint.Pi / (FixedPoint)4;
                List<HomingMissile> missileList = new List<HomingMissile>();

                for (int i = -1; i <= 1; i++)
                {
                    HomingMissile hm = context.ProjectileManager.GetProjectile(typeof(HomingMissile)) as HomingMissile;
                    missileList.Add(hm);

                    hm.Shoot(owner, (target - owner.LogicPosition).Rotate(rad * (FixedPoint)i) + owner.LogicPosition,
                        targetfighter, 
                        attackDesc.Missiles[0], 
                        timeRemainderInSec,
                        missileList);
                }
            }
            else
                GameSystem.Log.Write("Homing Missile: NO TARGET");
        }
    }
}
