﻿using HAJE.Cosmos.Combat.Projectile;

namespace HAJE.Cosmos.Combat.Fighter.AttackHandler
{
    public class DualBulletHandler : AttackHandlerBase
    {
        public DualBulletHandler(CombatContext context, Fighter owner, AttackDescriptor attackDesc)
            : base(context, owner, attackDesc)
        {

        }

        public override void Attack(
            int attackIndex,
            FixedVector2 target,
            FixedPoint timeRemainderInSec)
        {
            FixedVector2 orthvec = target - owner.LogicPosition;
            orthvec /= orthvec.Length() / (FixedPoint)100;
            orthvec.Rotate(FixedPoint.Pi / (FixedPoint)2);

            (context.ProjectileManager.GetProjectile(typeof(Bullet)) as Bullet).Shoot(
                owner,
                target,
                attackDesc.Bullets[0],
                timeRemainderInSec,
                1);
            (context.ProjectileManager.GetProjectile(typeof(Bullet)) as Bullet).Shoot(
                owner,
                target,
                attackDesc.Bullets[0],
                timeRemainderInSec,
                -1);
        }
    }
}
