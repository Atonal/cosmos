﻿using HAJE.Cosmos.Combat.Projectile;

namespace HAJE.Cosmos.Combat.Fighter.AttackHandler
{
    public class DefaultBulletHandler : AttackHandlerBase
    {
        public DefaultBulletHandler(CombatContext context, Fighter owner, AttackDescriptor attackDesc)
            : base(context, owner, attackDesc)
        {

        }

        public override void Attack(
            int attackIndex,
            FixedVector2 target,
            FixedPoint timeRemainderInSec)
        {
            (context.ProjectileManager.GetProjectile(typeof(Bullet)) as Bullet).Shoot(
                owner,
                target,
                attackDesc.Bullets[0],
                timeRemainderInSec);
        }
    }
}
