﻿using HAJE.Cosmos.Combat.Projectile;

namespace HAJE.Cosmos.Combat.Fighter.AttackHandler
{
    public class DefaultMissileHandler : AttackHandlerBase
    {
        public DefaultMissileHandler(CombatContext context, Fighter owner, AttackDescriptor attackDesc)
            : base(context, owner, attackDesc)
        {

        }

        public override void Attack(
            int attackIndex,
            FixedVector2 target,
            FixedPoint timeRemainderInSec)
        {
            (context.ProjectileManager.GetProjectile(typeof(Missile)) as Missile).Shoot(
                owner,
                target,
                attackDesc.Missiles[0],
                timeRemainderInSec);
        }
    }
}
