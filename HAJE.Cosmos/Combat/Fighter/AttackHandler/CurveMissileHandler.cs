﻿using HAJE.Cosmos.Combat.Projectile;

namespace HAJE.Cosmos.Combat.Fighter.AttackHandler
{
    public class CurveMissileHandler : AttackHandlerBase
    {
        public CurveMissileHandler(CombatContext context, Fighter owner, AttackDescriptor attackDesc)
            : base(context, owner, attackDesc)
        {

        }

        public override void Attack(
            int attackIndex,
            FixedVector2 target,
            FixedPoint timeRemainderInSec)
        {
            (context.ProjectileManager.GetProjectile(typeof(CurveMissile)) as CurveMissile).Shoot(
                owner,
                target,
                attackDesc.Missiles[0],
                timeRemainderInSec,
                1);
            (context.ProjectileManager.GetProjectile(typeof(CurveMissile)) as CurveMissile).Shoot(
                owner,
                target,
                attackDesc.Missiles[0],
                timeRemainderInSec,
                -1);
        }
    }
}
