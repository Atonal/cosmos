﻿using HAJE.Cosmos.Combat.Projectile;

namespace HAJE.Cosmos.Combat.Fighter.AttackHandler
{
    public class ScatterBulletHandler : AttackHandlerBase
    {
        public ScatterBulletHandler(CombatContext context, Fighter owner, AttackDescriptor attackDesc)
            : base(context, owner, attackDesc)
        {

        }

        public override void Attack(
            int attackIndex,
            FixedVector2 target,
            FixedPoint timeRemainderInSec)
        {
            for (int idx = -10; idx < 11; idx++)
                (context.ProjectileManager.GetProjectile(typeof(Bullet)) as Bullet).Shoot(
                    owner,
                    (target - owner.LogicPosition).Rotate((FixedPoint)0.1f * (FixedPoint)idx) + owner.LogicPosition,
                    attackDesc.Bullets[0],
                    timeRemainderInSec);
        }
    }
}
