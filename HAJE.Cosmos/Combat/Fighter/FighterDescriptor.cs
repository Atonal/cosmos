﻿using System.Diagnostics;

namespace HAJE.Cosmos.Combat.Fighter
{
    public class FighterDescriptor
    {
        public MovementDescriptor Movement;

        public static readonly FixedPoint DefaultFighterRadius = 8;
        public FixedPoint Radius;

        public static readonly FixedPoint DefaultFighterMass = 100;
        public FixedPoint Mass;

        public FixedPoint HP;

        public string Texture;

        public AttackDescriptor Attack;

        [Conditional("DEBUG")]
        public void CheckValid()
        {
            Debug.Assert(Movement != null);
            Movement.CheckValid();

            Debug.Assert(Radius > 0);
            Debug.Assert(Mass > 0);
            Debug.Assert(!string.IsNullOrWhiteSpace(Texture));

            Debug.Assert(HP > 0);
            Debug.Assert(Attack != null);
            Attack.CheckValid();
        }
    }
}
