﻿using HAJE.Cosmos.Combat.Projectile;
using System.Diagnostics;

namespace HAJE.Cosmos.Combat.Fighter
{
    public class AttackDescriptor
    {
        /// <summary>
        /// 매 공격 사이의 간격 (단위: 초)
        /// </summary>
        public FixedPoint Interval;

        /// <summary>
        /// 쿨다운 없이 할 수 있는 최대 보조 공격 횟수
        /// </summary>
        public int BulletAttackCount;

        /// <summary>
        /// 최대 보조 공격 횟수를 마친 뒤의 쿨다운 (단위: 초)
        /// </summary>
        public FixedPoint BulletCooldown;

        /// <summary>
        /// 쿨다운 없이 할 수 있는 최대 주 공격 횟수
        /// </summary>
        public int MissileAttackCount;

        /// <summary>
        /// 미사일 차징에 걸리는 시간 (단위: 초)
        /// </summary>
        public FixedPoint MissileChargeTime;

        /// <summary>
        /// 최대 주 공격 횟수를 마친 뒤의 쿨다운 (단위: 초)
        /// </summary>
        public FixedPoint MissileCooldown;

        /// <summary>
        /// 공격 처리 객체 이름
        /// </summary>
        public HandlerType LeftHandler;
        public HandlerType RightHandler;


        /// <summary>
        /// 사용하는 탄환 목록
        /// </summary>
        public ProjectileDescriptor[] Bullets;
        
        /// <summary>
        /// 사용하는 미사일 목록
        /// </summary>
        public ProjectileDescriptor[] Missiles;

        public enum HandlerType
        {
            DefaultBullet,
            ScatterBullet,

            DefaultMissile,
            ScatterMissile,
            CurveMissile,
            HomingMissile
        }

        [Conditional("DEBUG")]
        public void CheckValid()
        {
            Debug.Assert(Interval > 0);

            Debug.Assert(BulletAttackCount > 0);
            Debug.Assert(BulletCooldown > 0);
            Debug.Assert(Bullets.Length > 0);
            
            Debug.Assert(Missiles.Length > 0);
            Debug.Assert(MissileAttackCount > 0);
            Debug.Assert(MissileCooldown > 0);

            foreach (var b in Bullets)
                b.CheckValid();

            foreach (var m in Missiles)
                m.CheckValid();
        }
    }
}
