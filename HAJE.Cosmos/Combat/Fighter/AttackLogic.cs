﻿
namespace HAJE.Cosmos.Combat.Fighter
{
    public class AttackLogic
    {
        public AttackLogic(
            CombatContext
            context,
            Fighter owner,
            AttackDescriptor desc)
        {
            this.desc = desc;
			this.owner = owner;

			this.leftAttackIndex = 0;
            this.leftAttackCooldown = 0;

			this.rightAttackIndex = 0;
            this.rightAttackCharge = 0;
            this.rightAttackCooldown = 0;

            this.leftAttackInterval = 0;
            this.lhandler = AttackHandlerFactory.Get(
                context,
                owner,
                desc,
                desc.LeftHandler);
            this.rhandler = AttackHandlerFactory.Get(
                context,
                owner,
                desc,
                desc.RightHandler);
        }

        public void Update(
            bool leftButtonPressing,
            bool leftButtonUp,
            bool rightButtonPressing,
            bool rightButtonUp,
            FixedVector2 target,
            FixedPoint deltaTimeInSec)
        {
            // 보조 무기 발사
            if (leftButtonPressing && leftAttackIndex < desc.BulletAttackCount)
            {
                leftAttackCooldown = 0;
                leftReleased = false;
				while (leftAttackInterval >= desc.Interval && leftAttackIndex < desc.BulletAttackCount)
                {
                    leftAttackInterval -= desc.Interval;
					leftAttackIndex++;
					lhandler.Attack(leftAttackIndex, target, leftAttackInterval);
                }
                leftAttackInterval += deltaTimeInSec;
            }

            // 보조 무기 cooldown
			if (!leftButtonPressing || leftAttackIndex >= desc.BulletAttackCount)
            {
                if (leftAttackCooldown < desc.BulletCooldown)
                    leftAttackCooldown += deltaTimeInSec;
                if (leftButtonUp || !leftButtonPressing)
                    leftReleased = true;
                if (!leftButtonPressing && leftAttackInterval < desc.Interval)
                {
                    leftAttackInterval += deltaTimeInSec;
                    if (leftAttackInterval > desc.Interval)
                        leftAttackInterval = desc.Interval;
                }
            }

            // 보조 무기 cooldown 리셋
            if (leftReleased && leftAttackCooldown >= desc.BulletCooldown)
            {
				leftAttackIndex = 0;
                leftAttackCooldown = 0;
                lhandler.CooldownReset();
                leftReleased = false;
            }

            // 주 무기 발사
			if (rightButtonPressing && rightAttackIndex < desc.MissileAttackCount)
            {
                rightAttackCharge += deltaTimeInSec;

                // UI 짜는 사람을 위해 rightAttackCharge는 desc.MissileChargeTime을 넘지 못하도록 한다.
                if (rightAttackCharge > desc.MissileChargeTime)
                {
                    // UI가 없으니 임시로 로그라도 찍어야지.
                   //  GameSystem.Log.Write("Missile Charged");
                    rightAttackCharge = desc.MissileChargeTime;
                }
            }
			if (!rightButtonPressing && rightAttackIndex < desc.MissileAttackCount)
            {
                if (rightAttackCharge >= desc.MissileChargeTime)
                {
                    rightAttackCharge = 0;
                    rightAttackCooldown = 0;
                    rightReleased = false;
                    while (rightAttackInterval >= desc.Interval && rightAttackIndex < desc.MissileAttackCount)
                    {
                        rightAttackInterval -= desc.Interval;
						rightAttackIndex++;
                        rhandler.Attack(rightAttackIndex, target, rightAttackInterval);
                    }
                    rightAttackInterval += deltaTimeInSec;
                }
                else
                {
                    rightAttackCharge = 0;
                }
            }

            // 주 무기 cooldown
			if (!rightButtonPressing || rightAttackIndex >= desc.MissileAttackCount)
            {
                if (rightAttackCooldown < desc.MissileCooldown)
                    rightAttackCooldown += deltaTimeInSec;
                if (rightButtonUp || !rightButtonPressing)
                    rightReleased = true;
                if (!rightButtonPressing && rightAttackInterval < desc.Interval)
                {
                    rightAttackInterval += deltaTimeInSec;
                    if (rightAttackInterval > desc.Interval)
                        rightAttackInterval = desc.Interval;
                }
            }

            // 주 무기 cooldown 리셋
            if (rightReleased && rightAttackCooldown >= desc.BulletCooldown)
            {
				rightAttackIndex = 0;
                rightAttackCharge = 0;
                rightAttackCooldown = 0;
                rhandler.CooldownReset();
                rightReleased = false;
            }
        }

        /// <summary>
        /// 1부터 AttackCount까지 증가
        /// </summary>
        int leftAttackIndex;
        FixedPoint leftAttackCooldown;
        FixedPoint leftAttackInterval;

        int rightAttackIndex;
        FixedPoint rightAttackCharge;
        FixedPoint rightAttackCooldown;
        FixedPoint rightAttackInterval;
        bool leftReleased;
        bool rightReleased;

        AttackHandlerBase lhandler;
        AttackHandlerBase rhandler;
        AttackDescriptor desc;
        Fighter owner;
    }
}
