﻿using HAJE.Cosmos.Combat.Synchronization;
using HAJE.Cosmos.Network;
using HAJE.Cosmos.Rendering;
using HAJE.Cosmos.SystemComponent;
using OpenTK;
using OpenTK.Input;

namespace HAJE.Cosmos.Combat
{
    public class CombatGameLoop : GameLoopBase
    {
        public CombatGameLoop(Session session)
            : base("CombatGameLoop")
        {
            this.session = session;
            context = new CombatContext();
            context.Loop = this;
            context.Session = session;
            context.Schdeulder = new Scheduler("CombatGameLoop");
            context.Camera = new Rendering.Camera(GameSystem.Viewport);
            if (context.Session.IsHost)
                context.Session.ServerDispatcher.Start(context.Schdeulder);
            context.Session.ClientDispatcher.Start(context.Schdeulder);
            context.Random = new System.Random();
            context.GameAreaData = new GameArea.Factory.TestGameArea().Create();

            context.CombatSync = new CombatSync(context);
            context.CombatRenderer = new CombatRenderer(context);
            context.CombatSimulation = new CombatSimulation(context);
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            targetTexture = Texture.CreateFromFile("Resource/CombatUI/Aim.png");
            cursorTexture = TextureManager.GetTexture("Resource/CombatUI/MousePointer.png");
        }

        protected override void OnRun()
        {
            GameSystem.Scheduler.Schedule(context.Schdeulder);
        }

        public override void Update(Second deltaTime)
        {
            GameLoop.GameLoopCommon.TryExitGame();

            var form = GameSystem.MainForm;
            if (form.Focused)
            {
                var keyState = Keyboard.GetState();
                if (keyState.IsKeyDown(Key.Escape))
                {
                    form.CursorVisible = true;
                }
                else
                {
                    // 커서가 화면 영역 안일 경우에만 커서 숨기기 시작
                    var cursor = form.GetMousePosition();
                    if (cursor == Vector2.Clamp(cursor, Vector2.Zero, GameSystem.Viewport))
                        form.CursorVisible = false;
                }
            }

            Second interval = (Second)(float)SimulationTime.SimulationUpdateInterval;

            timeFromLastUpdate += deltaTime;

            var inputReceiver = context.CombatSync.InputReceiver;
            var inputList = context.CombatSync.InputList;
            if (firstTick && inputReceiver.IsNextFrameReady)
            {
                inputReceiver.PopFrame(inputList);
                SimulationUpdate(inputList);
                firstTick = false;
                frameLength = interval;
            }
            else if (timeFromLastUpdate >= frameLength)
            {
                if (inputReceiver.IsNextFrameReady)
                {
                    var remainder = timeFromLastUpdate - frameLength;
                    var fCnt = inputReceiver.ReservedFrameCount - 1;
                    inputReceiver.PopFrame(inputList);
                    SimulationUpdate(inputList);
                    if (fCnt > flushThreshold)
                    {
                        // 입력 큐가 막장으로 쌓인 경우
                        // 전부 비워버려서 입력 지연을 줄인다.
                        while (inputReceiver.IsNextFrameReady)
                        {
                            inputReceiver.PopFrame(inputList);
                            SimulationUpdate(inputList);
                        }
                        flushThreshold++;
                        frameLength = interval;
                    }
                    else if (fCnt > 1)
                    {
                        // 입력 큐가 쌓이고 있다.
                        // 많이 쌓일수록 빨리 게임 시뮬을 돌려서 입력 지연을 낮춘다.
                        frameLength = (Second)(interval - (fCnt * fCnt * interval * 0.02f));
                    }
                    else
                    {
                        // 입력 큐가 안 쌓인 아주 좋은 경우.
                        // 앞으로도 쌓일 일이 없도록 잘 해보자.
                        if (remainder < interval * 0.25f)
                        {
                            // 아주 약간의 지연이 있던 경우, 입력이 밀리지 않게 나머지 모두 보정
                            frameLength = interval - remainder;
                        }
                        else if (remainder < interval)
                        {
                            // 지연이 일정 수준 이상이면 렌더링에서 너무 많은 부분을 외삽한다.
                            // 적당히 시뮬레이션 재생을 늦춰서 진동을 완화한다.
                            frameLength = interval - (remainder * 0.25f);
                        }
                        else
                        {
                            // 뭐야 이거 무서워. 서버 렉인가?
                            // 일단 제일 안전한 값 사용
                            frameLength = interval;
                        }
                    }
                }
            }
            GameSystem.DebugOutput.SetWatch("Input Queued", inputReceiver.ReservedFrameCount);
        }   

        // 테스트용 임시 코드
        Texture targetTexture;
        Texture cursorTexture;
        Vector2 recentTarget;
        void SimulationUpdate(PlayerInput[] inputList)
        {
            var world2 = (Vector2)inputList[0].Cursor;
            var world3 = new Vector3(world2.X, world2.Y, ZOrder.GameDepth);
            recentTarget = context.Camera.WorldToScreen(world3);

            context.CombatSimulation.Update(inputList, SimulationTime.SimulationUpdateInterval);
            context.CombatRenderer.Update(SimulationTime.SimulationUpdateInterval);
            context.CombatRenderer.NextFrame();

            timeFromLastUpdate = (Second)0;
        }

        public override void RenderFrame(Second deltaTime)
        {
            context.CombatRenderer.Render(deltaTime, timeFromLastUpdate / frameLength);
            GLHelper.DrawTexture(targetTexture, recentTarget - targetTexture.Size / 2);

            Vector2 mousePos = GameSystem.MainForm.GetMousePosition();
            Vector2 mouseTargetPos = mousePos - new Vector2(0, targetTexture.Size.Y);
            GLHelper.DrawTexture(cursorTexture, mouseTargetPos);
        }

        protected override void OnStop()
        {
            if (context.Session.IsHost)
                context.Session.ServerDispatcher.End();
            context.Session.ClientDispatcher.End();
            GameSystem.Scheduler.Unschedule(context.Schdeulder);
        }

        bool firstTick = true;
        int flushThreshold = 3; // 이 값이 넘도록 입력 프레임이 쌓이면 전부 실행해버린다.
        Second timeFromLastUpdate = (Second)0;
        Second frameLength = (Second)(float)SimulationTime.SimulationUpdateInterval;
        CombatContext context;
        Session session;
    }
}
