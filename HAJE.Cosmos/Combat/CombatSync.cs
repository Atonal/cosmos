﻿using HAJE.Cosmos.Combat.Synchronization;

namespace HAJE.Cosmos.Combat
{
    public class CombatSync
    {
        public CombatSync(CombatContext context)
        {
            this.context = context;
            inputSender = new InputSender();
            inputSender.Start(context);
            if (context.Session.IsHost)
            {
                inputDispatcher = new InputDispatcher();
                inputDispatcher.Start(context);
            }
            InputReceiver = new InputReceiver();
            InputReceiver.Start(context);
            InputList = new PlayerInput[context.Session.MaxPlayer];
            for (int i = 0; i < context.Session.MaxPlayer; i++)
                InputList[i] = new PlayerInput();
        }

        InputSender inputSender;
        InputDispatcher inputDispatcher;
        public InputReceiver InputReceiver;
        public PlayerInput[] InputList;
        CombatContext context;
    }
}
