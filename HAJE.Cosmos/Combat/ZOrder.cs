﻿
namespace HAJE.Cosmos.Combat
{
    public static class ZOrder
    {
        public const float GameDepth = -500.0f;
        public const float ScaleAtGameDepth = 0.4f;

        public static readonly FloatRange DeepSkyDepth = new FloatRange(-11000, -9000);
        public const float MinStarDepth = -7500.0f;
        public static readonly FloatRange BackDepth = new FloatRange(-5000, -1000);
        public static readonly FloatRange MiddleDepth = new FloatRange(-950, -750);
        public static readonly FloatRange FrontDepth = new FloatRange(-700, -550);
    }
}
