﻿using HAJE.Cosmos.Combat.Projectile;
using HAJE.Cosmos.Combat.Fighter;

namespace HAJE.Cosmos.Combat.Data.Fighter
{
    public static class Default
    {
        public static FighterDescriptor Create()
        {
            var ret = new FighterDescriptor();
            ret.Movement = new MovementDescriptor()
            {
                MovementSpeed = 400,
                DashSpeed = 700,
                Acceleration = 150,
                AdditionalRotationAcceleration = 200,
                DefaultDeceleration = 100,
                BreakDeceleration = 2000,
            };
            ret.Radius = FighterDescriptor.DefaultFighterRadius;
            ret.Mass = FighterDescriptor.DefaultFighterMass;
            ret.HP = 10000;
            ret.Texture = "Resource/Fighter/T-100B.png";
            ret.Attack = new AttackDescriptor()
            {
                Interval = (FixedPoint)0.05f,
                LeftHandler = AttackDescriptor.HandlerType.ScatterBullet,
                RightHandler = AttackDescriptor.HandlerType.ScatterMissile,
                BulletAttackCount = 15,
                BulletCooldown = (FixedPoint)1.0f,
                Bullets = new ProjectileDescriptor[1]
                {
                    new ProjectileDescriptor()
                    {
                        Impulse = 200,
                        Radius = (FixedPoint)1.25f,
                        Range = 200,
                        Speed = 500
                    }
                },
                MissileAttackCount = 1,
                MissileChargeTime = (FixedPoint)0.5f,
                MissileCooldown = (FixedPoint)1.5f,
                Missiles = new ProjectileDescriptor[1] 
                {
                    new ProjectileDescriptor()
                    {
                        Impulse = 180,
                        Radius = 4,
                        ExplosionRange = new FixedPoint[3]{5, 10, 15},
                        Range = 200,
                        Speed = 220
                    }
                }
            };
            return ret;
        }
    }
}
