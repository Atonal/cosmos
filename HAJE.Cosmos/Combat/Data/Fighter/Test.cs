﻿using HAJE.Cosmos.Combat.Bullet;
using HAJE.Cosmos.Combat.Fighter;

namespace HAJE.Cosmos.Combat.Data.Fighter
{
    public static class Test
    {
        public static FighterDescriptor Create()
        {
            var ret = new FighterDescriptor();
            ret.Movement = new MovementDescriptor()
            {
                MovementSpeed = (FixedPoint)600,
                DashSpeed = (FixedPoint)700,
                Acceleration = (FixedPoint)150,
                AdditionalRotationAcceleration = (FixedPoint)200,
                DefaultDeceleration = (FixedPoint)100,
                BreakDeceleration = (FixedPoint)2000,
            };
            ret.Radius = FighterDescriptor.DefaultFighterRadius;
            ret.Mass = FighterDescriptor.DefaultFighterMass;
            ret.Texture = "Resource/Fighter/T-100B.png";
            ret.Attack = new AttackDescriptor()
            {
                Interval = (FixedPoint)0.05f,
                AttackCount = 5,
                Cooldown = (FixedPoint)1.0f,
                Handler = "Default",
                Bullets = new BulletDescriptor[1]
                {
                    new BulletDescriptor()
                    {
                        Impulse = (FixedPoint)200,
                        Radius = (FixedPoint)1.25f,
                        Range = (FixedPoint)200,
                        Speed = (FixedPoint)500,
                    }
                }
            };
            return ret;
        }
    }
}
