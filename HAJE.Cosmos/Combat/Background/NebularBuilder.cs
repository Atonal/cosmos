﻿using OpenTK;
using System;
using System.Collections.Generic;

namespace HAJE.Cosmos.Combat.Background
{
    public class NebularBuilder
    {
        public Vector2 MinSize = new Vector2(1000, 1000);
        public Vector2 MaxSize = new Vector2(5000, 5000);

        public void ClearColorPalette()
        {
            colorPalette.Clear();
        }

        public void AddColorPalette(ColorPalette palette, int weight)
        {
            colorPalette.Add(palette, weight);
        }

        public void AppendUniform(Vector3 point1, Vector3 point2, int count, int seed)
        {
            Vector3 max = Vector3.Max(point1, point2);
            Vector3 min = Vector3.Min(point1, point2);
            var rand = new Random(seed);
            for (int i = 0; i < count; i++)
            {
                Append(new Vector3()
                {
                    X = rand.NextFloat(min.X, max.X),
                    Y = rand.NextFloat(min.Y, max.Y),
                    Z = rand.NextFloat(min.Z, max.Z)
                }, rand);
            }
        }

        public void Append(Vector3 position, int seed)
        {
            Append(position, new Random(seed));
        }

        public NebularData[] FlushData()
        {
            var ret = innerList.ToArray();
            innerList.Clear();
            return ret;
        }

        #region privates

        void Append(Vector3 position, Random rand)
        {
            innerList.Add(new NebularData()
            {
                Color = colorPalette.GetRandom(rand).GetColor4(rand),
                NoiseIndex = new NebularNoiseIndex(rand),
                Position = position,
                Rotation = (Radian)rand.NextFloat(Radian.Pi),
                Size = new Vector2(
                    rand.NextFloat(MinSize.X, MaxSize.X),
                    rand.NextFloat(MinSize.Y, MaxSize.Y)
                )
            }
            );
        }

        List<NebularData> innerList = new List<NebularData>();
        Mathmatics.WeightedRandomPool<ColorPalette> colorPalette = new Mathmatics.WeightedRandomPool<ColorPalette>();

        #endregion
    }
}
