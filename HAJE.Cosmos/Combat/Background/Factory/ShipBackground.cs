﻿using OpenTK;


namespace HAJE.Cosmos.Combat.Background.Factory
{
    public class ShipBackground : IBackgroundFactory
    {
        public BackgroundData Create()
        {
            BackgroundData ret;
            SimpleStarBuilder starBuilder = new SimpleStarBuilder();

            #region dot star

            starBuilder.AddColorPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.125f, 0.45f),
                ColorFrom = StarColor.White,
                ColorTo = StarColor.White,
                ToBlend = new FloatRange(0, 1),
            }, 1);
            starBuilder.AppendUniformRectangular(
                new Vector3(-20000, -20000, -9000),
                new Vector3(20000, 20000, -11000), 0.15f, 0);
            ret.DotStars = starBuilder.FlushData();

            #endregion

            #region quad star

            starBuilder.AddColorPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.125f, 1.0f),
                ColorFrom = StarColor.Red,
                ColorTo = StarColor.Orange,
                ToBlend = new FloatRange(0, 1),
            }, 1);
            starBuilder.AppendUniformRectangular(
                new Vector3(-20000, -20000, -9000),
                new Vector3(20000, 20000, -11000), 0.05f, 1);
            ret.QuadStars = starBuilder.FlushData();

            #endregion

            #region nebula

            NebularBuilder nebularBuilder = new NebularBuilder();
            nebularBuilder.AddColorPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.5f, 0.75f),
                ColorFrom = StarColor.Red,
                ColorTo = StarColor.Orange,
                ToBlend = new FloatRange(0, 1),
            }, 1);
            nebularBuilder.AddColorPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.5f, 0.75f),
                ColorFrom = StarColor.Red,
                ColorTo = StarColor.Blue,
                ToBlend = new FloatRange(0, 0.88f),
            }, 1);
            nebularBuilder.AppendUniform(
                new Vector3(-20000, -20000, -9000),
                new Vector3(20000, 20000, -11000),
                150, 0
            );
            ret.Nebulas = nebularBuilder.FlushData();

            #endregion

            #region sprite star

            var spriteStarBuilder = new SpriteStarBuilder();
            spriteStarBuilder.AddAnimationGroupPalette(0);
            spriteStarBuilder.AddAnimationGroupPalette(1);
            spriteStarBuilder.AddAnimationGroupPalette(2);
            spriteStarBuilder.AddColorPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.5f, 1),
                ColorFrom = StarColor.Red,
                ColorTo = StarColor.Orange,
                ToBlend = new FloatRange(0, 1),
            }, 1);
            spriteStarBuilder.SetScaleRange(200.0f, 400.0f);
            spriteStarBuilder.SetFlareScaleVariation(0.33f, 1.15f);
            spriteStarBuilder.AppendUniform(
                new Vector3(-20000, -20000, -9000),
                new Vector3(20000, 20000, -11000),
                250, 0
            );
            spriteStarBuilder.SetScaleRange(300.0f, 500.0f);
            spriteStarBuilder.SetFlareScaleVariation(0.75f, 1.15f);
            spriteStarBuilder.AppendUniform(
                new Vector3(-20000, -20000, -9000),
                new Vector3(20000, 20000, -11000),
                100, 1
            );
            spriteStarBuilder.SetScaleRange(150.0f, 300.0f);
            spriteStarBuilder.AppendCircular(
                new Vector3(1000.0f, 250.0f, -10000.0f),
                new Vector3(2000.0f, 300.0f, 3000.0f),
                new Vector3(0.4f, 0.2f, 0.0f),
                20, 0
            );
            spriteStarBuilder.SetScaleRange(300.0f, 800.0f);
            spriteStarBuilder.AppendCircular(
                new Vector3(1000.0f, 250.0f, -10000.0f),
                new Vector3(2000.0f, 300.0f, 3000.0f),
                new Vector3(0.4f, 0.2f, 0.0f),
                5, 1
            );
            spriteStarBuilder.Append(
                new Vector3(1250.0f, 128.0f, -3000.0f),
                24.0f, 36.0f, 3, 0
            );
            ret.SpriteStars = spriteStarBuilder.FlushData();

            #endregion

            #region doodads

            DoodadBuilder doodadBuilder = new DoodadBuilder();
            doodadBuilder.AddDoodad(new DoodadData()
            {
                Position = new Vector3(-1000, 500, -2500),
                Scale = 1.4f,
                Texture = "Resource/Background/Jupiter.png",
                Layer = DoodadLayer.Back
            });
            doodadBuilder.AddDoodad(new DoodadData()
            {
                Position = new Vector3(800, 100, -1250),
                Scale = 0.8f,
                Texture = "Resource/Background/Mars.png",
                Layer = DoodadLayer.Back
            });
            doodadBuilder.AddDoodad(new DoodadData()
            {
                Position = new Vector3(-250, 0, -4000),
                Scale = 0.45f,
                Texture = "Resource/Background/ship1.png",
                Layer = DoodadLayer.Back
            });
            doodadBuilder.AddDoodad(new DoodadData()
            {
                Position = new Vector3(0, 0, -3100),
                Scale = 0.4f,
                Texture = "Resource/Background/ship2.png",
                Layer = DoodadLayer.Back
            });
            doodadBuilder.AddDoodad(new DoodadData()
            {
                Position = new Vector3(-200, -50, -3700),
                Scale = 0.4f,
                Texture = "Resource/Background/ship3.png",
                Layer = DoodadLayer.Back
            });
            doodadBuilder.AddDoodad(new DoodadData()
            {
                Position = new Vector3(-300, -500, -3400),
                Scale = 0.45f,
                Texture = "Resource/Background/ship4.png",
                Layer = DoodadLayer.Back
            });
            doodadBuilder.AddDoodad(new DoodadData()
            {
                Position = new Vector3(800, 100, -1250),
                Scale = 0.8f,
                Texture = "Resource/Background/Mars.png",
                Layer = DoodadLayer.Back
            });
            doodadBuilder.AddDoodad(new DoodadData()
            {
                Position = new Vector3(0, -300, -2500),
                Scale = 0.3f,
                Texture = "Resource/Background/Wreckage1.png",
                Layer = DoodadLayer.Back
            });
            doodadBuilder.AddDoodad(new DoodadData()
            {
                Position = new Vector3(-330, -70, -2750),
                Scale = 0.3f,
                Texture = "Resource/Background/Wreckage5.png",
                Layer = DoodadLayer.Back
            });
            doodadBuilder.AddDoodad(new DoodadData()
            {
                Position = new Vector3(-400, -390, -3000),
                Scale = 0.45f,
                Texture = "Resource/Background/Wreckage6.png",
                Layer = DoodadLayer.Back
            });
            doodadBuilder.AddDoodad(new DoodadData()
            {
                Position = new Vector3(-250, -150, -3250),
                Scale = 0.45f,
                Texture = "Resource/Background/Wreckage8.png",
                Layer = DoodadLayer.Back
            });
            doodadBuilder.AddDoodad(new DoodadData()
            {
                Position = new Vector3(50, -200, -2750),
                Scale = 0.3f,
                Texture = "Resource/Background/Wreckage5.png",
                Layer = DoodadLayer.Back
            });
            doodadBuilder.AddDoodad(new DoodadData()
            {
                Position = new Vector3(130, -620, -3000),
                Scale = 0.3f,
                Texture = "Resource/Background/Wreckage6.png",
                Layer = DoodadLayer.Back
            });
            doodadBuilder.SetTextureSet(
                "Resource/Background/Asteroid0.png",
                "Resource/Background/Asteroid1.png",
                "Resource/Background/Asteroid2.png",
                "Resource/Background/Asteroid3.png"
            );
            doodadBuilder.SetScaleRange(0.25f, 0.5f);
            doodadBuilder.SetRandomRotation(true);
            ret.Doodads = doodadBuilder.FlushData();

            #endregion

            return ret;
        }
    }
}