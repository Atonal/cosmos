﻿using OpenTK;

namespace HAJE.Cosmos.Combat.Background.Factory
{
    public class UrsaMajorBackground : IBackgroundFactory
    {
        public BackgroundData Create()
        {
            BackgroundData ret;
            SimpleStarBuilder starBuilder = new SimpleStarBuilder();

            #region dot star 

            starBuilder.AddColorPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.125f, 0.45f),
                ColorFrom = StarColor.White,
                ColorTo = StarColor.White,
                ToBlend = new FloatRange(0, 1),
            }, 1);
            starBuilder.AppendUniformRectangular(
                new Vector3(-20000, -20000, -9000),
                new Vector3(20000, 20000, -11000), 0.15f, 0);
            ret.DotStars = starBuilder.FlushData();

            #endregion

            #region quad star

            starBuilder.AddColorPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.125f, 1.0f),
                ColorFrom = StarColor.Blue,
                ColorTo = StarColor.BrightBlue,
                ToBlend = new FloatRange(0, 1),
            }, 1);
            starBuilder.AppendUniformRectangular(
                new Vector3(-20000, -20000, -9000),
                new Vector3(20000, 20000, -11000), 0.05f, 1);
            ret.QuadStars = starBuilder.FlushData();

            #endregion

            #region nebula

            NebularBuilder nebularBuilder = new NebularBuilder();
            nebularBuilder.AddColorPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.25f, 0.5f),
                ColorFrom = StarColor.BrightOrange,
                ColorTo = StarColor.White,
                ToBlend = new FloatRange(0.5f, 1),
            }, 1);
            nebularBuilder.AddColorPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.25f, 0.5f),
                ColorFrom = StarColor.BrightBlue,
                ColorTo = StarColor.SkyBlue,
                ToBlend = new FloatRange(0, 1.0f),
            }, 1);
            nebularBuilder.AppendUniform(
                new Vector3(-20000, -20000, -9000),
                new Vector3(20000, 20000, -11000),
                150, 0
            );
            ret.Nebulas = nebularBuilder.FlushData();

            #endregion

            #region sprite star

            var spriteStarBuilder = new SpriteStarBuilder();
            spriteStarBuilder.AddAnimationGroupPalette(0);
            spriteStarBuilder.AddAnimationGroupPalette(1);
            spriteStarBuilder.AddAnimationGroupPalette(2);
            spriteStarBuilder.AddColorPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.5f, 1),
                ColorFrom = StarColor.Blue,
                ColorTo = StarColor.White,
                ToBlend = new FloatRange(0, 1),
            }, 1);
            spriteStarBuilder.AddColorPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.5f, 1),
                ColorFrom = StarColor.SkyBlue,
                ColorTo = StarColor.Blue,
                ToBlend = new FloatRange(0, 1),
            }, 1);
            spriteStarBuilder.AddColorPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.5f, 1),
                ColorFrom = StarColor.BrightBlue,
                ColorTo = StarColor.Blue,
                ToBlend = new FloatRange(0, 1),
            }, 1);
            spriteStarBuilder.SetScaleRange(200.0f, 400.0f);
            spriteStarBuilder.SetFlareScaleVariation(0.33f, 1.15f);
            spriteStarBuilder.AppendUniform(
                new Vector3(-20000, -20000, -9000),
                new Vector3(20000, 20000, -11000),
                250, 0
            );
            spriteStarBuilder.ClearColorPalette();
            spriteStarBuilder.AddColorPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.25f, 0.5f),
                ColorFrom = StarColor.Orange,
                ColorTo = StarColor.BrightOrange,
                ToBlend = new FloatRange(0, 1),
            }, 2);
            spriteStarBuilder.AddColorPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.25f, 0.5f),
                ColorFrom = StarColor.BrightOrange,
                ColorTo = StarColor.White,
                ToBlend = new FloatRange(0, 1),
            }, 1);
            spriteStarBuilder.AddColorPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.25f, 0.5f),
                ColorFrom = StarColor.White,
                ColorTo = StarColor.BrightBlue,
                ToBlend = new FloatRange(0, 1),
            }, 1);
            spriteStarBuilder.SetScaleRange(450, 500);
            spriteStarBuilder.AppendUniform(
                new Vector3(-10000, -10000, -9000),
                new Vector3(10000, 10000, -11000),
                50, 0
            );
            ///////////////////////////////////////////////////////////////////
            // alpha
            spriteStarBuilder.ClearColorPalette();
            spriteStarBuilder.AddColorPalette(new ColorPalette()
            {
                Brightness = new FloatRange(1, 1),
                ColorFrom = StarColor.Orange,
                ColorTo = StarColor.BrightOrange,
                ToBlend = new FloatRange(0.5f, 1),
            }, 1);
            spriteStarBuilder.SetScaleRange(850, 850);
            spriteStarBuilder.Append(new Vector3(5100, 450, -8500), 1);
            // beta
            spriteStarBuilder.ClearColorPalette();
            spriteStarBuilder.AddColorPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.95f, 0.95f),
                ColorFrom = StarColor.BrightOrange,
                ColorTo = StarColor.White,
                ToBlend = new FloatRange(0.5f, 1),
            }, 1);
            spriteStarBuilder.SetScaleRange(700, 700);
            spriteStarBuilder.Append(new Vector3(4750, -1250, -8700), 2);
            // gamma
            spriteStarBuilder.ClearColorPalette();
            spriteStarBuilder.AddColorPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.8f, 0.8f),
                ColorFrom = StarColor.Orange,
                ColorTo = StarColor.BrightOrange,
                ToBlend = new FloatRange(0.5f, 1),
            }, 1);
            spriteStarBuilder.SetScaleRange(650, 650);
            spriteStarBuilder.Append(new Vector3(2250, -950, -8200), 3);
            // delta
            spriteStarBuilder.ClearColorPalette();
            spriteStarBuilder.AddColorPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.8f, 0.8f),
                ColorFrom = StarColor.Orange,
                ColorTo = StarColor.BrightOrange,
                ToBlend = new FloatRange(0.5f, 1),
            }, 1);
            spriteStarBuilder.SetScaleRange(550, 550);
            spriteStarBuilder.Append(new Vector3(1550, 450, -9000), 1);
            // etc
            spriteStarBuilder.ClearColorPalette();
            spriteStarBuilder.AddColorPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.8f, 0.8f),
                ColorFrom = StarColor.Orange,
                ColorTo = StarColor.BrightOrange,
                ToBlend = new FloatRange(0, 1),
            }, 2);
            spriteStarBuilder.AddColorPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.8f, 0.8f),
                ColorFrom = StarColor.BrightOrange,
                ColorTo = StarColor.White,
                ToBlend = new FloatRange(0, 1),
            }, 1);
            spriteStarBuilder.AddColorPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.8f, 0.8f),
                ColorFrom = StarColor.White,
                ColorTo = StarColor.BrightBlue,
                ToBlend = new FloatRange(0, 1),
            }, 1);
            spriteStarBuilder.SetScaleRange(500, 550);
            spriteStarBuilder.Append(new Vector3(-550, 1750, -9000), 2);
            spriteStarBuilder.Append(new Vector3(-2950, 2250, -9500), 3);
            spriteStarBuilder.Append(new Vector3(-5550, 2050, -9250), 4);
            ///////////////////////////////////////////////////////////////////
            ret.SpriteStars = spriteStarBuilder.FlushData();

            #endregion

            #region doodads

            DoodadBuilder doodadBuilder = new DoodadBuilder();
            doodadBuilder.AddDoodad(new DoodadData()
            {
                Position = new Vector3(-400, -250, -1050),
                Scale = 1.25f,
                Texture = "Resource/Background/Mars.png",
                Layer = DoodadLayer.Back
            });
            doodadBuilder.SetTextureSet(
                "Resource/Background/Asteroid0.png",
                "Resource/Background/Asteroid1.png",
                "Resource/Background/Asteroid2.png",
                "Resource/Background/Asteroid3.png"
            );
            doodadBuilder.SetScaleRange(0.33f, 0.5f);
            doodadBuilder.SetRandomRotation(true);
            doodadBuilder.AddUniform(-1250, 1750, -1200, 1200, -1200, -950, DoodadLayer.Middle, 35, 0);
            ret.Doodads = doodadBuilder.FlushData();            

            #endregion

            return ret;
        }
    }
}
