﻿using OpenTK;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.Cosmos.Combat.Background
{
    public class SpriteStarBuilder
    {
        public SpriteStarBuilder()
        {
            SetScaleRange(250.0f, 800.0f);
        }

        #region 팔레트 관련

        public void ClearColorPalette()
        {
            colorPalettes.Clear();
        }

        public void ClearAnimationPalette()
        {
            animationPalette.Clear();
        }

        public void AddColorPalette(ColorPalette palette, int weight)
        {
            colorPalettes.Add(palette, weight);
        }

        public void AddAnimationGroupPalette(int animationGroup)
        {
            Debug.Assert(0 <= animationGroup && animationGroup < SpriteStarRenderer.MaxAnimationGroup);
            animationPalette.Add(animationGroup);
        }

        #region privates

        List<int> animationPalette = new List<int>();
        Mathmatics.WeightedRandomPool<ColorPalette> colorPalettes = new Mathmatics.WeightedRandomPool<ColorPalette>();

        #endregion

        #endregion

        #region 크기 관련

        public void SetSourceScaleRange(float min, float max)
        {
            Debug.Assert(0 < min && min <= max);
            minSourceScale = min;
            maxSourceScale = max;
        }

        public void SetFlareScaleVariation(float min, float max)
        {
            Debug.Assert(0 < min && min <= max);
            minFlareScaleVariation = min;
            maxFlareScaleVariation = max;
        }

        public void SetScaleRange(float min, float max)
        {
            SetSourceScaleRange(min, max);
            SetFlareScaleVariation(1, 1);
        }

        #region privates

        float minSourceScale;
        float maxSourceScale;
        float minFlareScaleVariation;
        float maxFlareScaleVariation;

        #endregion

        #endregion

        #region 빌더 동작

        public void Clear()
        {
            innerList.Clear();
        }

        public void AppendUniform(Vector3 point1, Vector3 point2, int count, int seed)
        {
            Debug.Assert(animationPalette.Count > 0);
            Random rand = new Random(seed);

            Vector3 max = Vector3.Max(point1, point2);
            Vector3 min = Vector3.Min(point1, point2);
            for (int i = 0; i < count; i++)
            {
                Append(new Vector3()
                    {
                        X = rand.NextFloat(min.X, max.X),
                        Y = rand.NextFloat(min.Y, max.Y),
                        Z = rand.NextFloat(min.Z, max.Z)
                    },
                    rand
                );
            }
        }

        public void AppendCircular(Vector3 center, Vector3 radius, Vector3 rotation, int count, int seed)
        {
            Debug.Assert(animationPalette.Count > 0);
            Random rand = new Random(seed);

            Matrix4 rot = MatrixHelper.RotationYawPitchRoll(rotation);

            for (int i = 0; i < count; i++)
            {
                float r = rand.NextFloat(0, 1);
                Radian theta = (Radian)rand.NextFloat(0, Radian.Pi);
                Radian rho = (Radian)rand.NextFloat(0, Radian.Pi * 2);
                Vector4 unrotated = new Vector4(
                    r * radius.X * Radian.Cos(theta) * Radian.Cos(rho),
                    r * radius.Y * Radian.Sin(theta) * Radian.Cos(rho),
                    r * radius.Z * Radian.Sin(rho),
                    0
                );
                Vector4 rotated = Vector4.Transform(unrotated, rot);
                Append(new Vector3()
                    {
                        X = rotated.X + center.X,
                        Y = rotated.Y + center.Y,
                        Z = rotated.Z + center.Z
                    },
                    rand
                );
            }
        }

        public void Append(Vector3 position, float sourceRadius, float flareRadius, int animationGroup, int seed)
        {
            var data = new SpriteStarData()
            {
                Position = position,
                LightSourceRadius = sourceRadius,
                FlareRadius = flareRadius,
                AnimationGroup = animationGroup,
                Color = GetColor(new Random(seed))
            };
            innerList.Add(data);
        }

        public void Append(Vector3 position, int seed)
        {
            Append(position, new Random(seed));
        }

        public SpriteStarData[] FlushData()
        {
            var ret = innerList.ToArray();
            innerList.Clear();
            return ret;
        }

        #region privates

        void Append(Vector3 position, Random rand)
        {
            int aniGroupIndex = rand.Next(animationPalette.Count);
            float sourceRadius = rand.NextFloat(
                minSourceScale, maxSourceScale
            );
            float flareRadiusVariation = rand.NextFloat(
                minFlareScaleVariation, maxFlareScaleVariation
            );
            float flareRadius = flareRadiusVariation * sourceRadius;

            innerList.Add(new SpriteStarData()
                {
                    AnimationGroup = animationPalette[aniGroupIndex],
                    Color = GetColor(rand),
                    FlareRadius = flareRadius,
                    LightSourceRadius = sourceRadius,
                    Position = position
                }
            );
        }

        Color4 GetColor(Random rand)
        {
            return colorPalettes.GetRandom(rand).GetColor4(rand);
        }

        private List<SpriteStarData> innerList = new List<SpriteStarData>();

        #endregion

        #endregion
    }
}
