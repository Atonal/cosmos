﻿using OpenTK;
using OpenTK.Graphics;

namespace HAJE.Cosmos.Combat.Background
{
    public struct SpriteStarData
    {
        public Vector3 Position;
        public Color4 Color;
        public float LightSourceRadius;
        public float FlareRadius;
        public int AnimationGroup;
    }
}
