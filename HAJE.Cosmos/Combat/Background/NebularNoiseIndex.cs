﻿using System;
using System.Drawing;

namespace HAJE.Cosmos.Combat.Background
{
    public struct NebularNoiseIndex
    {
        public const int IndexPerRow = 4;
        public const int IndexPerColumn = 4;
        public const int MaxNebulaNoiseIndex = 16;

        public NebularNoiseIndex(Random rand)
        {
            index = rand.Next(MaxNebulaNoiseIndex);
        }

        public int Index
        {
            get
            {
                return index;
            }
            set
            {
                if (value >= MaxNebulaNoiseIndex || value < 0)
                    throw new ArgumentOutOfRangeException("value", "허용되지 않는 값입니다.");
                index = value;
            }
        }

        public RectangleF TextureUV
        {
            get
            {
                float width = (1.0f / IndexPerRow);
                float height = (1.0f / IndexPerColumn);
                return new RectangleF(
                    (index / IndexPerRow) * width,
                    (index % IndexPerRow) * height,
                    width,
                    height
                );
            }
        }

        private int index;
    }
}
