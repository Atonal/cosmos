﻿using OpenTK;

namespace HAJE.Cosmos.Combat.Background
{
    public struct DoodadData
    {
        public Vector3 Position;
        public float Scale;
        public Radian Rotation;
        public string Texture;
        public DoodadLayer Layer;
    }
}
