﻿using OpenTK;
using OpenTK.Graphics;
using System.Drawing;
using System.Runtime.InteropServices;

namespace HAJE.Cosmos.Combat.Background
{
    public struct SimpleStarData
    {
        public Vector3 Position;
        public Color4 Color;
    }
}
