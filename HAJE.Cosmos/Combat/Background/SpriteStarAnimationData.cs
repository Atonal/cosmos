﻿
namespace HAJE.Cosmos.Combat.Background
{
    public struct SpriteStarAnimationData
    {
        public Radian Rotation;
        public float Scale;
    }
}
