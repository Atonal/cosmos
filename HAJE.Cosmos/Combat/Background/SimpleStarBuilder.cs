﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.Cosmos.Combat.Background
{
    public class SimpleStarBuilder
    {
        public void AddColorPalette(ColorPalette palette, int weight)
        {
            Debug.Assert(weight > 0);
            palettes.Add(palette, weight);
        }

        public void ClearColorPalette()
        {
            palettes.Clear();
        }

        public void AppendUniformRectangular(Vector3 min, Vector3 max, float density, int seed)
        {
            Debug.Assert(!palettes.IsEmpty());
            Vector3 a = min;
            Vector3 b = max;
            min = Vector3.ComponentMin(a, b);
            max = Vector3.ComponentMax(a, b);

            float area = (max.X - min.X) * (max.Y - min.Y);
            int count = (int)(area * density / 10000);
            count = Math.Max(count, 1);

            var rand = new Random(seed);
            
            for (int i = 0; i < count; i++)
            {
                Append(new Vector3(
                    rand.NextFloat(min.X, max.X),
                    rand.NextFloat(min.Y, max.Y),
                    rand.NextFloat(min.Z, max.Z)),
                    rand);
            }
        }

        public void Append(Vector3 position, int seed)
        {
            Append(position, new Random(seed));
        }

        public SimpleStarData[] FlushData()
        {
            var ret = starData.ToArray();
            starData.Clear();
            return ret;
        }

        #region privates

        void Append(Vector3 position, Random rand)
        {
            SimpleStarData s;
            s.Position = position;
            var palette = palettes.GetRandom(rand);
            s.Color = palette.GetColor4(rand);
            starData.Add(s);
        }

        Mathmatics.WeightedRandomPool<ColorPalette> palettes = new Mathmatics.WeightedRandomPool<ColorPalette>();
        List<SimpleStarData> starData = new List<SimpleStarData>();

        #endregion
    }
}
