﻿
namespace HAJE.Cosmos.Combat.Background
{
    public interface IBackgroundFactory
    {
        BackgroundData Create();
    }
}
