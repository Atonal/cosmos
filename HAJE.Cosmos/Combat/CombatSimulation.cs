﻿using HAJE.Cosmos.Combat.Fighter;
using HAJE.Cosmos.Combat.Synchronization;

namespace HAJE.Cosmos.Combat
{
    public class CombatSimulation
    {
        public CombatSimulation(CombatContext context)
        {
            this.context = context;

            context.ProjectileManager = new Projectile.ProjectileManager(context);
            context.ParticleManager = new Rendering.ParticleSystem.ParticleManager(context);
            context.ColliderManager = new Physics.ColliderManager(context);

            // 플레이어 전투기 배열 초기화
            context.PlayerFighters = new Fighter.Fighter[context.Session.MaxPlayer];
            context.AIFighters = new Fighter.Fighter[1];

            //XXX: 임시 코드. 나중에 전투 스크립트가 담당해야 할 부분
            context.PlayerFighters[0] = new Fighter.Fighter(context, FighterID.ID.Default);

            // 임시 공격 대상
            context.AIFighters[0] = new Fighter.Fighter(context, FighterID.ID.AIDefault, true);
        }

        public void Update(PlayerInput[] inputList, FixedPoint deltaTimeInSec)
        {
            for (int i = 0; i < inputList.Length; i++)
            {
                var input = inputList[i];
                var fighter = context.PlayerFighters[i];
                fighter.Update(input, deltaTimeInSec);
                GameSystem.DebugOutput.SetWatch("My HP", fighter.HP);
            }
            foreach (var f in context.AIFighters)
                f.Update(null, deltaTimeInSec);
            GameSystem.DebugOutput.SetWatch("Enemy HP", context.AIFighters[0].HP);

            context.ProjectileManager.Update(deltaTimeInSec);
            context.ProjectileManager.Join();

            context.ParticleManager.Update(deltaTimeInSec);
            context.ParticleManager.Join();

            context.ColliderManager.Update();
        }

        CombatContext context;
    }
}
