﻿using HAJE.Cosmos.Rendering;
using OpenTK;
using System;
using System.Collections.Generic;

namespace HAJE.Cosmos.Combat.GameArea
{
    public class GameAreaRenderer : IDisposable
    {
        public GameAreaRenderer(GameAreaData data)
        {
            texture = Texture.CreateFromFile("Resource/CombatUI/Boundary.png");
            List<BoundaryElement> list = new List<BoundaryElement>();
            float distance = 50;
            foreach (var e in data.CircularGameAreas)
            {
                float circumference = Radian.Pi * 2 * (float)e.Radius;
                int count = Math.Max(5, (int)(circumference / distance));
                Radian deltaTheta = (Radian.Pi * 2) / count;
                for (Radian theta = (Radian)0; theta < 2 * Radian.Pi; theta += deltaTheta)
                {
                    var pos = e.Position + (FixedVector2)Radian.ToDirection(theta) * (FixedPoint)e.Radius;
                    if(!data.CheckArea(pos)) list.Add(new BoundaryElement(pos, theta));
                }

            }

            foreach (var e in data.RectangularGameAreas)
            {
                double width = (double)(e.From - e.To).Length();
                double halfwidth = width / 2;
                //left/right
                int lrSideCount = Math.Max(1, (int)((float)e.Radius / distance));
                //top/bottom
                int tbSideCount = Math.Max(1, (int)(halfwidth / distance));
                //Arrow starts down for convenience of angle
                Radian flipArrow = Radian.Pi/2;
                //How much it is tilt from horizontal.
                Radian tilt = (Radian)Math.Atan2((double)(e.To.Y - e.From.Y), (double)(e.To.X - e.From.X));
                Radian leftTheta = Radian.Pi/2 + tilt;
                //Left side
                for (int i = 0; i < lrSideCount; i++)
                {
                    double xIncrement = Math.Cos(leftTheta) * (float)e.Radius / lrSideCount;
                    double yIncrement = Math.Sin(leftTheta) * (float)e.Radius / lrSideCount;
                    //Bad conversion to float from double. Switch if there's a better way.
                    var pos1 = new FixedVector2(e.From.X + (FixedPoint)(xIncrement * i), e.From.Y + (FixedPoint)(yIncrement * i));
                    var pos2 = new FixedVector2(e.From.X - (FixedPoint)(xIncrement * i), e.From.Y - (FixedPoint)(yIncrement * i));
                    if (!data.CheckArea(pos1)) list.Add(new BoundaryElement(pos1, leftTheta + flipArrow));
                    if (!data.CheckArea(pos2)) list.Add(new BoundaryElement(pos2, leftTheta + flipArrow));
                }

                //Right side
                for (int i = 0; i < lrSideCount; i++)
                {
                    double xIncrement = Math.Cos(leftTheta) * (float)e.Radius / lrSideCount;
                    double yIncrement = Math.Sin(leftTheta) * (float)e.Radius / lrSideCount;
                    //Bad conversion to float from double. Switch if there's a better way.
                    var pos1 = new FixedVector2(e.To.X + (FixedPoint)(xIncrement * i), e.To.Y + (FixedPoint)(yIncrement * i));
                    var pos2 = new FixedVector2(e.To.X - (FixedPoint)(xIncrement * i), e.To.Y - (FixedPoint)(yIncrement * i));
                    if (!data.CheckArea(pos1)) list.Add(new BoundaryElement(pos1, tilt - (Radian.Pi / 2) + flipArrow));
                    if (!data.CheckArea(pos2)) list.Add(new BoundaryElement(pos2, tilt - (Radian.Pi / 2) + flipArrow));
                }
                
                //Top side
                for (int i = 0; i < tbSideCount; i++)
                {
                    double xIncrement = Math.Cos(tilt) * halfwidth / tbSideCount;
                    double yIncrement = Math.Sin(tilt) * halfwidth / tbSideCount;
                    var startpos = new FixedVector2((e.From.X + e.To.X) / 2 + (FixedPoint)(Math.Cos(leftTheta) * (float)e.Radius), (e.From.Y + e.To.Y)/2 + (FixedPoint)(Math.Sin(leftTheta) * (float)e.Radius));
                    var pos1 = new FixedVector2(startpos.X + (FixedPoint)(xIncrement * i), startpos.Y + (FixedPoint)(yIncrement * i));
                    var pos2 = new FixedVector2(startpos.X - (FixedPoint)(xIncrement * i), startpos.Y - (FixedPoint)(yIncrement * i));
                    if (!data.CheckArea(pos1)) list.Add(new BoundaryElement(pos1, tilt + flipArrow));
                    if (!data.CheckArea(pos2)) list.Add(new BoundaryElement(pos2, tilt + flipArrow));
                }

                //Bottom side
                for (int i = 0; i < tbSideCount; i++)
                {
                    double xIncrement = Math.Cos(tilt) * halfwidth / tbSideCount;
                    double yIncrement = Math.Sin(tilt) * halfwidth / tbSideCount;
                    var startpos = new FixedVector2((e.From.X + e.To.X) / 2 - (FixedPoint)(Math.Cos(leftTheta) * (float)e.Radius), (e.From.Y + e.To.Y)/2 - (FixedPoint)(Math.Sin(leftTheta) * (float)e.Radius));
                    //Bad conversion to float from double. Switch if there's a better way.
                    var pos1 = new FixedVector2(startpos.X + (FixedPoint)(xIncrement * i), startpos.Y + (FixedPoint)(yIncrement * i));
                    var pos2 = new FixedVector2(startpos.X - (FixedPoint)(xIncrement * i), startpos.Y - (FixedPoint)(yIncrement * i));
                    if (!data.CheckArea(pos1)) list.Add(new BoundaryElement(pos1, tilt + Radian.Pi + flipArrow));
                    if (!data.CheckArea(pos2)) list.Add(new BoundaryElement(pos2, tilt + Radian.Pi + flipArrow));
                }
               
            }
            BillboardBuilder builder = new BillboardBuilder(list.Count);
            foreach (var e in list)
            {
                var pos = new Vector3((float)e.Position.X, (float)e.Position.Y, ZOrder.GameDepth);
                builder.Append(pos, null, null, new Vector2(0.5f, 0.5f), new Vector2(20, 20), e.Rotation);
            }
            vbo = VertexBuffer.CreateStatic(builder.Vertices, ShaderProvider.SimpleTexture);
            ebo = ElementBuffer.Create(builder.Elements);
        }

        public void Draw(Camera camera)
        {
            ShaderProvider.SimpleTexture.Bind(ref camera.Matrix, texture);
            vbo.Bind();
            ebo.Bind();
            GLHelper.DrawTriangles(ebo.Count);
        }

        public void Dispose()
        {
            if (texture != null) texture.Dispose(); texture = null;
            if (vbo != null) vbo.Dispose(); vbo = null;
            if (ebo != null) ebo.Dispose(); ebo = null;
        }

        class BoundaryElement
        {
            public BoundaryElement(FixedVector2 position, Radian rotation)
            {
                this.Position = position;
                this.Rotation = rotation;
            }

            public FixedVector2 Position;
            public Radian Rotation;
        }

        Texture texture;
        VertexBuffer vbo;
        ElementBuffer ebo;
    }
}
