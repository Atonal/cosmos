﻿using OpenTK;

namespace HAJE.Cosmos.Combat.GameArea
{
    /// <summary>
    /// ┌────────────────────────┐ ┐
    /// │                                                │ │ Radius 
    /// + From                                       To + ┘
    /// │                                                │
    /// └────────────────────────┘
    /// </summary>
    public struct RectangularGameArea
    {
        public FixedVector2 From;
        public FixedVector2 To;
        public FixedPoint Radius;
    }
}
