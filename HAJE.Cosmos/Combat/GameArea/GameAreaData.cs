﻿using OpenTK;
using System;

namespace HAJE.Cosmos.Combat.GameArea
{
    public class GameAreaData
    {
        public CircularGameArea[] CircularGameAreas = new CircularGameArea[0];
        public RectangularGameArea[] RectangularGameAreas = new RectangularGameArea[0];

        //point가 GameAreaData 안에 존재하면 true를 리턴
        public bool CheckArea(FixedVector2 point)
        {
            foreach (var e in CircularGameAreas)
            {
                FixedVector2 distance = e.Position - point;
                if (distance.Length() < e.Radius - (FixedPoint)1) return true;
            }

            foreach (var e in RectangularGameAreas)
            {
                FixedVector2 center = (e.From + e.To) / 2;
                FixedVector2 newpoint = point - center;
                FixedPoint width = (e.From - e.To).Length() / 2;
                FixedPoint theta = FixedPoint.Atan2((e.To - center).X, (e.To - center).Y);
                newpoint = newpoint.Rotate(-theta);
                if (FixedPoint.Abs(newpoint.X) < (width - (FixedPoint)1) && FixedPoint.Abs(newpoint.Y) < (FixedPoint)(e.Radius-1)) return true;
            }
            return false;
        }

        public bool CheckArea(FixedVector2 point, int areaNumber)
        {
            if(areaNumber < CircularGameAreas.Length)
            {
                var e = CircularGameAreas[areaNumber];
                FixedVector2 distance = e.Position - point;
                if (distance.Length() < e.Radius - (FixedPoint)0.00001) return true;
            }

            else
            {
                var e = RectangularGameAreas[areaNumber - CircularGameAreas.Length];
                FixedVector2 center = (e.From + e.To) / 2;
                FixedVector2 newpoint = point - center;
                FixedPoint width = (e.From - e.To).Length() / 2;
                FixedPoint theta = FixedPoint.Atan2((e.To - center).X, (e.To - center).Y);
                newpoint = newpoint.Rotate(-theta);
                if (FixedPoint.Abs(newpoint.X) < width && FixedPoint.Abs(newpoint.Y) < (FixedPoint)e.Radius) return true;
            }
            return false;
        }

        public int FindArea(FixedVector2 point)
        {
            for (int i = 0 ; i < CircularGameAreas.Length; i++) 
            {
                var e = CircularGameAreas[i];
                FixedVector2 distance = e.Position - point;
                if (distance.Length() < e.Radius - (FixedPoint)0.00001) return i;
            }

            for (int i = 0; i < RectangularGameAreas.Length; i++) 
            {
                var e = RectangularGameAreas[i];
                FixedVector2 center = (e.From + e.To) / 2;
                FixedVector2 newpoint = point - center;
                FixedPoint width = (e.From - e.To).Length() / 2;
                FixedPoint theta = FixedPoint.Atan2((e.To - center).X, (e.To - center).Y);
                newpoint = newpoint.Rotate(-theta);
                if (FixedPoint.Abs(newpoint.X) < width && FixedPoint.Abs(newpoint.Y) < (FixedPoint)e.Radius) return i + CircularGameAreas.Length;
            }
            return -1;
        }

        public FixedVector2 GetNormalVector(FixedVector2 point, int areaNumber)
        {
            if (areaNumber < CircularGameAreas.Length)
            {
                var e = CircularGameAreas[areaNumber];
                return FixedVector2.Normalize((FixedVector2)e.Position - point);
            }

            else
            {
                var e = RectangularGameAreas[areaNumber - CircularGameAreas.Length];
                FixedVector2 center = (e.From + e.To) / 2;
                FixedVector2 newpoint = point - center;
                FixedVector2 normalVector = FixedVector2.Normalize((FixedVector2)(e.To - e.From));
                FixedPoint width = (e.From - e.To).Length() / 2;
                FixedPoint theta = FixedPoint.Atan2((e.To - center).X, (e.To - center).Y);
                newpoint = newpoint.Rotate(-theta);
                if (newpoint.X >= width) return -normalVector;
                if (newpoint.X <= -width) return normalVector;
                if (newpoint.Y >= e.Radius) return new FixedVector2(-normalVector.Y, normalVector.X);
                if (newpoint.Y <= -e.Radius) return new FixedVector2(normalVector.Y, -normalVector.X);
                return FixedVector2.Normalize(new FixedVector2(0, -1));
            }
        }
    }
}
