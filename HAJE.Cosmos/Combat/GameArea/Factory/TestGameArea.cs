﻿using OpenTK;
using System.Collections.Generic;

namespace HAJE.Cosmos.Combat.GameArea.Factory
{
    public class TestGameArea
    {
        public GameAreaData Create()
        {
            
            var circle = new List<CircularGameArea>();
            var rect = new List<RectangularGameArea>();
            // center
            circle.Add(new CircularGameArea()
            {
                Position = new FixedVector2(0, 0),
                Radius = 750
            });
            // cross
            rect.Add(new RectangularGameArea()
            {
                From = new FixedVector2(-1500, 0),
                Radius = 200,
                To = new FixedVector2(1500, 0)
            });
            rect.Add(new RectangularGameArea()
            {
                From = new FixedVector2(0, -1500),
                Radius = 200,
                To = new FixedVector2(0, 1500)
            });
            // four endpoint
            circle.Add(new CircularGameArea()
            {
                Position = new FixedVector2(-1500, 0),
                Radius = 200
            });
            circle.Add(new CircularGameArea()
            {
                Position = new FixedVector2(1500, 0),
                Radius = 200
            });
            circle.Add(new CircularGameArea()
            {
                Position = new FixedVector2(0, 1500),
                Radius = 200
            });
            circle.Add(new CircularGameArea()
            {
                Position = new FixedVector2(0, -1500),
                Radius = 200
            });
            // diagonal way
            rect.Add(new RectangularGameArea()
            {
                From = new FixedVector2(-1500, 0),
                Radius = 150,
                To = new FixedVector2(0, 1500)
            });
            rect.Add(new RectangularGameArea()
            {
                From = new FixedVector2(-1500, 0),
                Radius = 150,
                To = new FixedVector2(0, -1500)
            });
            rect.Add(new RectangularGameArea()
            {
                From = new FixedVector2(1500, 0),
                Radius = 150,
                To = new FixedVector2(0, 1500)
            });
            rect.Add(new RectangularGameArea()
            {
                From = new FixedVector2(1500, 0),
                Radius = 150,
                To = new FixedVector2(0, -1500)
            });
            var ret = new GameAreaData();
            ret.CircularGameAreas = circle.ToArray();
            ret.RectangularGameAreas = rect.ToArray();
            return ret;
        }
    }
}
