﻿using OpenTK;

namespace HAJE.Cosmos.Combat.GameArea
{
    public struct CircularGameArea
    {
        public FixedVector2 Position;
        public FixedPoint Radius;
    }
}
